<?php

namespace App\Observers;

use App\Models\Patient\Booking;
use App\Notifications\BookingCreated;
use Hashids\Hashids;
use Illuminate\Support\Facades\Auth;

class BookingObserver
{
    /**
     * Listen to the Booking created event.
     *
     * @param  Booking  $booking
     * @return void
     */
    public function created(Booking $booking)
    {
        $h = new Hashids();

        $booking->code = $h->encode($booking->id + 10000);
        $booking->save();

        if($booking->patient->email != '')
            $booking->patient->notify(new BookingCreated($booking));
    }

    /**
     * Listen to the Booking deleting event.
     *
     * @param  Booking  $booking
     * @return void
     */
    public function deleting(Booking $booking)
    {
        //
    }
}