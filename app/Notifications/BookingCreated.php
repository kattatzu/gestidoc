<?php

namespace App\Notifications;

use App\Models\Patient\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingCreated extends Notification implements ShouldQueue
{
    use Queueable;

    public $booking;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->error()
                    ->greeting('Nueva reserva de Hora Médica')
                    ->subject('Nueva reserva de Hora Médica - Centro Médico COMIN')
                    ->line('Se ha reservado una hora de atención para usted.')
                    
                    ->line('<b><u>Detalle de la reserva:</u></b>')
                    ->line(' - <b>Fecha:</b> ' . $this->booking->bookingdate->format('d/m/Y H:i'))
                    ->line(' - <b>Doctor:</b> ' . $this->booking->doctor->name)
                    ->line(' - <b>Sucursal:</b> ' . $this->booking->branch->name)

                    ->action('Anular Hora', '/')
                    ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'booking_id' => $this->booking->id,
            'date' => $this->booking->bookingdate->format('d/m/Y H:i'),
            'doctor' => $this->booking->doctor->name,
            'branch' => $this->booking->branch->name
        ];
    }
}
