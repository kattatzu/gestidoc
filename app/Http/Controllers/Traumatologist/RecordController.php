<?php

namespace App\Http\Controllers\Traumatologist;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Param\Branch;
use App\Models\Patient;
use App\Models\Patient\Booking;
use App\Models\Patient\Record;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RecordController extends Controller
{

    function index(Request $request){
        $records = Record::whereId(0);
        $ok = false;

        if(count($request->input()) > 0):
            $records = Record::with('patient', 'doctor', 'branch')->orderBy('attentiondate', 'desc');

            if($request->has('record') and is_numeric($request->input('record'))):
                $records = $records->whereId($request->input('record'));
                $ok = true;
            endif;

            if($request->has('date')):
                $records = $records->whereDate('attentiondate', $request->input('date'));
                $ok = true;
            endif;

            if($request->has('patient_rut') and $request->input('patient_rut') != ''):
                $patient = Patient::whereRut($request->input('patient_rut'))->first();
                $id = ($patient) ? $patient->id : null;

                $records = $records->wherePatientId($id);
                $ok = true;
            endif;

            if($request->has('patient_name') and $request->input('patient_name') != ''):

                $patientsId = Patient::where('firstname', 'like', '%' . $request->input('patient_name') . '%')->orWhere('lastname', 'like', '%' . $request->input('patient_name') . '%')->get()->pluck('id')->toArray();
                if(count($patientsId) > 0):
                    $records = $records->whereIn('patient_id', $patientsId);
                    $ok = true;
                else:
                    $ok = false;
                endif;
            endif;
        endif;

        if(!$ok)
            $records = Record::whereId(0);

        $records = $records->paginate();

        $selectedDate = $request->input('date');
        $selectedPatientRut = $request->input('patient_rut');
        $selectedPatientName = $request->input('patient_name');
        $selectedRecord = $request->input('record');

        return view('traumatologist.records',
            compact('records', 'selectedDate', 'selectedPatientRut', 'selectedPatientName', 'selectedRecord')
        );
    }

    function createRecord(Request $request, $id){
    	$booking = Booking::find($id);
    	if(!$booking)
    		return back()
    			->with('error', true)
    			->with('message', 'La reserva no existe!');

    	$record = Record::whereBookingId($id)->first();
    	if(!$record):
    		$record = new Record;
    		$record->booking_id = $booking->id;
    		$record->patient_id = $booking->patient_id;
    		$record->branch_id = $booking->branch_id;
    		$record->doctor_id = $booking->doctor_id;
    		$record->attentiondate = new Carbon;
    		$record->save();
    	endif;

    	return redirect()->action('Traumatologist\RecordController@edit', $record->id);
    }

    function edit(Request $request, $id){
    	$record = Record::find($id);
    	if(!$record)
    		return back()
    			->with('type', 'danger')
    			->with('message', 'La ficha no existe!');

    	$currentDoctor = Doctor::find(auth()->user()->id);

        $now = new Carbon();
        $isOwner = (auth()->user()->id == $record->doctor_id);
        $allowEditing = ($isOwner === true and $record->attentiondate->diffInHours($now) <= 48);

    	return view('traumatologist.edit',
            compact('record', 'currentDoctor', 'allowEditing', 'isOwner')
        );
    }

    function update(Request $request, $id){
    	$record = Record::find($id);
    	if(!$record)
    		return back()
    			->with('type', 'danger')
    			->with('message', 'La ficha no existe!');

    	$record->anamnesis = $request->input('anamnesis');
    	$record->physical_exam = $request->input('physical_exam');
    	$record->diagnosis = $request->input('diagnosis');
    	$record->save();

    	$patient = $record->patient;
    	$patient->family_history = $request->input('family_history');
    	$patient->save();

    	return back()
    		->with('type', 'success')
    		->with('message', 'La ficha fue guardada correctamente.');
    }

    function printOrder(Request $request, $record_id){
        $record = Record::find($record_id);
        if(!$record)
            return "La ficha médica no existe!";

    	$orderDetails = $request->input('orderDetails');
    	$doctorInfo = [
    		'doctor_fullname' 	=> $request->input('doctor_fullname'),
    		'doctor_speciality' => $request->input('doctor_speciality'),
    		'doctor_rut' 		=> $request->input('doctor_rut'),
    		'doctor_rcm' 		=> $request->input('doctor_rcm')
    	];

    	$html = view('traumatologist.printorder', compact('orderDetails', 'doctorInfo'))->render();
        $filename = 'order_' . $record_id . '_' . date('YmdHis') . '.html';

        Storage::put('uploads/' . $filename, $html);

        $record->files()->create([
            'author_id' => auth()->id(),
            'filename' => $filename,
            'realname' => $filename,
            'size' => 0,
            'mime' => 'text/html'
        ]);

        return $html;
    }

    function print(Request $request, $record_id){
        $record = Record::find($record_id);
        if(!$record)
            return "La ficha médica no existe!";

        return view('traumatologist.print',
            compact('record')
        );
    }

    function printMassive(Request $request){
        $ids = $request->input('print_ids', []);
        if(!is_array($ids) or count($ids) == 0)
            return "No ha seleccionado ninguna ficha a imprimir.";

        $records = Record::whereIn('id', $ids)->get();
        return view('traumatologist.printmassive',
            compact('records')
        );
    }
}
