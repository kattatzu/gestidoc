<?php

namespace App\Http\Controllers\Traumatologist;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    function index(Request $request){
    	$currentDoctor = Doctor::find(auth()->user()->id);

    	return view('traumatologist.index', compact('currentDoctor'));
    }
}
