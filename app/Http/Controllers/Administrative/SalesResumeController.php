<?php
namespace App\Http\Controllers\Administrative;
use App\Http\Controllers\Controller;
use App\Models\Param\Branch;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
class SalesResumeController extends Controller
{
    function index(Request $request){
        $branch_id = $request->input('branch_id');
        $datebegin = $request->input('datebegin', date('d/m/Y'));
        $dateend = $request->input('dateend', date('d/m/Y'));
        $type = $request->input('type', 'query');
        $branches = Branch::orderBy('name')->get()->pluck('name', 'id')->toArray();
        $branches = ['' => '-seleccione-'] + $branches;
        $payments = collect([]);
        $healthTotal = 0;
        $insuranceTotal = 0;
        $excessiveTotal = 0;
        $copagoTotal = 0;

        if(is_numeric($branch_id) and $datebegin != '' and $dateend != ''):
            $dateBeginEn = Carbon::createFromFormat('d/m/Y', $datebegin)->format('Y-m-d 00:00:00');
            $dateEndEn = Carbon::createFromFormat('d/m/Y', $dateend)->format('Y-m-d 23:59:59');
            $payments = Payment::whereBranchId($branch_id)->whereBetween('paymentdate', [$dateBeginEn, $dateEndEn])->get();

            foreach($payments as $payment):
                $healthTotal += $payment->productsPivot->sum('health_amount');
                $insuranceTotal += $payment->productsPivot->sum('insurance_amount');
                $excessiveTotal += $payment->productsPivot->sum('excessive_amount');
                $copagoTotal += $payment->productsPivot->sum('copago_amount');
            endforeach;
        endif;

        if($type == 'query')
            return view('administrative.salesresume.index', compact('branches', 'branch_id', 'datebegin', 'dateend', 'payments', 'healthTotal', 'insuranceTotal', 'excessiveTotal', 'copagoTotal'));

        return response()
            ->view('administrative.salesresume.excel', compact('payments', 'healthTotal', 'insuranceTotal', 'excessiveTotal', 'copagoTotal'))
            ->header('Content-Type', 'application/vnd.ms-excel; charset=utf-8')
            ->header('Content-type', 'application/x-msexcel charset=utf-8')
            ->header('Content-Disposition', 'attachment;filename=ResumenVentas-'.$dateBeginEn.'-'.$dateEndEn.'.xls');
    }
}
