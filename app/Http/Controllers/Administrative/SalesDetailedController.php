<?php
namespace App\Http\Controllers\Administrative;
use App\Http\Controllers\Controller;
use App\Models\Param\Branch;
use App\Models\Payment\Product as PaymentProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
class SalesDetailedController extends Controller
{
    function index(Request $request){
        $branch_id = $request->input('branch_id');
        $datebegin = $request->input('datebegin', date('d/m/Y'));
        $dateend = $request->input('dateend', date('d/m/Y'));
        $type = $request->input('type', 'query');
        $branches = Branch::orderBy('name')->get()->pluck('name', 'id')->toArray();
        $branches = ['' => '-seleccione-'] + $branches;
        $paymentsProducts = collect([]);
        if(is_numeric($branch_id) and $datebegin != '' and $dateend != ''):
            $dateBeginEn = Carbon::createFromFormat('d/m/Y', $datebegin)->format('Y-m-d');
            $dateEndEn = Carbon::createFromFormat('d/m/Y', $dateend)->format('Y-m-d');
            $paymentsProducts = PaymentProduct::whereBranchId($branch_id)->whereBetween('paymentdate', [$dateBeginEn, $dateEndEn])->get();
        endif;

        if($type == 'query')
            return view('administrative.salesdetailed.index', compact('branches', 'branch_id', 'datebegin', 'dateend', 'paymentsProducts'));

        return response()
            ->view('administrative.salesdetailed.excel', compact('paymentsProducts'))
            ->header('Content-Type', 'application/vnd.ms-excel; charset=utf-8')
            ->header('Content-type', 'application/x-msexcel charset=utf-8')
            ->header('Content-Disposition', 'attachment;filename=DetalleVentas-'.$dateBeginEn.'-'.$dateEndEn.'.xls');
    }
}
