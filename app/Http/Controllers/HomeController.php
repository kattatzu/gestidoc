<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        if($request->user()->isSecretaria())	return redirect('secretary');
        if($request->user()->isTraumatologo())	return redirect('traumatologo');
        if($request->user()->isKinesiologo())	return redirect('kinesiologo');
        if($request->user()->isRadiologo())		return redirect('radiologo');
        if($request->user()->isAdministrador())	return redirect('administrador');
        if($request->user()->isAdministrativo())return redirect('administrativo');
        if($request->user()->isContador())		return redirect('contador');
    }
}
