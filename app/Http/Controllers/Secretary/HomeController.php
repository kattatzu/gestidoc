<?php

namespace App\Http\Controllers\Secretary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    function index(Request $request){
    	if(!Session::get('box-opened')){
    		$box = $request->user()->openedboxes()->first();
    		if($box):
    			Session::put('box-opened', true);
		    	Session::put('box-opened-id', $box->id);
		    	Session::put('box-opened-name', $box->name);
		    	Session::put('box-opened-code', $box->code);
		    endif;
    	}

    	return view('secretary.index');
    }
}
