<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Doctor\Schedule;
use App\Models\Param\Branch;
use Illuminate\Http\Request;
class HourlyController extends Controller
{
    function index(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $doctors = Doctor::whereIn('profile', ['Kinesiólogo','Radiólogo','Traumatólogo'])->orderBy('name')->get()->pluck('name', 'id')->toArray();
        $branches = Branch::orderBy('name')->get()->pluck('name', 'id')->toArray();
        $doctors = ['' => '-seleccione-'] + $doctors;
        $branches = ['' => '-seleccione-'] + $branches;
        $disabled = ($doctor_id == '' and $branch_id == '') ? true : null;
        $hourly = [
            '1' => ['slot' => 0, 'begin' => '', 'bbegin' => '', 'bend' => '', 'end' => ''],
            '2' => ['slot' => 0, 'begin' => '', 'bbegin' => '', 'bend' => '', 'end' => ''],
            '3' => ['slot' => 0, 'begin' => '', 'bbegin' => '', 'bend' => '', 'end' => ''],
            '4' => ['slot' => 0, 'begin' => '', 'bbegin' => '', 'bend' => '', 'end' => ''],
            '5' => ['slot' => 0, 'begin' => '', 'bbegin' => '', 'bend' => '', 'end' => ''],
            '6' => ['slot' => 0, 'begin' => '', 'bbegin' => '', 'bend' => '', 'end' => ''],
        ];
        $slots = ['0' => '0 min', '5' => '5 min', '10' => '10 min', '15' => '15 min', '20' => '20 min', '30' => '30 min', '45' => '45 min', '60' => '60 min'];
        if($disabled === null):
            $schedules = Schedule::whereDoctorIdAndBranchId($doctor_id, $branch_id)->get();
            foreach($schedules as $schedule):
                $hourly[$schedule->dayofweek] = [
                    'slot' => $schedule->slots_minutes,
                    'begin' => $schedule->begin,
                    'bbegin' => $schedule->break_begin,
                    'bend' => $schedule->break_end,
                    'end' => $schedule->end
                ];
            endforeach;
        endif;

        return view('admin.hourly.index', compact('doctors', 'branches', 'disabled', 'hourly', 'slots'));
    }

    function store(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $days = ['1' => 'lun', '2' => 'mar', '3' => 'mie', '4' => 'jue', '5' => 'vie', '6' => 'sab'];
        for($i=1; $i<7; $i++):
            $day = $days[$i];
            if($request->input($day . '_slots') != '0' and $request->input($day . '_begin') != '' and $request->input($day . '_end') != ''):
                $schedule = Schedule::whereDoctorIdAndBranchIdAndDayofweek($doctor_id, $branch_id, $i)->first();
                if(!$schedule):
                    $schedule = new Schedule;
                    $schedule->doctor_id = $doctor_id;
                    $schedule->branch_id = $branch_id;
                    $schedule->dayofweek = $i;
                endif;
                $schedule->slots_minutes = $request->input($day . '_slots');
                $schedule->begin = $request->input($day . '_begin');
                $schedule->break_begin = $request->input($day . '_bbegin');
                $schedule->break_end = $request->input($day . '_bend');
                $schedule->end = $request->input($day . '_end');
                $schedule->save();
            else:
                Schedule::whereDoctorIdAndBranchIdAndDayofweek($doctor_id, $branch_id, $i)->delete();
            endif;
        endfor;

        return redirect()->action('Admin\HourlyController@index', compact('doctor_id', 'branch_id'))
            ->with([
                'message' => 'Los cambios fueron registrados correctamente.',
                'type' => 'success'
            ]);
    }
}
