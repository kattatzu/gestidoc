<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Param\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    function index(Request $request){
        $products = Product::orderBy('type')
            ->orderBy('category')
            ->orderBy('sub_category')
            ->orderBy('name')
            ->paginate();

        return view('admin.products.index', compact('products'));
    }
    function create(Request $request){
        $product = new Product;

        $types = ['Consulta'=>'Consulta','Examen'=>'Examen','Pabellón'=>'Pabellón','Radiología'=>'Radiología','Kinesiología'=>'Kinesiología','Packs Kinesicos'=>'Packs Kinesicos','Ortopédicos'=>'Ortopédicos','Otros'=>'Otros (externos)'];
        $types = collect($types)->sort();

        $doctors = Doctor::whereProfile('Traumatólogo')->orderBy('name')->get()->pluck('name', 'id')->toArray();
        $doctors = ['' => 'Ninguno'] + $doctors;

        return view('admin.products.create', compact('product', 'types', 'doctors'));
    }
    function store(Request $request){
        $product = new Product;
        $product->type = $request->input('type');
        $product->category = $request->input('category');
        $product->sub_category = $request->input('sub_category');
        $product->code = $request->input('code');
        $product->name = $request->input('name');
        if($request->input('type') == 'Consulta'):
            $product->doctor_id = is_numeric($request->input('doctor_id', 2)) ? $request->input('doctor_id', 2) : 2;
        else:
            $product->doctor_id = null;
        endif;
        $product->save();

        return redirect()->action('Admin\ProductController@index')->with([
            'message' => 'El producto/servicio fue registrado correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $product = Product::find($id);
        if(!$product)
            return back()->with([
                'message' => 'El producto/servicio consultado no existe',
                'type' => 'danger'
            ]);

        $types = ['Consulta'=>'Consulta','Examen'=>'Examen','Pabellón'=>'Pabellón','Radiología'=>'Radiología','Kinesiología'=>'Kinesiología','Packs Kinesicos'=>'Packs Kinesicos','Ortopédicos'=>'Ortopédicos','Otros'=>'Otros (externos)'];
        $types = collect($types)->sort();

        $doctors = Doctor::whereProfile('Traumatólogo')->orderBy('name')->get()->pluck('name', 'id')->toArray();
        $doctors = ['' => 'Ninguno'] + $doctors;

        return view('admin.products.edit', compact('product', 'types', 'doctors'));
    }

    function update(Request $request, $id){
        $product = Product::find($id);
        if(!$product)
            return back()->with([
                'message' => 'El producto/servicio consultado no existe',
                'type' => 'danger'
            ]);

        $product->type = $request->input('type');
        $product->category = $request->input('category');
        $product->sub_category = $request->input('sub_category');
        $product->code = $request->input('code');
        $product->name = $request->input('name');
        if($request->input('type') == 'Consulta'):
            $product->doctor_id = is_numeric($request->input('doctor_id', 2)) ? $request->input('doctor_id', 2) : 2;
        else:
            $product->doctor_id = null;
        endif;
        $product->save();

        return back()->with([
            'message' => 'El producto/servicio fue actualizado correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $product = Product::find($id);
        if(!$product)
            return back()->with([
                'message' => 'El producto/servicio consultado no existe',
                'type' => 'danger'
            ]);

        $product->delete();

        return redirect()->action('Admin\ProductController@index')->with([
            'message' => 'El producto/servicio fue eliminado correctamente',
            'type' => 'success'
        ]);
    }
}
