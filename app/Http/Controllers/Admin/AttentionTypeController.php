<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\AttentionType;
use Illuminate\Http\Request;

class AttentionTypeController extends Controller
{
    function index(Request $request){
        $atypes = AttentionType::orderBy('name')
            ->paginate();

        return view('admin.attentiontypes.index', compact('atypes'));
    }
    function create(Request $request){
        $atype = new AttentionType;

        return view('admin.attentiontypes.create', compact('atype'));
    }
    function store(Request $request){
        $atype = new AttentionType;
        $atype->name = $request->input('name');
        $atype->save();

        return redirect()->action('Admin\AttentionTypeController@index')->with([
            'message' => 'El tipo de atención fue registrado correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $atype = AttentionType::find($id);
        if(!$atype)
            return back()->with([
                'message' => 'El tipo de atención consultado no existe',
                'type' => 'danger'
            ]);

        return view('admin.attentiontypes.edit', compact('atype'));
    }

    function update(Request $request, $id){
        $atype = AttentionType::find($id);
        if(!$atype)
            return back()->with([
                'message' => 'El tipo de atención consultado no existe',
                'type' => 'danger'
            ]);

        $atype->name = $request->input('name');
        $atype->save();

        return back()->with([
            'message' => 'El tipo de atención fue actualizado correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $atype = AttentionType::find($id);
        if(!$atype)
            return back()->with([
                'message' => 'El tipo de atención consultado no existe',
                'type' => 'danger'
            ]);

        $atype->delete();

        return redirect()->action('Admin\AttentionTypeController@index')->with([
            'message' => 'El tipo de atención fue eliminado correctamente',
            'type' => 'success'
        ]);
    }
}
