<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\HealthCompany;
use Illuminate\Http\Request;

class HealthCompanyController extends Controller
{
    function index(Request $request){
        $hcompanies = HealthCompany::orderBy('order')
            ->paginate();

        return view('admin.healthcompanies.index', compact('hcompanies'));
    }
    function create(Request $request){
        $hcompany = new HealthCompany;

        return view('admin.healthcompanies.create', compact('hcompany'));
    }
    function store(Request $request){
        $hcompany = new HealthCompany;
        $hcompany->name = $request->input('name');
        $hcompany->save();

        return redirect()->action('Admin\HealthCompanyController@index')->with([
            'message' => 'La previsión fue registrada correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $hcompany = HealthCompany::find($id);
        if(!$hcompany)
            return back()->with([
                'message' => 'La previsión consultada no existe',
                'type' => 'danger'
            ]);

        return view('admin.healthcompanies.edit', compact('hcompany'));
    }

    function update(Request $request, $id){
        $hcompany = HealthCompany::find($id);
        if(!$hcompany)
            return back()->with([
                'message' => 'La previsión consultada no existe',
                'type' => 'danger'
            ]);

        $hcompany->name = $request->input('name');
        $hcompany->save();

        return back()->with([
            'message' => 'La previsión fue actualizada correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $hcompany = HealthCompany::find($id);
        if(!$hcompany)
            return back()->with([
                'message' => 'La previsión consultada no existe',
                'type' => 'danger'
            ]);

        $hcompany->delete();

        return redirect()->action('Admin\HealthCompanyController@index')->with([
            'message' => 'La previsión fue eliminada correctamente',
            'type' => 'success'
        ]);
    }
}
