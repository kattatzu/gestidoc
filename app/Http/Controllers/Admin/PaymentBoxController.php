<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\Branch;
use App\Models\Param\Branch\PaymentBox;
use Illuminate\Http\Request;

class PaymentBoxController extends Controller
{
    function index(Request $request){
        $pboxes = PaymentBox::orderBy('branch_id')
            ->orderBy('name')
            ->paginate();

        return view('admin.paymentboxes.index', compact('pboxes'));
    }
    function create(Request $request){
        $pbox = new PaymentBox;
        $branches = Branch::orderBy('name')->get()->pluck('name', 'id');

        return view('admin.paymentboxes.create', compact('pbox', 'branches'));
    }
    function store(Request $request){
        $pbox = new PaymentBox;
        $pbox->branch_id = $request->input('branch_id');
        $pbox->code = $request->input('code');
        $pbox->name = $request->input('name');
        $pbox->save();

        return redirect()->action('Admin\PaymentBoxController@index')->with([
            'message' => 'La caja fue registrada correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $pbox = PaymentBox::find($id);
        if(!$pbox)
            return back()->with([
                'message' => 'La caja consultada no existe',
                'type' => 'danger'
            ]);

        $branches = Branch::orderBy('name')->get()->pluck('name', 'id');

        return view('admin.paymentboxes.edit', compact('pbox', 'branches'));
    }

    function update(Request $request, $id){
        $pbox = PaymentBox::find($id);
        if(!$pbox)
            return back()->with([
                'message' => 'La caja consultada no existe',
                'type' => 'danger'
            ]);

        $pbox->branch_id = $request->input('branch_id');
        $pbox->code = $request->input('code');
        $pbox->name = $request->input('name');
        $pbox->save();

        return back()->with([
            'message' => 'La caja fue actualizada correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $pbox = PaymentBox::find($id);
        if(!$pbox)
            return back()->with([
                'message' => 'La caja consultada no existe',
                'type' => 'danger'
            ]);

        $pbox->delete();

        return redirect()->action('Admin\PaymentBoxController@index')->with([
            'message' => 'La caja fue eliminada correctamente',
            'type' => 'success'
        ]);
    }
}
