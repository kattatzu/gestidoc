<?php

namespace App\Http\Controllers\Admin;

use Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function index(Request $request){
        $users = User::orderBy('active', 'desc')
            ->orderBy('name')
            ->paginate();

        return view('admin.users.index', compact('users'));
    }
    function create(Request $request){
        $user = new User;
        $profiles = ['Secretaria' => 'Secretaria','Kinesiólogo' => 'Kinesiólogo','Administrativo' => 'Administrativo','Contador' => 'Contador','Administrador' => 'Administrador','Radiólogo' => 'Radiólogo','Traumatólogo' => 'Traumatólogo'];
        $status = [1 => 'Activo', 0 => 'Inactivo'];

        return view('admin.users.create', compact('user', 'profiles', 'status'));
    }
    function store(Request $request){
        $user = new User;
        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->profile = $request->input('profile');
        $user->active = $request->input('active');
        $user->password = Hash::make($request->input('password'));
        $user->secure_password = md5($request->input('secure_password'));

        $user->doctor_age_begin = $request->input('doctor_age_begin');
        $user->doctor_age_end = $request->input('doctor_age_end');
        $user->doctor_fullname = $request->input('doctor_fullname');
        $user->doctor_speciality = $request->input('doctor_speciality');
        $user->doctor_rut = $request->input('doctor_rut');
        $user->doctor_icm = $request->input('doctor_icm');

        $user->save();

        return redirect()->action('Admin\UserController@index')->with([
            'message' => 'El usuario fue registrado correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $user = User::find($id);
        if(!$user)
            return back()->with([
                'message' => 'El usuario consultado no existe',
                'type' => 'danger'
            ]);
        $profiles = ['Secretaria' => 'Secretaria','Kinesiólogo' => 'Kinesiólogo','Administrativo' => 'Administrativo','Contador' => 'Contador','Administrador' => 'Administrador','Radiólogo' => 'Radiólogo','Traumatólogo' => 'Traumatólogo'];
        $status = [1 => 'Activo', 0 => 'Inactivo'];

        return view('admin.users.edit', compact('user', 'profiles', 'status'));
    }

    function update(Request $request, $id){
        $user = User::find($id);
        if(!$user)
            return back()->with([
                'message' => 'El usuario consultado no existe',
                'type' => 'danger'
            ]);

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->profile = $request->input('profile');
        $user->active = $request->input('active');
        if($request->input('password') != '' and $request->input('password') == $request->input('password_confirmation'))
            $user->password = Hash::make($request->input('password'));
        if($request->input('secure_password') != '' and $request->input('secure_password') == $request->input('secure_password_confirmation'))
            $user->secure_password = md5($request->input('secure_password'));
        $user->save();

        return back()->with([
            'message' => 'El usuario fue actualizado correctamente',
            'type' => 'success'
        ]);
    }
}
