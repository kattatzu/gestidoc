<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\HealthCompany;
use App\Models\Patient;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    function index(Request $request){
        $patients = Patient::orderBy('lastname')
            ->orderBy('firstname')
            ->paginate();

        return view('admin.patients.index', compact('patients'));
    }
    function create(Request $request){
        $patient = new Patient;

        $healthcompanies = HealthCompany::orderBy('order')->get()->pluck('name', 'id');

        return view('admin.patients.create', compact('patient', 'healthcompanies'));
    }
    function store(Request $request){
        $patient = null;
        if($request->input('rut') != '' and $request->input('rut') != 'PENDIENTE')
            $patient = Patient::whereRut($request->input('rut'))->first();

        if(!$patient)
            $patient = new Patient;

        $patient->healthcompany_id = $request->input('healthcompany_id');
        $patient->rut = $request->input('rut');
        $patient->firstname = $request->input('firstname');
        $patient->lastname = $request->input('lastname');
        $patient->phone = $request->input('phone');
        $patient->email = $request->input('email');
        $patient->birthdate = Carbon::createFromFormat('d/m/Y', $request->input('birthdate'))->format('Y-m-d');
        $patient->age = Carbon::createFromFormat('d/m/Y', $request->input('birthdate'))->diffInYears(Carbon::today(), true);
        $patient->address = $request->input('address');
        $patient->save();

        return redirect()->action('Admin\PatientController@index')->with([
            'message' => 'El paciente fue registrado correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $patient = Patient::find($id);
        if(!$patient)
            return back()->with([
                'message' => 'El paciente consultado no existe',
                'type' => 'danger'
            ]);

        $healthcompanies = HealthCompany::orderBy('order')->get()->pluck('name', 'id');

        return view('admin.patients.edit', compact('patient', 'healthcompanies'));
    }

    function update(Request $request, $id){
        $patient = Patient::find($id);
        if(!$patient)
            return back()->with([
                'message' => 'El paciente consultado no existe',
                'type' => 'danger'
            ]);

        $patient->healthcompany_id = $request->input('healthcompany_id');
        $patient->rut = $request->input('rut');
        $patient->firstname = $request->input('firstname');
        $patient->lastname = $request->input('lastname');
        $patient->phone = $request->input('phone');
        $patient->email = $request->input('email');
        $patient->birthdate = Carbon::createFromFormat('d/m/Y', $request->input('birthdate'))->format('Y-m-d');
        $patient->age = Carbon::createFromFormat('d/m/Y', $request->input('birthdate'))->diffInYears(Carbon::today(), true);
        $patient->address = $request->input('address');
        $patient->save();

        return back()->with([
            'message' => 'El paciente fue actualizado correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $patient = Patient::find($id);
        if(!$patient)
            return back()->with([
                'message' => 'El paciente consultado no existe',
                'type' => 'danger'
            ]);

        $patient->delete();

        return redirect()->action('Admin\PatientController@index')->with([
            'message' => 'El paciente fue eliminado correctamente',
            'type' => 'success'
        ]);
    }
}
