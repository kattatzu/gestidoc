<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\Branch;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    function index(Request $request){
        $branches = Branch::orderBy('name')
            ->paginate();

        return view('admin.branches.index', compact('branches'));
    }
    function create(Request $request){
        $branch = new Branch;

        return view('admin.branches.create', compact('branch'));
    }
    function store(Request $request){
        $branch = new Branch;
        $branch->name = $request->input('name');
        $branch->save();

        return redirect()->action('Admin\BranchController@index')->with([
            'message' => 'La sucursal fue registrada correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $branch = Branch::find($id);
        if(!$branch)
            return back()->with([
                'message' => 'La sucursal consultada no existe',
                'type' => 'danger'
            ]);

        return view('admin.branches.edit', compact('branch'));
    }

    function update(Request $request, $id){
        $branch = Branch::find($id);
        if(!$branch)
            return back()->with([
                'message' => 'La sucursal consultada no existe',
                'type' => 'danger'
            ]);

        $branch->name = $request->input('name');
        $branch->save();

        return back()->with([
            'message' => 'La sucursal fue actualizada correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $branch = Branch::find($id);
        if(!$branch)
            return back()->with([
                'message' => 'La sucursal consultada no existe',
                'type' => 'danger'
            ]);

        $branch->delete();

        return redirect()->action('Admin\BranchController@index')->with([
            'message' => 'La sucursal fue eliminada correctamente',
            'type' => 'success'
        ]);
    }
}
