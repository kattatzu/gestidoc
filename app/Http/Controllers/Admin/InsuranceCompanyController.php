<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\InsuranceCompany;
use Illuminate\Http\Request;

class InsuranceCompanyController extends Controller
{
    function index(Request $request){
        $icompanies = InsuranceCompany::orderBy('name')
            ->paginate();

        return view('admin.insurancecompanies.index', compact('icompanies'));
    }
    function create(Request $request){
        $icompany = new InsuranceCompany;

        return view('admin.insurancecompanies.create', compact('icompany'));
    }
    function store(Request $request){
        $icompany = new InsuranceCompany;
        $icompany->code = $request->input('code');
        $icompany->name = $request->input('name');
        $icompany->save();

        return redirect()->action('Admin\InsuranceCompanyController@index')->with([
            'message' => 'La compañía de seguros fue registrada correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $icompany = InsuranceCompany::find($id);
        if(!$icompany)
            return back()->with([
                'message' => 'La compañía de seguros consultada no existe',
                'type' => 'danger'
            ]);

        return view('admin.insurancecompanies.edit', compact('icompany'));
    }

    function update(Request $request, $id){
        $icompany = InsuranceCompany::find($id);
        if(!$icompany)
            return back()->with([
                'message' => 'La compañía de seguros consultada no existe',
                'type' => 'danger'
            ]);

        $icompany->code = $request->input('code');
        $icompany->name = $request->input('name');
        $icompany->save();

        return back()->with([
            'message' => 'La compañía de seguros fue actualizada correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $icompany = InsuranceCompany::find($id);
        if(!$icompany)
            return back()->with([
                'message' => 'La compañía de seguros consultada no existe',
                'type' => 'danger'
            ]);

        $icompany->delete();

        return redirect()->action('Admin\InsuranceCompanyController@index')->with([
            'message' => 'La compañía de seguros fue eliminada correctamente',
            'type' => 'success'
        ]);
    }
}
