<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Param\ComercialCompany;
use Illuminate\Http\Request;

class CommercialCompanyController extends Controller
{
    function index(Request $request){
        $ccompanies = ComercialCompany::orderBy('name')
            ->paginate();

        return view('admin.commercialcompanies.index', compact('ccompanies'));
    }
    function create(Request $request){
        $ccompany = new ComercialCompany;

        return view('admin.commercialcompanies.create', compact('ccompany'));
    }
    function store(Request $request){
        $ccompany = new ComercialCompany;
        $ccompany->name = $request->input('name');
        $ccompany->save();

        return redirect()->action('Admin\CommercialCompanyController@index')->with([
            'message' => 'La compañía fue registrada correctamente',
            'type' => 'success'
        ]);
    }
    function edit(Request $request, $id){
        $ccompany = ComercialCompany::find($id);
        if(!$ccompany)
            return back()->with([
                'message' => 'La compañía consultada no existe',
                'type' => 'danger'
            ]);

        return view('admin.commercialcompanies.edit', compact('ccompany'));
    }

    function update(Request $request, $id){
        $ccompany = ComercialCompany::find($id);
        if(!$ccompany)
            return back()->with([
                'message' => 'La compañía consultada no existe',
                'type' => 'danger'
            ]);

        $ccompany->name = $request->input('name');
        $ccompany->save();

        return back()->with([
            'message' => 'La compañía fue actualizada correctamente',
            'type' => 'success'
        ]);
    }
    function destroy(Request $request, $id){
        $ccompany = ComercialCompany::find($id);
        if(!$ccompany)
            return back()->with([
                'message' => 'La compañía consultada no existe',
                'type' => 'danger'
            ]);

        $ccompany->delete();

        return redirect()->action('Admin\CommercialCompanyController@index')->with([
            'message' => 'La compañía fue eliminada correctamente',
            'type' => 'success'
        ]);
    }
}
