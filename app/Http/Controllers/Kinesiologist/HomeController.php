<?php
namespace App\Http\Controllers\Kinesiologist;
use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index(Request $request){
        $currentDoctor = Doctor::find(auth()->user()->id);

        return view('kinesiologist.index', compact('currentDoctor'));
    }
}
