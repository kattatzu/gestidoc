<?php
namespace App\Http\Controllers\Kinesiologist;
use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Patient\Booking;
use App\Models\Patient\BookingDetail;
use Illuminate\Http\Request;
class RecordController extends Controller
{
    function index(){

    }
    function createRecord(Request $request, $id){
        $booking = Booking::find($id);
        if(!$booking)
            return back()
                ->with('type', 'danger')
                ->with('message', 'La ficha no existe!');

        return redirect()->action('Kinesiologist\RecordController@edit', $booking->id);
    }

    function edit(Request $request, $booking_id){
        $booking = Booking::find($booking_id);
        $kinedetail = $booking->kinedetail;
        if(!$booking or !$kinedetail)
            return back()
                ->with('type', 'danger')
                ->with('message', 'La ficha no existe!');

        $currentDoctor = Doctor::find(auth()->user()->id);

        return view('kinesiologist.edit',
            compact('booking', 'kinedetail', 'currentDoctor')
        );
    }

    function update(Request $request, $booking_id){
        $booking = Booking::find($booking_id);
        if(!$booking)
            return back()
                ->with('type', 'danger')
                ->with('message', 'La ficha no existe!');

        $booking->patient_status = 'Atendido';
        $booking->save();

        $kinedetail = $booking->kinedetail;
        $kinedetail->diagnosis = $request->input('diagnosis');
        $kinedetail->indication_date = $request->input('indication_date');
        $kinedetail->treatment_date = $request->input('treatment_date');
        $kinedetail->observations = $request->input('observations');
        $kinedetail->save();

        if($request->hasFile('file')):
            $filename = md5(auth()->user()->id . date('YmdHis')) . '.' . $request->file->extension();
            $request->file->storeAs('uploads', $filename);

            $kinedetail->files()->create([
                'author_id' => auth()->id(),
                'filename' => $filename,
                'realname' => $filename,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getClientMimeType()
            ]);
        endif;

        $bookigns = $request->input('kine_success');
        foreach($bookigns as $bookingId => $success):
            $aBooking = Booking::find($bookingId);
            $aBooking->kine_success = $success;
            $aBooking->save();
        endforeach;

        return back()
                ->with('type', 'success')
                ->with('message', 'La ficha kinesiológica fue actualizada correctamente!');
    }

    function print(Request $request, $booking_id){
        $booking = Booking::find($booking_id);
        $kinedetail = $booking->kinedetail;
        if(!$booking or !$kinedetail)
            return "La ficha no existe!";

        return view('kinesiologist.print',
            compact('booking', 'kinedetail')
        );
    }
}
