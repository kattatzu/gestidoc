<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookingRequest;
use App\Libs\AjaxResponse;
use App\Models\Param\Product;
use App\Models\Param\Product\Price;
use App\Models\Patient;
use App\Models\Patient\Booking;
use App\Models\Patient\BookingDetail;
use App\Models\Patient\Record;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    function store(StoreBookingRequest $request){
        $password = md5($request->input('password'));
        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

    	$branchId = $request->input('branch_id');
    	$doctorId = $request->input('doctor_id');

        $detailsId = null;
        if($request->has('packSessions')):
            $detail = new BookingDetail;
            $detail->product_id = $request->input('packSelectedId');
            $detail->sessions = $request->input('packSessions');
            $detail->details = $request->input('packDetails');
            $detail->evaluation = $request->input('packEvaluation');
            $detail->save();

            $detailsId = $detail->id;
        endif;

    	if($request->has('bookingdate')):
    		$bookingDate = $request->input('bookingdate');

    		return $this->storeBooking($request, $branchId, $doctorId, $bookingDate, $user, $detailsId);
    	else:
    		$rs = [];
    		$allOk = true;
    		$msg = null;

    		$bookingDates = $request->input('bookingdates');

    		foreach($bookingDates as $bookingDate):
    			$rsNewBooking = $this->storeBooking($request, $branchId, $doctorId, $bookingDate, $user, $detailsId);
                $allOk = !$rsNewBooking['success'] ? false : $allOk;
    			$rs[] = $rsNewBooking;
    		endforeach;

    		if(!$allOk)
    			$msg = "No se logró reservar algunas fechas ya que se encuentran copadas, por favor revise e intente nuevamente";

            $rs = collect($rs)->sortBy(function($booking){
                return $booking['data']['bookingdate'];
            })->toArray();

    		return AjaxResponse::make($rs, $allOk, $msg);
    	endif;
    }

    private function storeBooking($request, $branchId, $doctorId, $bookingDate, $user, $detailsId){
    	if(Booking::validateUniqueHour($branchId, $doctorId, $bookingDate)):
	    	$booking = new Booking;
            $booking->author_id = $user->id;
            $booking->kinedetails_id = $detailsId;
	    	$booking->healthcompany_id = $request->input('healthcompany_id');
	    	$booking->branch_id = $branchId;
	    	$booking->patient_id = $request->input('patient_id');
	    	$booking->doctor_id = $doctorId;
	    	$booking->bookingdate = $bookingDate;
	    	$booking->attentiontype_id = $request->input('attentiontype_id');
	    	$booking->observations = $request->input('observations');
	    	$booking->type = $request->input('type', 'Normal');
	    	$booking->status = $request->input('status', 'Reservada');
	    	$booking->save();

            $booking = Booking::with('doctor', 'branch', 'attentiontype')->find($booking->id);

	    	return AjaxResponse::success($booking);
	    else:
			return AjaxResponse::make(['bookingdate' => $bookingDate], false, 'La hora ingresada ya ha sido reservada.');
	    endif;
    }

    function search(Request $request){
        $code = $request->input('code');
        $rut = $request->input('rut');
        $name = $request->input('name');

        $bookingdate = $request->input('bookingdate');
        $branch_id = $request->input('branch_id');
        $doctor_id = $request->input('doctor_id');

        if($code != ''):
            $bookings = Booking::whereCode($code)->with('patient', 'healthcompany', 'attentiontype', 'doctor', 'doctor.prices', 'branch', 'kinedetail', 'kinedetail.bookings', 'kinedetail.product', 'kinedetail.product.subproducts', 'kinedetail.product.subproducts.prices')->wherePatientStatus('Pendiente')->get();
            return AjaxResponse::make($bookings, ($bookings !== null), (($bookings) ? null : "No se encontró la reserva buscada."));
        else:
            $bookings = [];

            if($rut != ''):
                $patient = Patient::whereRut($rut)->first();

                if(!$patient)
                    return AjaxResponse::fail("No se encontraron reservas para el rut consultado");

                $bookings = $patient->bookings()->wherePatientStatus('Pendiente');
            elseif($name != ''):
                $patientsId = Patient::where(DB::raw("CONCAT(firstname, ' ', lastname)"), 'like', '%' . str_replace(' ', '%', $name) . '%')->get()->pluck('id');
                if(count($patientsId) == 0)
                    return AjaxResponse::fail("No se encontraron reservas para el nombre consultado");

                $bookings = Booking::whereIn('patient_id', $patientsId)->wherePatientStatus('Pendiente');
            endif;

            $bookings = $bookings->whereDate('bookingdate', '>=', Carbon::today()->format('Y-m-d'))->orderBy('bookingdate');

            if($bookingdate != '')
                $bookings = $bookings->whereDate('bookingdate', $bookingdate);

            if($branch_id != '')
                $bookings = $bookings->whereBranchId($branch_id);

            if($doctor_id != '')
                $bookings = $bookings->whereDoctorId($doctor_id);

            if(count($bookings) == 0)
                return AjaxResponse::fail("No se encontraron reservas con los filtros ingresados");

            return AjaxResponse::make($bookings->with('patient', 'healthcompany', 'attentiontype', 'doctor', 'doctor.prices', 'branch', 'kinedetail', 'kinedetail.bookings', 'kinedetail.product', 'kinedetail.product.subproducts', 'kinedetail.product.subproducts.prices')->get());
        endif;
    }

    function receipt(Request $request, $id){
        $booking = Booking::find($id);
        if(!$booking)
            return AjaxResponse::fail('No se encontró la hora consultada');

        $password = md5($request->input('password'));
        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

        $booking->patient_status        = 'En Espera';
        if($request->input('booking_healthcompany_id'))
            $booking->healthcompany_id      = $request->input('booking_healthcompany_id');
        if($request->input('booking_attentiontype_id'))
            $booking->attentiontype_id      = $request->input('booking_attentiontype_id');
        if($request->input('booking_observations'))
            $booking->observations          = $request->input('booking_observations');

        $patientId              = $request->input('patient_id');
        $rut                    = $request->input('patient_rut');
        $patient                = Patient::find($patientId);
        if(!$patient)
            return AjaxResponse::fail("No se encontró al paciente consultado.");

         $patientByRut           = Patient::whereRut($rut)->first();
        if($patientByRut and $patientByRut->id != $patient->id):
            $patientByRut->firstname        = $request->input('patient_firstname');
            $patientByRut->lastname         = $request->input('patient_lastname');
            $patientByRut->email            = $request->input('patient_email');
            $patientByRut->phone            = $request->input('patient_phone');
            $patientByRut->address          = $request->input('patient_address');
            $patientByRut->birthdate        = $request->input('patient_birthdate');
            $patientByRut->age              = $request->input('patient_age', $patientByRut->age);
            $patientByRut->save();

            $booking->patient_id = $patientByRut->id;
        else:
            if($patient->rut == 'PENDIENTE')
                $patient->rut        = $request->input('patient_rut');

            $patient->firstname        = $request->input('patient_firstname');
            $patient->lastname         = $request->input('patient_lastname');
            $patient->email            = $request->input('patient_email');
            $patient->phone            = $request->input('patient_phone');
            $patient->address          = $request->input('patient_address');
            $patient->birthdate        = $request->input('patient_birthdate');
            $patient->age              = $request->input('patient_age', $patient->age);
            $patient->save();
        endif;

        $booking->save();

        if($request->input('pay')):
            if($user->openedboxes()->count() == 0)
                return AjaxResponse::fail("Usted no tiene ninguna caja aperturada.");

            $box = $user->openedboxes->first();

            // Se registra la ficha asociada
            $record = new Record;
            $record->patient_id     = $booking->patient_id;
            $record->booking_id     = $booking->id;
            $record->branch_id      = $booking->branch_id;
            $record->doctor_id      = $booking->doctor_id;
            $record->attentiondate  = new Carbon;
            $record->save();

            $payment = $booking->payment;
            if(!$payment):
                $booking->payment()->create([
                    'branch_id'             => $booking->branch_id,
                    'box_id'                => $box->id,
                    'boxopened_id'          => $box->pivot->id,
                    'seller_id'             => $user->id,
                    'doctor_id'             => $booking->doctor_id,
                    'patient_id'            => $booking->patient_id,
                    'healthcompany_id'      => $request->input('payment_healthcompany_id'),
                    //'insurancecompany_id'   => $request->input('payment_insurancecompany_id'),
                    'commercialcompany_id'  => $request->input('payment_commercialcompany_id'),
                    'paymentdate'           => new Carbon,

                    'subtotal'              => $request->input('payment_subtotal'),
                    'discount_amount'       => $request->input('payment_discount_amount'),
                    'discount_reason'       => $request->input('payment_discount_reason'),
                    'total'                 => $request->input('payment_total'),
                    'copago_amount'         => $request->input('payment_copago_amount'),
                    /*
                    'health_amount'         => $request->input('payment_health_amount'),
                    'health_document'       => $request->input('payment_health_document'),
                    'insurance_amount'      => $request->input('payment_insurance_amount'),
                    'insurance_document'    => $request->input('payment_insurance_document'),
                    'copago_document'       => $request->input('payment_copago_document'),
                    */

                    'debit_amount'          => $request->input('payment_debit_amount'),
                    'credit_amount'         => $request->input('payment_credit_amount'),
                    'commercial_amount'     => $request->input('payment_commercial_amount'),
                    'cash_amount'           => $request->input('payment_cash_amount'),
                ]);
                $booking->payment_status = 'Pagado';
                $booking->save();

                $booking = Booking::find($id);
                $payment = $booking->payment;

                $doctor = $booking->doctor;
                $price = $doctor->prices()->whereCompanyId($payment->healthcompany_id)->first();

                if(!$price)
                    $price = $doctor->prices()->whereCompanyId(11)->first();

                if(!$price):
                    $product = Product::whereDoctorId($doctor->id)->first();
                    if(!$product):
                        $product = new Product;
                        $product->doctor_id = $doctor->id;
                        $product->type = 'Consulta';
                        $product->search = str_slug('consulta ' . $doctor->name, ' ') . "Consulta " . $doctor->name;
                        $product->save();
                    endif;

                    $price = new Price;
                    $price->product_id = $product->id;
                    $price->company_id = 11;
                    $price->price = 0;
                    $price->save();
                endif;

                if($price):
                    $openedPivotBox = $user->openedPivotBox()->first();

                    $payment->products()->attach($price->product_id, [
                        'branch_id'     => $booking->branch_id,
                        'box_id'        => $box->id,
                        'boxopened_id'  => $openedPivotBox->id,
                        'seller_id'     => $payment->seller_id,
                        'doctor_id'     => $booking->doctor_id,
                        'patient_id'    => $booking->patient_id,
                        'price'         => $payment->subtotal,
                        'quantity'      => 1,
                        'subtotal'      => $payment->subtotal,
                        'discount'      => $payment->discount_amount,
                        'total'         => $payment->total,
                        'paymentdate'   => new Carbon,

                        'healthcompany_id'      => $payment->healthcompany_id,
                        'insurancecompany_id'   => $request->input('payment_insurancecompany_id', null),
                        'health_amount'         => $request->input('payment_health_amount', 0),
                        'health_document'       => $request->input('payment_health_document'),
                        'insurance_amount'      => $request->input('payment_insurance_amount', 0),
                        'insurance_document'    => $request->input('payment_insurance_document'),
                        'excessive_amount'      => $request->input('payment_excessive_amount', 0),
                        'excessive_document'    => $request->input('payment_excessive_document'),
                        'copago_amount'         => $request->input('payment_copago_amount', 0),
                        'copago_document'       => $request->input('payment_copago_document'),
                    ]);
                endif;
            endif;

            return AjaxResponse::success(['payed' => true, 'payment' => $payment]);
        endif;

        return AjaxResponse::success(['payed' => false]);
    }

    function setPatientActitude(Request $request, $id){
        $booking = Booking::find($id);
        if(!$booking)
            return AjaxResponse::fail('No se encontró la hora consultada');

        $booking->patient_actitude = $request->input('actitude');
        $booking->save();

        return AjaxResponse::success(null);
    }

    function lock(Request $request){
        $branch_id = $request->input('branch_id');
        $doctor_id = $request->input('doctor_id');
        $datetime = $request->input('datetime');
        $reason = $request->input('reason');
        $password = $request->input('password');

        $password = md5($request->input('password'));
        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

        $booking = Booking::whereBranchIdAndDoctorIdAndBookingdateAndStatus($branch_id, $doctor_id, $datetime, 'Bloqueada')->first();
        if($booking)
            return AjaxResponse::fail("La hora ya fue reservada o bloqueada previamente, por favor re intente.");

        $booking = new Booking;
        $booking->author_id = $user->id;
        $booking->branch_id = $branch_id;
        $booking->patient_id = 1;
        $booking->doctor_id = $doctor_id;
        $booking->bookingdate = $datetime;
        $booking->observations = $reason;
        $booking->status = 'Bloqueada';
        $booking->save();

        return AjaxResponse::success($booking);
    }

    function unlock(Request $request){
        $booking_id = $request->input('booking_id');
        $reason = $request->input('reason');
        $password = $request->input('password');

        $booking = Booking::find($booking_id);
        if(!$booking)
            return AjaxResponse::fail('No se encontró la hora consultada');

        if($booking->status != 'Bloqueada')
            return AjaxResponse::fail('No se pudo desbloquear la hora ya que no se encuentra bloqueada');

        $password = md5($request->input('password'));
        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

        $booking->observations = $booking->observations . ' - Desbloqueo: ' . $reason;
        $booking->status = 'Bloqueada';
        $booking->save();

        $booking->delete();

        return AjaxResponse::success($booking);
    }

    function cancel(Request $request){
        $booking_id = $request->input('booking_id');
        $reason = $request->input('reason');
        $password = $request->input('password');

        $booking = Booking::find($booking_id);
        if(!$booking)
            return AjaxResponse::fail('No se encontró la hora consultada');

        $password = md5($request->input('password'));
        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

        $booking->observations = $booking->observations . ' - Anulada: ' . $reason;
        $booking->status = 'Anulada';
        $booking->save();

        $booking->delete();

        return AjaxResponse::success($booking);
    }

    function issetOrNull($value){
        return (isset($value) and $value !== null) ? $value : null;
    }

    function issetOrZero($value){
        return (isset($value) and $value !== null and is_numeric($value)) ? $value : 0;
    }

    function issetOr($value, $default = null){
        return (isset($value) and $value !== null) ? $value : $default;
    }
}







