<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\Branch;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    function index(Request $request){
    	return AjaxResponse::make(
    		Branch::orderBy('name')
    			->with('paymentboxes')
    		    ->with('traumatologists')
    		    ->with('kinesiologists')
    		    ->with('radiologists')
    		    ->get()
    	);
    }

    function show(Request $request, $id){
    	$branch = Branch::find($id);
    	if(!$branch)
    		return AjaxResponse::make(null, false, "Sucursal no encontrada");

    	return AjaxResponse::make($branch);
    }

    function lastFreeHours(Request $request, $id){
        $date = $request->input('date');
        $profile = $request->input('profile');

        $branch = Branch::find($id);
        if(!$branch) return AjaxResponse::make(null, false, "Sucursal no encontrada");
        if(!$date) return AjaxResponse::make(null, false, "Debe especificar una fecha");
        if(!$profile) return AjaxResponse::make(null, false, "Debe especificar un perfil");

        $day = new Carbon($date);

        return AjaxResponse::make($branch->lastFreeHours($day, $profile));
    }
}
