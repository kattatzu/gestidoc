<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Doctor;
use App\Models\Param\Branch;
use App\Models\Param\HealthCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    function index(Request $request){

        $doctors = Doctor::orderBy('name')->whereIn('profile', [
            'Kinesiólogo',
            'Radiólogo',
            'Traumatólogo'
        ]);

        if($request->has('healthcompany_id')):
            $doctorsByHC = HealthCompany::find($request->get('healthcompany_id'));
            $ids = $doctorsByHC->doctors()->pluck('doctor_id');
            
            $doctors = $doctors->whereIn('id', $ids);
        endif;

        if($request->has('branch_id')):
            $doctorsByB = Branch::find($request->get('branch_id'));
            $ids = $doctorsByB->doctors()->pluck('doctor_id');

            $doctors = $doctors->whereIn('id', $ids);
        endif;

    	return AjaxResponse::success(
            $doctors->get()
        );
    }

    function dailyHours(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $date = $request->input('date');

        $doctor = Doctor::find($doctor_id);
        if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

        $branch = Branch::find($branch_id);
        if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

        if($date == '') return AjaxResponse::make(null, false, "La fecha no es válida");

        $day = Carbon::createFromFormat("Y-m-d", $date);
        if(!$day) return AjaxResponse::make(null, false, "La fecha no es válida");

        return AjaxResponse::make($doctor->dailyHours($branch->id, $day));
    }

    function dailyHoursBetween(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $beginDate = $request->input('begin_date');
        $endDate = $request->input('end_date');

        $doctor = Doctor::find($doctor_id);
        if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

        $branch = Branch::find($branch_id);
        if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

        if($beginDate == '') return AjaxResponse::make(null, false, "La fecha de inicio no es válida");

        $dayBegin = Carbon::createFromFormat("Y-m-d", $beginDate);
        if(!$dayBegin) return AjaxResponse::make(null, false, "La fecha de inicio no es válida");

        if($endDate == '') return AjaxResponse::make(null, false, "La fecha de término no es válida");

        $dayEnd = Carbon::createFromFormat("Y-m-d", $endDate);
        if(!$dayEnd) return AjaxResponse::make(null, false, "La fecha de término no es válida");

        return AjaxResponse::make($doctor->dailyHoursBetween($branch_id, $dayBegin, $dayEnd));
    }

    function dailyHoursBetweenKine(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $beginDate = $request->input('begin_date');
        $endDate = $request->input('end_date');

        $doctor = Doctor::find($doctor_id);
        if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

        $branch = Branch::find($branch_id);
        if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

        if($beginDate == '') return AjaxResponse::make(null, false, "La fecha de inicio no es válida");

        $dayBegin = Carbon::createFromFormat("Y-m-d", $beginDate);
        if(!$dayBegin) return AjaxResponse::make(null, false, "La fecha de inicio no es válida");

        if($endDate == '') return AjaxResponse::make(null, false, "La fecha de término no es válida");

        $dayEnd = Carbon::createFromFormat("Y-m-d", $endDate);
        if(!$dayEnd) return AjaxResponse::make(null, false, "La fecha de término no es válida");

        return AjaxResponse::make($doctor->dailyHoursBetweenKine($branch_id, $dayBegin, $dayEnd));
    }

    function freeHours(Request $request){
    	$doctor_id = $request->input('doctor_id');
    	$branch_id = $request->input('branch_id');
    	$date = $request->input('date');

    	$doctor = Doctor::find($doctor_id);
    	if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

    	$branch = Branch::find($branch_id);
    	if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

    	if($date == '') return AjaxResponse::make(null, false, "La fecha no es válida");

    	$day = Carbon::createFromFormat("Y-m-d", $date);
    	if(!$day) return AjaxResponse::make(null, false, "La fecha no es válida");

    	return AjaxResponse::make($doctor->freeHours($branch->id, $day));
    }

    function resumeHours(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $date = $request->input('date');

        $doctor = Doctor::find($doctor_id);
        if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

        $branch = Branch::find($branch_id);
        if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

        if($date == '') return AjaxResponse::make(null, false, "La fecha no es válida");
        
        $day = Carbon::createFromFormat("Y-m-d", $date);
        if(!$day) return AjaxResponse::make(null, false, "La fecha no es válida");

        return AjaxResponse::make($doctor->resumeHours($branch_id, $day));
    }

    function allBookingForDate(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $date = $request->input('date');

        $doctor = Doctor::find($doctor_id);
        if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

        $branch = Branch::find($branch_id);
        if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

        if($date == '') return AjaxResponse::make(null, false, "La fecha no es válida");
        
        $day = Carbon::createFromFormat("Y-m-d", $date);
        if(!$day) return AjaxResponse::make(null, false, "La fecha no es válida");

        return AjaxResponse::make($doctor->allBookingForDate($branch_id, $day));
    }


    function resumeHoursBetween(Request $request){
        $doctor_id = $request->input('doctor_id');
        $branch_id = $request->input('branch_id');
        $beginDate = $request->input('begin_date');
        $endDate = $request->input('end_date');

        $doctor = Doctor::find($doctor_id);
        if(!$doctor) return AjaxResponse::make(null, false, "El doctor no existe");

        $branch = Branch::find($branch_id);
        if(!$branch) return AjaxResponse::make(null, false, "La sucursal no existe");

        if($beginDate == '') return AjaxResponse::make(null, false, "La fecha de inicio no es válida");

        $dayBegin = Carbon::createFromFormat("Y-m-d", $beginDate);
        if(!$dayBegin) return AjaxResponse::make(null, false, "La fecha de inicio no es válida");

        if($endDate == '') return AjaxResponse::make(null, false, "La fecha de término no es válida");

        $dayEnd = Carbon::createFromFormat("Y-m-d", $endDate);
        if(!$dayEnd) return AjaxResponse::make(null, false, "La fecha de término no es válida");

        return AjaxResponse::make($doctor->resumeHoursBetween($branch_id, $dayBegin, $dayEnd));
    }




    function oldFreehours($doctor_id, $branch_id, $date){

        $date = new Carbon($date);
        $dayOfWeek = $date->dayOfWeek;

        $doctor = Doctor::find($doctor_id);
        $schedule = $doctor->schedules()->whereBranchIdAndDayofweek($branch_id, $dayOfWeek)->first();
        if(!$schedule) return AjaxResponse::make(null, false, 'El doctor no atiende el día consultado.');

        $slotsTime = $schedule->slots_minutes;
        $beginDatetime = new Carbon($date->format("Y-m-d " . $schedule->begin));
        $endDatetime = new Carbon($date->format("Y-m-d " . $schedule->end));

        $slots = [];

        while($beginDatetime->lte($endDatetime)){
            $count = $doctor->bookings()->whereBranchIdAndBookingdate($branch_id, $beginDatetime->format("Y-m-d H:i:00"))->count();
            if($count == 0){
                $slots[] = [
                    'date' => $beginDatetime->format("Y-m-d"),
                    'time' => $beginDatetime->format("H:i")
                ];
            }

            $beginDatetime->addMinutes($slotsTime);
        }

        return AjaxResponse::make($slots);
    }
}
