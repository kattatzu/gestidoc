<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\Branch;
use Illuminate\Http\Request;

class BranchPaymentBoxController extends Controller
{
    function index(Request $request, $branch_id){
    	$branch = Branch::find($branch_id);
    	if(!$branch)
    		return AjaxResponse::make(null, false, "Sucursal no encontrada");

    	return AjaxResponse::make($branch->paymentboxes);
    }

    function show(Request $request, $branch_id, $id){
    	$branch = Branch::find($branch_id);
    	if(!$branch)
    		return AjaxResponse::make(null, false, "Sucursal no encontrada");

    	$box = $branch->paymentboxes()->find($id);
    	if(!$box)
    		return AjaxResponse::make(null, false, "Caja no encontrada");

    	return AjaxResponse::make($box);
    }
}
