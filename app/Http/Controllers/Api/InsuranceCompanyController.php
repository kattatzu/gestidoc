<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\InsuranceCompany;
use Illuminate\Http\Request;

class InsuranceCompanyController extends Controller
{
	function index(Request $request){
    	return AjaxResponse::success(InsuranceCompany::orderBy('name')->get());
    }
}
