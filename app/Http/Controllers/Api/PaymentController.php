<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentRequest;
use App\Libs\AjaxResponse;
use App\Models\Patient;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

	function store(StorePaymentRequest $request){
		$password = md5($request->input('password'));
        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

       	if($user->openedboxes()->count() == 0)
            return AjaxResponse::fail("Usted no tiene ninguna caja aperturada.");

        $box = $user->openedboxes->first();
        $openedPivotBox = $user->openedPivotBox()->first();

        $patient 				= null;
        $patientData 			= $request->input('patient');
        $patientId              = $patientData['id'];

        if($patientId):
        	$patient            = Patient::find($patientId);
        	if(!$patient)
            	return AjaxResponse::fail("No se encontró al paciente consultado.");
        endif;

        if(!$patient):
        	$rut           = $patientData['rut'];
        	$patient       = Patient::whereRut($rut)->first();

        	if(!$patient):
        		$patient = new Patient;
        		$patient->healthcompany_id 	= $request->input('healthcompany_id');
        		$patient->rut 				= $rut;
        		$patient->firstname 		= $patientData['firstname'];
	            $patient->lastname  		= $patientData['lastname'];
	            $patient->save();
        	else:
        		$patient->healthcompany_id 	= $request->input('healthcompany_id');
        		$patient->firstname 		= $patientData['firstname'];
	            $patient->lastname  		= $patientData['lastname'];
	            $patient->save();
        	endif;
        else:
        	$patient->healthcompany_id 	= $request->input('healthcompany_id');
    		$patient->firstname 		= $patientData['firstname'];
            $patient->lastname  		= $patientData['lastname'];
            $patient->save();
       	endif;

       	$paymentData = $request->input('payment');

        $payment = new Payment;
        $payment->branch_id 			= $box->branch_id;
        $payment->box_id 				= $box->id;
        $payment->boxopened_id 			= $openedPivotBox->id;
        $payment->seller_id 			= $user->id;
        $payment->doctor_id 			= $request->input('doctor_id', null);
        $payment->patient_id 			= $patient->id;

        $payment->healthcompany_id 		= $request->input('healthcompany_id');
        $payment->paymentable_id 		= $patient->id;
        $payment->paymentable_type		= Patient::class;

        $payment->paymentdate 			= new Carbon;
        $payment->subtotal 				= $this->issetOrZero($paymentData['subtotal']);
        $payment->discount_amount		= $this->issetOrZero($paymentData['discount']);
        $payment->discount_reason		= $this->issetOrNull($paymentData['discount_reason']);
        $payment->total					= $this->issetOrZero($paymentData['total']);
        $payment->copago_amount          = $this->issetOrZero($paymentData['copago_amount']);

        $payment->debit_amount			= $this->issetOrZero($paymentData['payment_debit_amount']);
        $payment->credit_amount			= $this->issetOrZero($paymentData['payment_credit_amount']);
        $payment->commercial_amount		= $this->issetOrZero($paymentData['payment_commercial_amount']);
        $payment->cash_amount			= $this->issetOrZero($paymentData['payment_cash_amount']);
        $payment->save();

        $products = $request->input('products', []);
        if(is_array($products)):
        	foreach($products as $product):
        		$payment->products()->attach($product['id'], [
        			'branch_id' 	=> $box->branch_id,
        			'box_id' 		=> $box->id,
        			'boxopened_id'  => $openedPivotBox->id,
        			'seller_id'		=> $user->id,
        			'doctor_id'		=> $request->input('doctor_id', null),
        			'patient_id'	=> $patient->id,
        			'price'			=> $this->issetOrZero($product['price']),
        			'quantity'		=> $this->issetOr($product['quantity'], 1),
        			'subtotal'		=> $this->issetOrZero($product['total']),
        			'discount'		=> 0,
        			'total'         => $this->issetOrZero($product['total']),

                    'paymentdate'           => new Carbon,
                    'healthcompany_id'      => $payment->healthcompany_id,
                    'health_amount'         => $this->issetOrZero($product['health_amount']),
                    'health_document'       => $this->issetOrNull($product['health_document']),
                    'insurancecompany_id'   => $this->issetOrNull($product['insurance_company']['id']),
                    'insurance_amount'      => $this->issetOrZero($product['insurance_amount']),
                    'insurance_document'    => $this->issetOrNull($product['insurance_document']),
                    'excessive_amount'      => $this->issetOrZero($product['excessive_amount']),
                    'excessive_document'    => $this->issetOrNull($product['excessive_document']),
                    'copago_amount'         => $this->issetOrZero($product['copago_amount']),
                    'copago_document'       => $this->issetOrNull($product['copago_document']),
        		]);
        	endforeach;
        endif;

		return [
			'data' => $payment,
			'success' => true
		];

	}

	function issetOrNull($value){
        return (isset($value) and $value !== null) ? $value : null;
	}

	function issetOrZero($value){
		return (isset($value) and $value !== null and is_numeric($value)) ? $value : 0;
	}

	function issetOr($value, $default = null){
		return (isset($value) and $value !== null) ? $value : $default;
	}

}
