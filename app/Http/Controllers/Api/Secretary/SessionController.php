<?php

namespace App\Http\Controllers\Api\Secretary;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\Branch;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SessionController extends Controller
{
    function askForOpenBox(Request $request){
        $password = md5($request->input('password'));

        $user = User::whereSecurePassword($password)->first();
        if(!$user)
            return AjaxResponse::fail("Clave segura no válida");

        $box = $user->openedPivotBox()->with('box', 'box.branch')->first();
        if($box)
            return AjaxResponse::success($box);

        return AjaxResponse::success(null);
    }

    function openBox(Request $request){
    	$password = md5($request->input('password'));
    	$branch_id = $request->input('branch_id');
    	$box_id = $request->input('paymentbox_id');

    	$user = User::whereSecurePassword($password)->first();
    	if(!$user)
    		return AjaxResponse::fail("Clave segura no válida");

    	if($user->openedboxes()->count() > 0)
    		return AjaxResponse::fail("Usted ya tiene una caja aperturada, primero debe cerrarla si desea aperturar otra caja.");

    	$branch = Branch::find($branch_id);
    	if(!$branch)
    		return AjaxResponse::fail("La sucursal no existe");

    	$box = $branch->paymentboxes()->find($box_id);
    	if(!$box)
    		return AjaxResponse::fail("La caja no existe");


    	$user->openedboxes()->attach($box, ['created_at' => new Carbon(), 'updated_at' => new Carbon(), 'opened_datetime' => new Carbon(), 'branch_id' => $branch->id]);

        $box = $user->openedPivotBox()->with('box', 'box.branch')->first();

    	return AjaxResponse::success($box);
    }

    function closeBox(Request $request){
    	$password = md5($request->input('password'));

    	$user = User::whereSecurePassword($password)->first();
    	if(!$user)
    		return AjaxResponse::fail("Clave segura no válida");

    	if($user->openedboxes()->count() == 0)
    		return AjaxResponse::fail("Usted no tiene una caja aperturada que cerrar.");

    	$box = $user->openedboxes()->first();
    	$box->pivot->closed_datetime = new Carbon;
    	$box->pivot->deleted_at = new Carbon;
    	$box->pivot->save();

    	return AjaxResponse::success($box);
    }
}
