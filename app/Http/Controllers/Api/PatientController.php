<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePatientRequest;
use App\Libs\AjaxResponse;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    function index(Request $request){
    	$rut = $request->input('rut');
    	if($rut != ''):
    		$patient = Patient::whereRut($rut)->first();
    		if($patient)
    			return AjaxResponse::make($patient);

    		return AjaxResponse::make(null, false, 'No existe un paciente con el rut consultado');
    	endif;

    	return AjaxResponse::make(Patient::orderBy('lastname')->get());
    }

    function store(StorePatientRequest $request){
        $patient = Patient::whereRut($request->input('rut'))->first();
        if(!$patient)
            $patient = new Patient;
        
        $patient->healthcompany_id = $request->input('healthcompany_id');
        $patient->rut = $request->input('rut');
        $patient->firstname = $request->input('firstname');
        $patient->lastname = $request->input('lastname');
        $patient->email = $request->input('email');
        $patient->phone = $request->input('phone');
        $patient->address = $request->input('address');
        $patient->birthdate = $request->input('birthdate');
        $patient->age= $request->input('age', 1);
        $patient->family_history = $request->input('family_history');
        $patient->save();

        return AjaxResponse::success($patient);
    }

    function update(StorePatientRequest $request, $id){
        $patient = Patient::find($id);
        if(!$patient)
            return AjaxResponse::fail('El paciente no existe.');

        $patient->healthcompany_id = $request->input('healthcompany_id');
        $patient->rut = $request->input('rut');
        $patient->firstname = $request->input('firstname');
        $patient->lastname = $request->input('lastname');
        $patient->email = $request->input('email');
        $patient->phone = $request->input('phone');
        $patient->address = $request->input('address');
        $patient->birthdate = $request->input('birthdate');
        $patient->age= $request->input('age', 1);
        $patient->family_history = $request->input('family_history');
        $patient->save();

        return AjaxResponse::success($patient);
    }
}
