<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\ComercialCompany;
use Illuminate\Http\Request;

class ComercialCompanyController extends Controller
{
    function index(Request $request){
    	return AjaxResponse::success(ComercialCompany::orderBy('name')->get());
    }
}
