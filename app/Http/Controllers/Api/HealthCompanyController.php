<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\HealthCompany;
use Illuminate\Http\Request;

class HealthCompanyController extends Controller
{
    function index(Request $request){
    	return AjaxResponse::success(HealthCompany::orderBy('order')->orderBy('name')->get());
    }
}
