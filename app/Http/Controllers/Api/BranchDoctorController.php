<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\Branch;
use Illuminate\Http\Request;

class BranchDoctorController extends Controller
{
    function index(Request $request, $branch_id){
    	$profile = $request->input('profile');
    	$branch = Branch::find($branch_id);
    	if(!$branch)
    		return AjaxResponse::make(null, false, "Sucursal no encontrada");

    	return AjaxResponse::make($branch->doctors()->whereProfile($profile)->get()->unique());
    }
}
