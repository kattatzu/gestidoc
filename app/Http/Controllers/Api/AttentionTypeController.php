<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\AttentionType;
use Illuminate\Http\Request;

class AttentionTypeController extends Controller
{
    function index(Request $request){
    	return AjaxResponse::make(AttentionType::orderBy('name')->get());
    }
}
