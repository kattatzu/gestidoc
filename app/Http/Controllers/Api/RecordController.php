<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRecordRequest;
use App\Libs\AjaxResponse;
use App\Models\Patient\Record;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    function store(StoreRecordRequest $request){
    	$doctorId = auth()->user()->id;

		$record = new Record;
        $record->patient_id = $request->input('patient_id');
    	$record->branch_id = $request->input('branch_id');
    	$record->doctor_id = $doctorId;
    	$record->booking_id = null;
    	$record->attentiondate = $request->input('attentiondate');
    	$record->save();

    	return AjaxResponse::success($record);
    }
}
