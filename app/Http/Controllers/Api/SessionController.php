<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    function index(Request $request){
    	return AjaxResponse::success($request->user());
    }
}
