<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\AjaxResponse;
use App\Models\Param\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    function index(Request $request){
    	$products = Product::select(
                'id', 'doctor_id', 'code', 'name', 'details', 'type', 'category', 'sub_category', 'options', 'price', 'exempt'
            )
    		->with(['prices' => function($query){
    			$query->select('product_id', 'company_id', 'price');
    		}])
            ->with('doctor')
            ->orderBy('type')
            ->orderBy('category')
    		->orderBy('code')
    		->get();

    	return AjaxResponse::success($products);
    }
}
