<?php
namespace App\Http\Controllers;

use App\Models\Globals\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function show($id, $filename){
    	$file = File::find($id);
    	if(!$file or $file->filename != $filename)
    		return "El archivo consultado no existe!";

    	if(!Storage::exists('uploads/' . $file->filename))
    		return "El archivo consultado no existe!";

    	return response()->file(storage_path('app/uploads/' . $file->filename));
    }

    public function download($id, $filename){
    	$file = File::find($id);
    	if(!$file or $file->filename != $filename)
    		return "El archivo consultado no existe!";

    	if(!Storage::exists('uploads/' . $file->filename))
    		return "El archivo consultado no existe!";

    	return response()->download(storage_path('app/uploads/' . $file->filename), $file->realname);
    }
}
