<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient.rut'       => 'required',
            'patient.firstname' => 'required',
            'patient.lastname'  => 'required',
            'payment.subtotal'  => 'required',
            'payment.total'     => 'required',
            'healthcompany_id'  => 'required',
            'password'          => 'required'
        ];
    }
}
