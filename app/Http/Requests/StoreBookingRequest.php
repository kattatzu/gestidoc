<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'healthcompany_id' => 'required',
            'branch_id' => 'required',
            'doctor_id' => 'required',
            'bookingdate' => 'required_if:bookingdates,null',
            'bookingdates' => 'required_if:bookingdate,null|array',
            'attentiontype_id' => 'required',
            'type' => 'required'
        ];
    }
}
