<?php

namespace App\Models\Doctor;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'doctors_schedules';
}
