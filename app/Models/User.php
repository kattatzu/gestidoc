<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden = ['password', 'remember_token', 'secure_password'];
    protected $table = 'users';

    function openedboxes(){return $this->belongsToMany('App\Models\Param\Branch\PaymentBox', 'global_paymentboxs_openings', 'user_id', 'box_id')->whereNull('global_paymentboxs_openings.deleted_at')->withPivot('id', 'opened_datetime');}
    function openedPivotBox(){return $this->hasMany('App\Models\Param\Branch\OpenedBox')->whereNull('deleted_at');}

    function isTraumatologo(){return ($this->profile == 'Traumatólogo');}
    function isKinesiologo(){return ($this->profile == 'Kinesiólogo');}
    function isRadiologo(){return ($this->profile == 'Radiólogo');}
    function isAdministrador(){return ($this->profile == 'Administrador');}
    function isAdministrativo(){return ($this->profile == 'Administrativo');}
    function isContador(){return ($this->profile == 'Contador');}
    function isSecretaria(){return ($this->profile == 'Secretaria');}
    function publicProfile(){
        return [
            'name' => $this->name,
            'profile' => $this->profile,
        ];
    }
}
