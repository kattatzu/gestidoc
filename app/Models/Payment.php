<?php

namespace App\Models;

use App\Models\Param\Branch;
use App\Models\Param\ComercialCompany;
use App\Models\Param\HealthCompany;
use App\Models\Param\Product;
use App\Models\Payment\Product as PaymentProduct;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'global_payments';
    protected $fillable = ['branch_id', 'box_id', 'boxopened_id', 'seller_id', 'doctor_id', 'patient_id', 'healthcompany_id', 'insurancecompany_id', 'commercialcompany_id', 'paymentdate', 'subtotal', 'discount_amount', 'discount_reason', 'total', 'health_amount', 'health_document', 'insurance_amount', 'insurance_document', 'copago_amount', 'copago_document', 'debit_amount', 'credit_amount', 'commercial_amount', 'cash_amount'];

    function branch(){return $this->belongsTo(Branch::class);}
    function healthcompany(){return $this->belongsTo(HealthCompany::class, 'healthcompany_id');}
    function commercialcompany(){return $this->belongsTo(ComercialCompany::class, 'commercialcompany_id');}
    function seller(){return $this->belongsTo(User::class, 'seller_id');}
    function products(){return $this->belongsToMany(Product::class, 'global_payments_products')->withPivot('health_amount', 'insurance_amount');}
    function productsPivot(){return $this->hasMany(PaymentProduct::class);}
}
