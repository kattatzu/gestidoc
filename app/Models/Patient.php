<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Patient extends Model
{
    use SoftDeletes;
    use Notifiable;
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'birthdate'];

    function getFullnameAttribute(){
        return $this->firstname . ' ' . $this->lastname;
    }
    function getFullnameInverseAttribute(){
        return $this->lastname . ' ' . $this->firstname;
    }

    function bookings(){
        return $this->hasMany('App\Models\Patient\Booking')->orderBy('bookingdate');
    }
}
