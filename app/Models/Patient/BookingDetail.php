<?php
namespace App\Models\Patient;
use App\Models\Param\Product;
use App\Models\Patient\Booking;
use App\Models\Patient\Record;
use Illuminate\Database\Eloquent\Model;
class BookingDetail extends Model
{
    protected $table = 'patients_bookings_details';
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'indication_date', 'treatment_date'];
    function bookings(){return $this->hasMany(Booking::class, 'kinedetails_id');}
    function product(){return $this->belongsTo(Product::class);}
    function records(){return $this->hasMany(Record::class);}
    function files(){return $this->morphMany('App\Models\Globals\File', 'fileable')->orderBy('created_at');}
}
