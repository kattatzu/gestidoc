<?php

namespace App\Models\Patient;

use App\Models\Doctor;
use App\Models\Patient\BookingDetail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $table = 'patients_bookings';
    protected $dates = ['bookingdate', 'created_at', 'updated_at', 'deleted_at'];

    function kinedetail(){return $this->belongsTo(BookingDetail::class, 'kinedetails_id');}
    function patient(){return $this->belongsTo('App\Models\Patient');}
    function branch(){return $this->belongsTo('App\Models\Param\Branch');}
    function doctor(){return $this->belongsTo('App\Models\Doctor');}
    function attentiontype(){return $this->belongsTo('App\Models\Param\AttentionType');}
    function healthcompany(){return $this->belongsTo('App\Models\Param\HealthCompany');}
    function payment(){return $this->morphOne('App\Models\Payment', 'paymentable');}
    function record(){return $this->hasOne('App\Models\Patient\Record');}

    function scopeFromToday($query){
        return $query->whereDate('bookingdate', Carbon::today()->format('Y-m-d'));
    }

    static function validateUniqueHour($branchId, $doctorId, $bookingDate){
    	$doctor = Doctor::find($doctorId);
    	if(!$doctor) return false;

    	$limit = $doctor->profile == 'Kinesiólogo' ? 4 : 1;

    	return (Booking::whereTypeAndBranchIdAndDoctorIdAndBookingdate('Normal', $branchId, $doctorId, $bookingDate)->count() < $limit);
    }
}
