<?php
namespace App\Models\Patient;
use App\Models\Patient\BookingDetail;
use Illuminate\Database\Eloquent\Model;
class Record extends Model
{
    protected $table = 'patients_records';

    protected $dates = ['attentiondate', 'created_at', 'updated_at', 'deleted_at'];

    function kine_details(){return $this->belongsTo(BookingDetail::class);}
    function patient(){return $this->belongsTo('App\Models\Patient');}
    function branch(){return $this->belongsTo('App\Models\Param\Branch');}
    function doctor(){return $this->belongsTo('App\Models\Doctor');}
    function booking(){return $this->belongsTo('App\Models\Patient\Booking');}
    function files(){return $this->morphMany('App\Models\Globals\File', 'fileable')->orderBy('created_at');}
}
