<?php

namespace App\Models;

use App\Libs\AjaxResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Doctor extends User
{
    function schedules(){return $this->hasMany('App\Models\Doctor\Schedule');}
    function bookings(){return $this->hasMany('App\Models\Patient\Booking')->orderBy('bookingdate');}
    function prices(){return $this->hasManyThrough('App\Models\Param\Product\Price', 'App\Models\Param\Product');}

    /**
     * Retorna el resumen de horas de atención y horas reservadas/libres
     * en un periodo de tiempo
     * @param  integer $branch_id       Id de la sucursal
     * @param  Carbon\Carbon $dayBegin  Fecha de inicio a consultar
     * @param  Carbon\Carbon $dayEnd    Fecha de término a consultar
     * @return array                    Slot de trabajo, reservadas / libres
     */
    function resumeHoursBetween($branch_id, $dayBegin, $dayEnd){
    	$hours = [];
    	$tmpDay = $dayBegin;

    	while($tmpDay < $dayEnd):
	    	if($this->schedules()->whereBranchIdAndDayofweek($branch_id, $tmpDay->dayOfWeek)->count() > 0):
	    		$hours[$tmpDay->format('Y-m-d')] = [
                    'date' => $tmpDay->format('Y-m-d'),
                    'hours' => $this->resumeHours($branch_id, $tmpDay),
                    'extra' => $this->extraHours($branch_id, $tmpDay),
                ];
	    		
	    	endif;

	    	$tmpDay->addDay();
	    endwhile;

	    return $hours;
    }

    /**
     * Retorna el resumen de horas de atención y horas reservadas/libres
     * @param  integer $branch_id 		Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array                    Slot de trabajo, reservadas / libres
     */
    function resumeHours($branch_id, $day){
    	$hours = [];
    	$workingHours = $this->workingHours($branch_id, $day);    	

    	foreach($workingHours as $hour):
            if($this->profile == 'Kinesiólogo'):
                $bookingHours = $this->bookingHoursKine($branch_id, $day);
                $hours[$hour] = [
                    'free' => (!array_key_exists($hour, $bookingHours) or ($bookingHours[$hour] < 4)),
                    'hour' => $hour
                ];
            else:
                $bookingHours = $this->bookingHours($branch_id, $day);
        		$hours[$hour] = [
                    'free' => !in_array($hour, $bookingHours),
                    'hour' => $hour
                ];
            endif;
    	endforeach;

    	return $hours;
    }

    /**
     * Retorna el resumen de horas de trabajo con las horas reservadas
     * en un periodo de tiempo
     * @param  integer $branch_id       Id de la sucursal
     * @param  Carbon\Carbon $dayBegin  Fecha de inicio a consultar
     * @param  Carbon\Carbon $dayEnd    Fecha de término a consultar
     * @return array                    Slot de trabajo, reservadas / libres
     */
    function dailyHoursBetween($branch_id, $dayBegin, $dayEnd){
        $hours = [];
        $tmpDay = $dayBegin;

        while($tmpDay < $dayEnd):
            if($this->schedules()->whereBranchIdAndDayofweek($branch_id, $tmpDay->dayOfWeek)->count() > 0):
                $hours[] = [
                    'date' => $tmpDay->format('Y-m-d'),
                    'hours' => $this->dailyHours($branch_id, $tmpDay),
                    'extra' => $this->extraHoursModel($branch_id, $tmpDay),
                ];
                
            endif;

            $tmpDay->addDay();
        endwhile;

        return $hours;
    }

    function dailyHoursBetweenKine($branch_id, $dayBegin, $dayEnd){
        $hours = [];
        $tmpDay = $dayBegin;

        while($tmpDay < $dayEnd):
            if($this->schedules()->whereBranchIdAndDayofweek($branch_id, $tmpDay->dayOfWeek)->count() > 0):
                $hours[] = [
                    'date' => $tmpDay->format('Y-m-d'),
                    'hours' => $this->dailyHoursKine($branch_id, $tmpDay)
                ];
                
            endif;

            $tmpDay->addDay();
        endwhile;

        return $hours;
    }

    /**
     * Retorna el resumen de horas de trabajo con las horas reservadas
     * @param  integer $branch_id         Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array                    Slot de trabajo, reservadas / libres
     */
    function dailyHours($branch_id, $day){
        $dailyHours = collect();

        $workingHours = collect($this->workingHours($branch_id, $day))->mapWithKeys(function ($item) {
            return [[
                'bookingdate' => $item,
                'reserved' => false
            ]];
        });
        
        $bookingHours = collect($this->bookingHoursModel($branch_id, $day))->map(function($item){
            $item['reserved'] = true;
            return $item;
        });

        $workingHours->each(function($wHour) use ($bookingHours, $dailyHours){
            
            $bHour = $bookingHours->where('bookingdate', $wHour['bookingdate'])->first();

            if($bHour):
                $dailyHours[] = $bHour;
            else:
                $dailyHours[] = $wHour;
            endif;
        });

        return $dailyHours->sortBy('bookingdate');
    }

    function dailyHoursKine($branch_id, $day){
        $dailyHours = collect();

        $workingHours = collect($this->workingHours($branch_id, $day));
        $bookingHours = collect($this->bookingHoursModel($branch_id, $day));

        $workingHours->each(function($workingHour) use ($bookingHours, $dailyHours){
            
            $bHours = $bookingHours->filter(function($hour) use ($workingHour){
                return $hour['bookingdate']  == $workingHour;
            })->values();

            $dailyHours->push([
                'bookingdate' => $workingHour,
                'bookings' => $bHours,
                'bookings_number' => $bHours->count()
            ]);
        });

        return $dailyHours->sortBy('bookingdate');
    }

    /**
     * Retorna los slots disponibles
     * @param  integer $branch_id 		Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array                    Slot disponibles
     */
    function freeHours($branch_id, $day){
    	$workingHours = $this->workingHours($branch_id, $day);

        if($this->profile == 'Kinesiólogo'):
            $bookingHours = collect($this->bookingHoursKine($branch_id, $day))->filter(function ($value, $key) {
                return $value >= 4;
            })->toArray();
        
            $freeHours = collect($workingHours)->filter(function($value, $key) use ($bookingHours){
                return !array_key_exists($value, $bookingHours);
            });
        else:
            $bookingHours = $this->bookingHours($branch_id, $day);
            $freeHours = collect($workingHours)->diff($bookingHours)->flatten();
        endif;

    	return $freeHours;
    }

    /**
     * Retorna los slots reservados
     * @param  integer $branch_id 		Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array     				Slot reservados
     */
    function bookingHours($branch_id, $day){
    	return $this->bookings()
            ->whereTypeAndBranchId('Normal', $branch_id)
            ->whereDate('bookingdate', '>=', $day->format('Y-m-d 00:00:00'))
            ->whereDate('bookingdate', '<=', $day->format('Y-m-d 23:59:59'))
            ->orderBy('bookingdate')
            ->get()
            ->pluck('bookingdate')
            ->toArray();
    }

    function bookingHoursModel($branch_id, $day){
        return $this->bookings()
            ->with('patient', 'healthcompany', 'attentiontype', 'record')
            ->whereTypeAndBranchIdAndStatus('Normal', $branch_id, 'Reservada')
            ->whereDate('bookingdate', '>=', $day->format('Y-m-d 00:00:00'))
            ->whereDate('bookingdate', '<=', $day->format('Y-m-d 23:59:59'))
            ->orderBy('bookingdate')
            ->get()
            ->toArray();
    }

    function allBookingForDate($branch_id, $day){
        return $this->bookings()
            ->with('patient', 'healthcompany', 'attentiontype', 'record')
            ->whereBranchIdAndStatus($branch_id, 'Reservada')
            ->whereDate('bookingdate', $day->format('Y-m-d'))
            ->orderBy('bookingdate')
            ->get()
            ->toArray();
    }

    /**
     * Retorna los slots reservados de Kinesiología
     * @param  integer $branch_id       Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array                    Slot reservados
     */
    function bookingHoursKine($branch_id, $day){
        return DB::table('patients_bookings')
            ->select('bookingdate', DB::raw('count(*) as total'))
            ->whereDoctorId($this->id)
            ->whereTypeAndBranchId('Normal', $branch_id)
            ->whereBetween('bookingdate', [$day->format('Y-m-d 00:00:00'), $day->format('Y-m-d 23:59:59')])
            ->groupBy('bookingdate')
            ->orderBy('bookingdate')
            ->get()
            ->pluck('total', 'bookingdate')
            ->toArray();
    }

    /**
     * Retorna las horas extras
     * @param  integer $branch_id       Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array                    Horas extras
     */
    function extraHours($branch_id, $day){
        return $this->bookings()
                ->whereTypeAndBranchId('Extra', $branch_id)
                ->whereDate('bookingdate', '>=', $day->format('Y-m-d 00:00:00'))
                ->whereDate('bookingdate', '<=', $day->format('Y-m-d 23:59:59'))
                ->orderBy('bookingdate')
                ->get()->pluck('bookingdate')->toArray();
    }

    /**
     * Retorna las horas extras
     * @param  integer $branch_id       Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array                    Horas extras
     */
    function extraHoursModel($branch_id, $day){
        return $this->bookings()
                ->with('patient', 'healthcompany', 'attentiontype')
                ->whereTypeAndBranchId('Extra', $branch_id)
                ->whereDate('bookingdate', '>=', $day->format('Y-m-d 00:00:00'))
                ->whereDate('bookingdate', '<=', $day->format('Y-m-d 23:59:59'))
                ->orderBy('bookingdate')
                ->get();
    }

    /**
     * Genera los slot de atención según el agendamiento del doctor
     * @param  integer $branch_id 		Id de la sucursal
     * @param  Carbon\Carbon $day       Fecha a consultar
     * @return array     				Slot de trabajo
     */
	function workingHours($branch_id, $day){
		$hours = [];

		$schedule = $this->schedules()->whereBranchIdAndDayofweek($branch_id, $day->dayOfWeek)->first();
		if(!$schedule)
			return $hours;

		$hours = $this->generateHours($day, $schedule->begin, $schedule->end, $schedule->break_begin, $schedule->break_end, $schedule->slots_minutes);

		return $hours;
	}

	/**
	 * Genera los slot de atención según el agendamiento del doctor
	 * @param  string $begin  Hora de inicio
	 * @param  string $end    Hora de término
	 * @param  string $bbegin Hora de Inicio del break
	 * @param  string $bend   Hora de término del break
	 * @param  string $slot   Minutos de duración de cada slot
	 * @return array         	Slot de trabajo
	 */
	function generateHours($day, $begin, $end, $bbegin, $bend, $slot){
		$hours = [];
		$tmpBBegin = null;
		$tmpBEnd = null;

		$tmpBegin = Carbon::createFromFormat("Y-m-d H:i", $day->format("Y-m-d ") . $begin);
		if($bbegin != '' and $bend != ''):
			$tmpBBegin = Carbon::createFromFormat("Y-m-d H:i", $day->format("Y-m-d ") . $bbegin);
			$tmpBEnd = Carbon::createFromFormat("Y-m-d H:i", $day->format("Y-m-d ") . $bend);
		endif;
		$tmpEnd = Carbon::createFromFormat("Y-m-d H:i", $day->format("Y-m-d ") . $end);

		$ttmpEnd = ($tmpBBegin === null) ? $tmpEnd : $tmpBBegin;
		while($tmpBegin <= $ttmpEnd):
			$hours[] = $tmpBegin->format('Y-m-d H:i:00');

			$tmpBegin->addMinutes($slot);
		endwhile;

		if($tmpBBegin !== null):
			$tmpBegin = $tmpBEnd;
			while($tmpBegin <= $tmpEnd):
				$hours[] = $tmpBegin->format('Y-m-d H:i:00');

				$tmpBegin->addMinutes($slot);
			endwhile;
		endif;

		return $hours;
	}

}
