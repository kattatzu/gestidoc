<?php

namespace App\Models\Param\Branch;

use Illuminate\Database\Eloquent\Model;

class OpenedBox extends Model
{
    protected $table = 'global_paymentboxs_openings';
    protected $appends = [
    	'subtotal', 'discounts', 'total', 'health_total', 'insurance_total', 'excessive_total',
    	'copago_total', 'debit_total', 'credit_total', 'commercial_total',
    	'cash_total'
    ];

    function box(){return $this->belongsTo('App\Models\Param\Branch\PaymentBox');}
    function payments(){return $this->hasMany('App\Models\Payment', 'boxopened_id');}

    function getSubtotalAttribute(){
    	return (int) $this->payments()->sum('subtotal');
    }

    function getDiscountsAttribute(){
    	return (int) $this->payments()->sum('discount_amount');
    }

    function getTotalAttribute(){
    	return (int) $this->payments()->sum('total');
    }

    function getHealthTotalAttribute(){
        $sum = 0;
        foreach($this->payments as $payment):
    	   $sum += (int) $payment->products()->sum('health_amount');
        endforeach;
        return $sum;
    }

    function getInsuranceTotalAttribute(){
        $sum = 0;
        foreach($this->payments as $payment):
           $sum += (int) $payment->products()->sum('insurance_amount');
        endforeach;
        return $sum;
    }

    function getExcessiveTotalAttribute(){
        $sum = 0;
        foreach($this->payments as $payment):
           $sum += (int) $payment->products()->sum('excessive_amount');
        endforeach;
        return $sum;
    }

    function getCopagoTotalAttribute(){
    	return (int) $this->payments()->sum('copago_amount');
    }

    function getDebitTotalAttribute(){
    	return (int) $this->payments()->sum('debit_amount');
    }

    function getCreditTotalAttribute(){
    	return (int) $this->payments()->sum('credit_amount');
    }

    function getCommercialTotalAttribute(){
    	return (int) $this->payments()->sum('commercial_amount');
    }

    function getCashTotalAttribute(){
    	return (int) $this->payments()->sum('cash_amount');
    }
}
