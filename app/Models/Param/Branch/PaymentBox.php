<?php

namespace App\Models\Param\Branch;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PaymentBox extends Model
{
    use SoftDeletes;
    protected $table = 'params_branches_paymentboxes';

    function payments(){return $this->hasMany('App\Models\Payment', 'box_id');}
    function branch(){return $this->belongsTo('App\Models\Param\Branch');}
}
