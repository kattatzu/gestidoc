<?php

namespace App\Models\Param;

use App\Models\Doctor;
use App\Models\Param\Product\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
    use SoftDeletes;
    protected $table = 'params_products';

    function prices(){return $this->hasMany(Price::class);}
    function doctor(){return $this->belongsTo(Doctor::class);}
    function subproducts(){return $this->belongsToMany(Product::class, 'params_products_subproducts', 'product_id', 'subproduct_id');}
}
