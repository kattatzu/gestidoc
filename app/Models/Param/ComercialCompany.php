<?php

namespace App\Models\Param;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ComercialCompany extends Model
{
    use SoftDeletes;
    protected $table = 'params_comercial_companies';
}
