<?php

namespace App\Models\Param;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HealthCompany extends Model
{
    use SoftDeletes;
    protected $table = 'params_health_companies';

    function doctors(){return $this->belongsToMany('App\Models\Doctor', 'doctors_health_companies', 'company_id')->orderBy('name');}
}
