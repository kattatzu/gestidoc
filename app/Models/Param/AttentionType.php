<?php
namespace App\Models\Param;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AttentionType extends Model
{
    use SoftDeletes;
    protected $table = 'params_attentiontypes';
}
