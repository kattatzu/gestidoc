<?php

namespace App\Models\Param\Product;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'params_products_prices';
}
