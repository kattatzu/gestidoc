<?php

namespace App\Models\Param;

use App\Models\Doctor;
use App\Models\Doctor\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;
    protected $table = 'params_branches';

    function paymentboxes(){return $this->hasMany('App\Models\Param\Branch\PaymentBox');}
    function doctors(){return $this->belongsToMany('App\Models\Doctor', 'doctors_schedules')->distinct();}

    function traumatologists(){return $this->belongsToMany('App\Models\Doctor', 'doctors_schedules')->whereProfile('Traumatólogo')->orderBy('name')->distinct();}
    function kinesiologists(){return $this->belongsToMany('App\Models\Doctor', 'doctors_schedules')->whereProfile('Kinesiólogo')->orderBy('name')->distinct();}
    function radiologists(){return $this->belongsToMany('App\Models\Doctor', 'doctors_schedules')->whereProfile('Radiólogo')->orderBy('name')->distinct();}

    function lastFreeHours($day, $profile){
    	$hours = collect([]);

        $doctors = Doctor::whereProfile($profile)->get();
    	foreach($doctors as $doctor):
    		$doctorHours = $doctor->freeHours($this->id, $day);

            foreach($doctorHours as $dHour):
                $hours->push([
                    'hour' => $dHour,
                    'doctor' => $doctor
                ]);
            endforeach;
    	endforeach;

        $sortedHours = $hours->sortBy('hour');

    	return $sortedHours->values()->all();
    }
}
