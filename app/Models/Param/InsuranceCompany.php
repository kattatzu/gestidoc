<?php

namespace App\Models\Param;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class InsuranceCompany extends Model
{
    use SoftDeletes;
    protected $table = 'params_insurance_companies';
}
