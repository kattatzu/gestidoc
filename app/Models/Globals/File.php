<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

	protected $table = 'global_files';
	protected $fillable = ['author_id', 'filename', 'realname', 'size', 'mime'];
	
    function fileable(){
    	return $this->morphTo();
    }
}
