<?php

namespace App\Models\Payment;

use App\Models\Param\Branch;
use App\Models\Param\HealthCompany;
use App\Models\Param\InsuranceCompany;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'global_payments_products';
    protected $dates = ['paymentdate'];

    function branch(){return $this->belongsTo(Branch::class);}
    function healthcompany(){return $this->belongsTo(HealthCompany::class, 'healthcompany_id');}
    function insurancecompany(){return $this->belongsTo(InsuranceCompany::class, 'insurancecompany_id');}
    function seller(){return $this->belongsTo(User::class, 'seller_id');}
}
