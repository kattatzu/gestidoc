<?php
namespace App\Libs;

class AjaxResponse{
	
	static function make($data = null, $success = true, $message = null){

		if(request()->has('callback'))
			return response()
	            ->json([
					'success' => $success,
					'data' => $data,
					'message' => $message
				])
				->withCallback(request()->input('callback'));

		return [
			'success' => $success,
			'data' => $data,
			'message' => $message
		];
	}

	static function success($data){
		return self::make($data);
	}

	static function fail($message = null, $data = null){
		return self::make($data, false, $message);
	}

}