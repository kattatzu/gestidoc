<?php
Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'HomeController@index');

	Route::group(['prefix' => 'secretary', 'namespace' => 'Secretary', 'middleware' => 'profile:Secretaria'], function(){
		Route::get('/', 'HomeController@index');
	});
	Route::group(['prefix' => 'traumatologo', 'namespace' => 'Traumatologist', 'middleware' => 'profile:Traumatólogo'], function(){
		Route::get('/', 'HomeController@index');

		Route::get('create/{id}', 'RecordController@createRecord');
		Route::post('printorder/{record}', 'RecordController@printOrder');
		Route::get('records/{record}/print', 'RecordController@print');
		Route::post('print-massive', 'RecordController@printMassive');
		Route::post('saveorder', 'RecordController@saveOrder');
		Route::resource('records', 'RecordController');
	});
	Route::group(['prefix' => 'kinesiologo', 'namespace' => 'Kinesiologist', 'middleware' => 'profile:Kinesiólogo'], function(){
		Route::get('/', 'HomeController@index');

		Route::get('create/{id}', 'RecordController@createRecord');
		Route::get('records/{record}/print', 'RecordController@print');
		Route::resource('records', 'RecordController');
	});


	Route::group(['prefix' => 'api', 'namespace' => 'Api'], function(){

		Route::get('session', 'SessionController@index');

		Route::get('branches/{id}/last-hours', 'BranchController@lastFreeHours');

		Route::resource('branches', 'BranchController');
		Route::resource('branches.paymentboxes', 'BranchPaymentBoxController');
		Route::resource('branches.doctors', 'BranchDoctorController');
		Route::resource('patients', 'PatientController');
		Route::resource('insurancecompanies', 'InsuranceCompanyController');
		Route::resource('comercialcompanies', 'ComercialCompanyController');
		Route::resource('attentiontypes', 'AttentionTypeController');
		Route::resource('payments', 'PaymentController');
		Route::resource('records', 'RecordController');

		Route::get('doctors/dailyhours', 'DoctorController@dailyHours');
		Route::get('doctors/dailyhoursbetween', 'DoctorController@dailyHoursBetween');
		Route::get('doctors/dailyhoursbetweenkine', 'DoctorController@dailyHoursBetweenKine');
		Route::get('doctors/resumehours', 'DoctorController@resumeHours');
		Route::get('doctors/resumehoursbetween', 'DoctorController@resumeHoursBetween');
		Route::get('doctors/allbookingfordate', 'DoctorController@allBookingForDate');

		Route::post('bookings/search', 'BookingController@search');
		Route::post('bookings/lock', 'BookingController@lock');
		Route::post('bookings/unlock', 'BookingController@unlock');
		Route::post('bookings/cancel', 'BookingController@cancel');
		Route::post('bookings/{booking}/patient-actitude', 'BookingController@setPatientActitude');
		Route::post('bookings/{booking}/receipt', 'BookingController@receipt');
		Route::resource('patients', 'PatientController');
		Route::resource('products', 'ProductController');

		Route::group(['prefix' => 'secretary', 'namespace' => 'Secretary', 'middleware' => 'profile:Secretaria'], function(){

			Route::group(['prefix' => 'session'], function(){

				Route::any('ask-for-open-box', 'SessionController@askForOpenBox');
				Route::any('open-box', 'SessionController@openBox');
				Route::any('close-box', 'SessionController@closeBox');

			});

		});
	});

	Route::group(['prefix' => 'administrador', 'namespace' => 'Admin'], function(){
		Route::get('/', 'HomeController@index');
		Route::resource('users', 'UserController');
		Route::resource('branches', 'BranchController');
		Route::resource('attentiontypes', 'AttentionTypeController');
		Route::resource('paymentboxes', 'PaymentBoxController');
		Route::resource('commercialcompanies', 'CommercialCompanyController');
		Route::resource('healthcompanies', 'HealthCompanyController');
		Route::resource('insurancecompanies', 'InsuranceCompanyController');
		Route::resource('products', 'ProductController');
		Route::resource('patients', 'PatientController');
		Route::resource('hourly', 'HourlyController');
	});

	Route::group(['prefix' => 'administrativo', 'namespace' => 'Administrative'], function(){
		Route::get('/', 'HomeController@index');
		Route::resource('sales-resume', 'SalesResumeController');
		Route::resource('sales-detailed', 'SalesDetailedController');
	});

	Route::get('download/{id}/{filename}', ['uses' => 'FileController@download', 'as' => 'files.download']);
	Route::get('showfile/{id}/{filename}', ['uses' => 'FileController@show', 'as' => 'files.show']);
});

Route::group(['prefix' => 'api', 'namespace' => 'Api'], function(){
	Route::get('doctors/freehours', 'DoctorController@freeHours');
	Route::get('doctors/{doctor}/freehours/{branch}/{date}', 'DoctorController@oldFreeHours');
	Route::resource('doctors', 'DoctorController');
	Route::resource('bookings', 'BookingController');
	Route::resource('healthcompanies', 'HealthCompanyController');
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
