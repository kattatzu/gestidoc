<div class="modal fade" tabindex="-1" role="dialog" id="receivePatientDialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Recepcionar Paciente</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-3">
						{{ Form::label('rut', 'Rut:', ['class' => 'control-label']) }}
						{{ Form::text('rut', '', ['v-model' => 'receivePatientDialog.search.rut', 'class' => 'form-control', 'placeholder' => 'ej. 16023817-8', 'maxlength' => '10']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('name', 'Nombre:', ['class' => 'control-label']) }}
						{{ Form::text('name', '', ['v-model' => 'receivePatientDialog.search.name', 'class' => 'form-control', 'placeholder' => 'ej. Jorge Rojas']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('bookingdate', 'Fecha:', ['class' => 'control-label']) }}
						{{ Form::text('bookingdate', '', ['v-model' => 'receivePatientDialog.search.bookingdate', 'class' => 'form-control', 'id' => 'bookingReceivePatientDatepicker']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('branch_id', 'Sucursal:', ['class' => 'control-label']) }}
						<select class="form-control" v-model="receivePatientDialog.search.branch_id">
							<option :value="null">-seleccione-</option>
							<option :value="branch.id" v-for="branch in branches">@{{ branch.name }}</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						{{ Form::label('doctor_id', 'Doctor:', ['class' => 'control-label']) }}
						<select class="form-control" v-model="receivePatientDialog.search.doctor_id">
							<option :value="null">-seleccione-</option>
							<option :value="doctor.id" v-for="doctor in allDoctors">@{{ doctor.name }}</option>
						</select>
					</div>
					<div class="col-sm-3 col-sm-offset-6">
						<br>
						<button type="button" class="btn btn-primary btn-block">
							<i class="fa fa-search"></i> Buscar
						</button>
					</div>
				</div>
				<hr>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Rut</th>
							<th>Nombre</th>
							<th>Fecha/Hora</th>
							<th>Doctor</th>
							<th>Sucursal</th>
							<th>Especialidad</th>
							<th>Tipo de atención</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>12345768-9</td>
							<td>Jose Eduardo Rios</td>
							<td>29/12 15:30</td>
							<td>Dr. Mondaca</td>
							<td>Viña del Mar</td>
							<td>Kinesiología</td>
							<td>Consulta</td>
							<td class="text-center">
								<a href="#" @click="openReceivePatientSetHour(1)" class="btn btn-primary btn-sm">
									<i class="fa fa-circle-o"></i>
								</a>
							</td>
						</tr>
						<tr class="warning">
							<td>12345768-9</td>
							<td>Jose Eduardo Rios</td>
							<td>29/12 15:30</td>
							<td>Dr. Mondaca</td>
							<td>Viña del Mar</td>
							<td>Kinesiología</td>
							<td>Consulta</td>
							<td class="text-center">
								<a href="#" @click="openReceivePatientSetHour(1)" class="btn btn-primary btn-sm">
									<i class="fa fa-circle"></i>
								</a>
							</td>
						</tr>
						<tr>
							<td>12345768-9</td>
							<td>Jose Eduardo Rios</td>
							<td>29/12 15:30</td>
							<td>Dr. Mondaca</td>
							<td>Viña del Mar</td>
							<td>Kinesiología</td>
							<td>Consulta</td>
							<td class="text-center">
								<a href="#" @click="openReceivePatientSetHour(1)" class="btn btn-primary btn-sm">
									<i class="fa fa-circle-o"></i>
								</a>
							</td>
						</tr>
						<tr>
							<td>12345768-9</td>
							<td>Jose Eduardo Rios</td>
							<td>29/12 15:30</td>
							<td>Dr. Mondaca</td>
							<td>Viña del Mar</td>
							<td>Kinesiología</td>
							<td>Consulta</td>
							<td class="text-center">
								<a href="#" @click="openReceivePatientSetHour(1)" class="btn btn-primary btn-sm">
									<i class="fa fa-circle-o"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" :disabled="!receivePatientDialog.selectedHour">Continuar</button>
			</div>
		</div>
	</div>
</div>