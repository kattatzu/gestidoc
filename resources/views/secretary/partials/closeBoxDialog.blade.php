<div class="modal fade" tabindex="-1" role="dialog" id="closeBoxDialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Cerrar Caja {{ app('session')->get('box-opened-code') }}</h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro de cerrar está caja?</p>
				<div class="row">
					<div class="col-sm-12">
						{{ Form::label('password', 'Clave Segura:', ['class' => 'control-label']) }}
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							{{ Form::password('password', ['class' => 'form-control', 'v-model' => 'closeBoxDialog.password', 'placeholder' => 'Ingrese su clave segura para confirmar']) }}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
				<button @click.prevent="confirmCloseBox()" type="button" class="btn btn-primary" :disabled="closeBoxDialog.password === null || closeBoxDialog.password == ''">Confirmar</button>
			</div>
		</div>
	</div>
</div>