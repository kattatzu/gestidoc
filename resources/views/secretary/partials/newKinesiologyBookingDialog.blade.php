<div v-if="newKineBookingDialog" class="modal fade" tabindex="-1" role="dialog" id="newKinesiologyBookingDialog">
	<div class="modal-dialog modal-xlg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Nueva Reserva de Hora Kinesiológica 
					[
						Sucursal <b>@{{ (newKineBookingDialog.currentBranch ? newKineBookingDialog.currentBranch.name : '') }}</b> -
						<b>@{{ (newKineBookingDialog.currentDoctor ? newKineBookingDialog.currentDoctor.name : '') }}</b>
					]
				</h4>
			</div>
			<div class="modal-body">
				<transition name="fade">
					<div v-show="alert.message != ''" :class="alert.alertClass">
						<i :class="alert.icon"></i> @{{ alert.message }}
					</div>
				</transition>

				<!-- Step 1 -->
				<div v-show="newKineBookingDialog.step == 1">
					<div class="row" style="margin: 30px 0;">
						<div class="col-sm-3 col-sm-offset-2">
							<button @click.prevent="bookingKineSelectMode('doctor')" type="button" class="btn btn-primary btn-lg btn-block">
								<br>
								<i class="fa fa-user-md"></i> Hora por Kinesiólogo
								<br>&nbsp;
							</button>
						</div>
						<div class="col-sm-2 text-center">
							<br><br>
							ú
						</div>
						<div class="col-sm-3">
							<button @click.prevent="bookingKineSelectMode('hour')" type="button" class="btn btn-primary btn-lg btn-block">
								<br>
								<i class="fa fa-clock-o"></i> Hora más cercana
								<br>&nbsp;
							</button>
						</div>
					</div>
				</div>

				<!-- Step 2 -->
				<div v-show="newKineBookingDialog.step == 2">
					<div class="row">
						<div v-if="newKineBookingDialog.mode == 'doctor'">
							<div class="col-sm-3">
								<div class="control-group">
									<label class="control-label">Sucursal</label>
									<select class="form-control input-lg" v-model="newKineBookingDialog.currentBranch" @change="bookingKineSelectBranch()" :disabled="branches.length == 0">
										<option :value="null" v-if="branches.length > 0">-seleccione-</option>
										<option :value="null" v-if="branches.length == 0">cargando...</option>
										<option v-for="branch in branches" :value="branch">@{{ branch.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Kinesiólogo</label>
									<select class="form-control input-lg" v-model="newKineBookingDialog.currentDoctor" @change="bookingKineSelectDoctor()" :disabled="newKineBookingDialog.branch_id == null || doctors.length == 0">
										<option :value="null" v-if="doctors.length > 0">-seleccione-</option>
										<option :value="null" v-if="newKineBookingDialog.branch_id != null && doctors.length == 0">cargando doctores...</option>
										<option v-for="doctor in doctors" :value="doctor">@{{ doctor.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Fecha</label>
									<div class="datepicker" id="bookingKineCalendar"></div>
								</div>
							</div>
							<div class="col-sm-7" style="max-height: 400px; overflow: scroll;">
								<div class="table-responsive">
									<table v-show="!newKineBookingDialog.loading" class="table table-bordered">
										<thead>
											<tr>
												<th v-for="day in freehours" class="text-center" style="width: 16.5%;">
													@{{ moment(day.date).format("ddd DD") }}
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td v-for="day in freehours" style="vertical-align: top; width: 16.5%;">
													<button @click="bookingKineSelectTime(time.hour)" v-for="time in day.hours" :class="{'btn': true, 'btn-success': time.free, 'btn-primary': !time.free, 'btn-block': true, 'active': inArray(time.hour, newKineBookingDialog.dates)}" :disabled="!time.free">
														@{{ moment(time.hour).format('HH:mm') }}
													</button>
												</td>
											</tr>
										</tbody>
									</table>

									<div v-show="newKineBookingDialog.loading" style="padding-top: 120px;" class="text-center">
										<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
									</div>

									<div v-show="!newKineBookingDialog.currentBranch || !newKineBookingDialog.currentDoctor || !newKineBookingDialog.date" style="padding: 80px 0;" class="text-center">
										<i class="fa fa-info-circle" style="font-size: 120px; color: #ddd;"></i><br>
										<p class="lead">
											<i class="fa fa-long-arrow-left"></i> Seleccione los filtros<br> para encontrar horas disponibles.
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Horas a agendar (@{{ newKineBookingDialog.dates.length }})</label>
								<ul class="list-group">
									<transition-group name="fade">
									<li v-for="bdate in newKineBookingDialog.dates" v-bind:key="bdate" class="list-group-item">
										@{{ moment(bdate).format("DD MMM HH:mm") }}

										<a href="#" @click.prevent="bookingKineRemoveTime(bdate)" class="pull-right">&times;</a>
									</li>
									</transition-group>
								</ul>
							</div>
						</div>
						<div v-if="newKineBookingDialog.mode == 'hour'">
							<div class="col-sm-3">
								<div class="control-group">
									<label class="control-label">Sucursal</label>
									<select class="form-control input-lg" v-model="newKineBookingDialog.currentBranch" @change="bookingKineSelectBranch()" :disabled="branches.length == 0">
										<option :value="null" v-if="branches.length > 0">-seleccione-</option>
										<option :value="null" v-if="branches.length == 0">cargando...</option>
										<option v-for="branch in branches" :value="branch">@{{ branch.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Fecha</label>
									<div class="datepicker" id="bookingKineCalendar2"></div>
								</div>
							</div>
							<div class="col-sm-9" style="max-height: 400px; overflow: scroll;">
								<table v-show="!newKineBookingDialog.loading" class="table table-bordered">
									<thead>
										<tr>
											<th width="100px">Hora</th>
											<th>Doctor</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="time in freehours">
											<td width="100px">
												<button @click="bookingKineSelectTime(time.hour, time.doctor)" :class="{'btn': true, 'btn-success': true, 'btn-block': true, 'active': (time.doctor.id == newKineBookingDialog.doctor_id && inArray(time.hour, newKineBookingDialog.dates))}">
													<i class="fa fa-clock-o"></i> @{{ moment(time.hour).format('HH:mm') }}
												</button>
											</td>
											<td><i class="fa fa-user-md"></i> @{{ time.doctor.name }}</td>
										</tr>
									</tbody>
								</table>

								<div v-show="newKineBookingDialog.loading" style="padding-top: 120px;" class="text-center">
									<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
								</div>

								<div v-show="!newKineBookingDialog.currentBranch || !newKineBookingDialog.date" style="padding: 80px 0;" class="text-center">
									<i class="fa fa-info-circle" style="font-size: 120px; color: #ddd;"></i><br>
									<p class="lead">
										<i class="fa fa-long-arrow-left"></i> Seleccione los filtros<br> para encontrar horas disponibles.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Step 3 -->
				<div v-show="newKineBookingDialog.step == 3">
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">
									Rut Paciente:
								</label>
								<div class="input-group">
									<input type="text" maxlength="10" placeholder="ej. 16023817-8" class="form-control" v-model="newKineBookingDialog.patient.rut" @keyup.enter="bookingKineFindPatient()" :disabled="newKineBookingDialog.patient.id || newKineBookingDialog.patient.rut == 'PENDIENTE'">
									<span class="input-group-btn">
								        <button v-show="newKineBookingDialog.patient.id == null && newKineBookingDialog.patient.rut != 'PENDIENTE'" :disabled="newKineBookingDialog.patient.rut == '' || newKineBookingDialog.patient.rut == 'PENDIENTE' || newKineBookingDialog.patient.status == 'Buscando'" @click="bookingKineFindPatient()" class="btn btn-default" type="button">
								        	<i :class="{fa: true, 'fa-search': (newKineBookingDialog.patient.status != 'Buscando'), 'fa-refresh fa-spin fa-fw': (newKineBookingDialog.patient.status == 'Buscando')}"></i>
								        </button>
								        <button v-show="newKineBookingDialog.patient.id == null && newKineBookingDialog.patient.rut != 'PENDIENTE' && newKineBookingDialog.patient.status != 'Buscando'" :disabled="newKineBookingDialog.patient.id" @click="bookingKineSetNewPatient(true)" class="btn btn-default" type="button">
								        	<i class="fa fa-asterisk"></i>
								        </button> 
								        <button v-show="newKineBookingDialog.patient.id !== null || newKineBookingDialog.patient.rut == 'PENDIENTE'" @click="bookingKineResetPatient(true)" class="btn btn-default" type="button">
								        	<i class="fa fa-times"></i>
								        </button>
								    </span>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Nombres:</label>
								<input type="text" class="form-control" v-model="newKineBookingDialog.patient.firstname"  :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Apellidos:</label>
								<input type="text" class="form-control" v-model="newKineBookingDialog.patient.lastname"  :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Fecha de Nacimiento:</label>
								<input type="text" class="form-control" id="bookingKineBirthdate" placeholder="dd/mm/aaaa" v-model="newKineBookingDialog.patient.birthdate" :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Teléfono:</label>
								<input type="text" class="form-control" v-model="newKineBookingDialog.patient.phone"  :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Email:</label>
								<input type="text" class="form-control" v-model="newKineBookingDialog.patient.email"  :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Dirección:</label>
								<input type="text" class="form-control" v-model="newKineBookingDialog.patient.address"  :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Previsión:</label>
								<select class="form-control" v-model="newKineBookingDialog.healthcompany_id" :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
									<option :value="null">-seleccione-</option>
									<option v-for="company in healthcompanies" :value="company.id">@{{ company.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Tipo de Atención:</label>
								<select class="form-control" v-model="newKineBookingDialog.attentiontype_id" :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
									<option :value="null">-seleccione-</option>
									<option v-for="atype in attentiontypes" :value="atype.id">@{{ atype.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Observaciones:</label>
								<input type="text" class="form-control" v-model="newKineBookingDialog.observations"  :disabled="newKineBookingDialog.patient.status == 'Pendiente' || newKineBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
				</div>

				<!-- Step 4 -->
				<div v-show="newKineBookingDialog.step == 4">
					<div class="text-center" v-if="newKineBookingDialog.booking">
						<span style="font-size: 120px; color: green;"><i class="fa fa-check-circle"></i></span>

						<h1>Hora(s) Kinesiológica(s) Reservada(s)</h1>
						
						<p class="lead">
							@{{ newKineBookingDialog.booking.success ? 'La o las horas fueron agendadas correctamente, el detalle es el siguiente:' : 'Algunas horas ya se encuentran copadas, por favor revise el siguiente detalle e intente con nuevas fechas:' }}
						</p>

						<table v-if="newKineBookingDialog.booking" class="table table-bordered">
							<thead>
								<tr>
									<td>Fecha</td>
									<td>Hora</td>
									<td>Doctor</td>
									<td>Sucursal</td>
									<td>Estado Reserva</td>
									<td>Código Reserva</td>
								</tr>
							</thead>
							<tbody>
								<tr v-for="booking in newKineBookingDialog.booking.data" :class="{success: booking.success, danger: !booking.success}">
									<td>@{{ moment(booking.data.bookingdate, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY") }}</td>
									<td>@{{ moment(booking.data.bookingdate, "YYYY-MM-DD HH:mm:ss").format("HH:mm") }}</td>
									<td>@{{ booking.success ? booking.data.doctor.name : '' }}</td>
									<td>@{{ booking.success ? booking.data.branch.name : '' }}</td>
									<td>@{{ booking.success ? 'Agendada' : 'No Agendada: Hora copada' }}</td>
									<td>@{{ booking.success ? booking.data.code : '' }}</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div v-show="!newKineBookingDialog.booking" style="padding: 80px 0;" class="text-center">
						<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
					<button v-show="newKineBookingDialog.step < 4" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button v-show="newKineBookingDialog.step < 4" :disabled="(newKineBookingDialog.step == 1)" @click.prevent="bookingKineGoBack()" type="button" class="btn btn-default">Volver</button>
				</div>

				<button v-show="newKineBookingDialog.step == 2" @click="bookingKineGoNextStep()" :disabled="!bookingKineCheckBookingDateStep" type="button" class="btn btn-primary">
					Continuar
				</button>

				<button v-show="newKineBookingDialog.step == 3" @click="bookingKineRegister()" :disabled="!bookingKineCheckDateForRegister" type="button" class="btn btn-primary">
					Confirmar
				</button>

				<button v-show="newKineBookingDialog.step == 4" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>