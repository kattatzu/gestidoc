<div v-if="newTraBookingDialog" class="modal fade" tabindex="-1" role="dialog" id="newTraumatologyBookingDialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Nueva Reserva de Hora Traumatológica 
					[
						Sucursal <b>@{{ (newTraBookingDialog.currentBranch ? newTraBookingDialog.currentBranch.name : '') }}</b> -
						<b>@{{ (newTraBookingDialog.currentDoctor ? newTraBookingDialog.currentDoctor.name : '') }}</b> -
						Fecha/Hora: <b>@{{ bookingTraDate ? bookingTraDate.format("ddd DD - HH:mm") : '' }}</b>
					]
				</h4>
			</div>
			<div class="modal-body">
				<transition name="fade">
					<div v-show="alert.message != ''" :class="alert.alertClass">
						<i :class="alert.icon"></i> @{{ alert.message }}
					</div>
				</transition>

				<!-- Step 1 -->
				<div v-show="newTraBookingDialog.step == 1">
					<div class="row" style="margin: 30px 0;">
						<div class="col-sm-3 col-sm-offset-2">
							<button @click.prevent="bookingTraSelectMode('doctor')" type="button" class="btn btn-primary btn-lg btn-block">
								<br>
								<i class="fa fa-user-md"></i> Hora por Doctor
								<br>&nbsp;
							</button>
						</div>
						<div class="col-sm-2 text-center">
							<br><br>
							ú
						</div>
						<div class="col-sm-3">
							<button @click.prevent="bookingTraSelectMode('hour')" type="button" class="btn btn-primary btn-lg btn-block">
								<br>
								<i class="fa fa-clock-o"></i> Hora más cercana
								<br>&nbsp;
							</button>
						</div>
					</div>
				</div>

				<!-- Step 2 -->
				<div v-show="newTraBookingDialog.step == 2">
					<div class="row">
						<div v-if="newTraBookingDialog.mode == 'doctor'">
							<div class="col-sm-3">
								<div class="control-group">
									<label class="control-label">Sucursal</label>
									<select class="form-control input-lg" v-model="newTraBookingDialog.currentBranch" @change="bookingTraSelectBranch()" :disabled="branches.length == 0">
										<option :value="null" v-if="branches.length > 0">-seleccione-</option>
										<option :value="null" v-if="branches.length == 0">cargando...</option>
										<option v-for="branch in branches" :value="branch">@{{ branch.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Doctor</label>
									<select class="form-control input-lg" v-model="newTraBookingDialog.currentDoctor" @change="bookingTraSelectDoctor()" :disabled="newTraBookingDialog.branch_id == null || doctors.length == 0">
										<option :value="null" v-if="doctors.length > 0">-seleccione-</option>
										<option :value="null" v-if="newTraBookingDialog.branch_id != null && doctors.length == 0">cargando doctores...</option>
										<option v-for="doctor in doctors" :value="doctor">@{{ doctor.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Fecha</label>
									<div class="datepicker" id="bookingTraCalendar"></div>
								</div>
							</div>
							<div class="col-sm-9" style="max-height: 400px; overflow: scroll;">
								<div class="table-responsive">
									<table v-show="!newTraBookingDialog.loading" class="table table-bordered">
										<thead>
											<tr>
												<th v-for="day in freehours" class="text-center" style="width: 16.5%;">
													@{{ moment(day.date).format("ddd DD") }}
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td v-for="day in freehours" style="vertical-align: top; width: 16.5%;">
													<button @click="bookingTraSelectTime(time.hour)" v-for="time in day.hours" :class="{'btn': true, 'btn-success': time.free, 'btn-primary': !time.free, 'btn-block': true, 'active': (newTraBookingDialog.type == 'Normal' && time.hour == newTraBookingDialog.currentTime)}" :disabled="!time.free">
														@{{ moment(time.hour).format('HH:mm') }}
													</button>
													<hr>
													
													<small class="text-center">HORAS EXTRAS</small>
													<div style="margin-bottom: 10px;">
														<button @click="bookingTraSelectExtraTime(day.date)" :class="{'btn': true, 'btn-warning': true, 'btn-block': true, 'active': (newTraBookingDialog.extraDate == day.date && newTraBookingDialog.type == 'Extra')}">
															<i class="fa fa-plus-circle"></i>
														</button>
														<input :id="'extraHour_' + moment(day.date).format('YYYYMMDD')" v-show="newTraBookingDialog.type == 'Extra' && newTraBookingDialog.extraDate == day.date" type="text" v-model="newTraBookingDialog.extraHour" placeholder="HH:MM" class="form-control timeInput text-center" maxlength="5">
													</div>
													
													<button v-for="extra in day.extra" class="btn btn-info btn-block" disabled>
														@{{ moment(extra).format('HH:mm') }}
													</button>
												</td>
											</tr>
										</tbody>
									</table>

									<div v-show="newTraBookingDialog.loading" style="padding-top: 120px;" class="text-center">
										<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
									</div>

									<div v-show="!newTraBookingDialog.currentBranch || !newTraBookingDialog.currentDoctor || !newTraBookingDialog.date" style="padding: 80px 0;" class="text-center">
										<i class="fa fa-info-circle" style="font-size: 120px; color: #ddd;"></i><br>
										<p class="lead">
											<i class="fa fa-long-arrow-left"></i> Seleccione los filtros<br> para encontrar horas disponibles.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div v-if="newTraBookingDialog.mode == 'hour'">
							<div class="col-sm-3">
								<div class="control-group">
									<label class="control-label">Sucursal</label>
									<select class="form-control input-lg" v-model="newTraBookingDialog.currentBranch" @change="bookingTraSelectBranch()" :disabled="branches.length == 0">
										<option :value="null" v-if="branches.length > 0">-seleccione-</option>
										<option :value="null" v-if="branches.length == 0">cargando...</option>
										<option v-for="branch in branches" :value="branch">@{{ branch.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Fecha</label>
									<div class="datepicker" id="bookingTraCalendar2"></div>
								</div>
							</div>
							<div class="col-sm-9" style="max-height: 400px; overflow: scroll;">
								<table v-show="!newTraBookingDialog.loading" class="table table-bordered">
									<thead>
										<tr>
											<th width="100px">Hora</th>
											<th>Doctor</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="time in freehours">
											<td width="100px">
												<button @click="bookingTraSelectTime(time.hour, time.doctor)" :class="{'btn': true, 'btn-success': true, 'btn-block': true, 'active': (newTraBookingDialog.type == 'Normal' && time.hour == newTraBookingDialog.currentTime)}">
													<i class="fa fa-clock-o"></i> @{{ moment(time.hour).format('HH:mm') }}
												</button>
											</td>
											<td><i class="fa fa-user-md"></i> @{{ time.doctor.name }}</td>
										</tr>
									</tbody>
								</table>

								<div v-show="newTraBookingDialog.loading" style="padding-top: 120px;" class="text-center">
									<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
								</div>

								<div v-show="!newTraBookingDialog.currentBranch || !newTraBookingDialog.date" style="padding: 80px 0;" class="text-center">
									<i class="fa fa-info-circle" style="font-size: 120px; color: #ddd;"></i><br>
									<p class="lead">
										<i class="fa fa-long-arrow-left"></i> Seleccione los filtros<br> para encontrar horas disponibles.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Step 3 -->
				<div v-show="newTraBookingDialog.step == 3">
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">
									Rut Paciente:
								</label>
								<div class="input-group">
									<input type="text" maxlength="10" placeholder="ej. 16023817-8" class="form-control" v-model="newTraBookingDialog.patient.rut" @keyup.enter="bookingTraFindPatient()" :disabled="newTraBookingDialog.patient.id || newTraBookingDialog.patient.rut == 'PENDIENTE'">
									<span class="input-group-btn">
								        <button v-show="newTraBookingDialog.patient.id == null && newTraBookingDialog.patient.rut != 'PENDIENTE'" :disabled="newTraBookingDialog.patient.rut == '' || newTraBookingDialog.patient.rut == 'PENDIENTE' || newTraBookingDialog.patient.status == 'Buscando'" @click="bookingTraFindPatient()" class="btn btn-default" type="button">
								        	<i :class="{fa: true, 'fa-search': (newTraBookingDialog.patient.status != 'Buscando'), 'fa-refresh fa-spin fa-fw': (newTraBookingDialog.patient.status == 'Buscando')}"></i>
								        </button>
								        <button v-show="newTraBookingDialog.patient.id == null && newTraBookingDialog.patient.rut != 'PENDIENTE' && newTraBookingDialog.patient.status != 'Buscando'" :disabled="newTraBookingDialog.patient.id" @click="bookingTraSetNewPatient(true)" class="btn btn-default" type="button">
								        	<i class="fa fa-asterisk"></i>
								        </button> 
								        <button v-show="newTraBookingDialog.patient.id !== null || newTraBookingDialog.patient.rut == 'PENDIENTE'" @click="bookingTraResetPatient(true)" class="btn btn-default" type="button">
								        	<i class="fa fa-times"></i>
								        </button>
								    </span>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Nombres:</label>
								<input type="text" class="form-control" v-model="newTraBookingDialog.patient.firstname"  :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Apellidos:</label>
								<input type="text" class="form-control" v-model="newTraBookingDialog.patient.lastname"  :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Fecha de Nacimiento:</label>
								<input type="text" class="form-control" id="bookingTraBirthdate" placeholder="dd/mm/aaaa" v-model="newTraBookingDialog.patient.birthdate" :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Teléfono:</label>
								<input type="text" class="form-control" v-model="newTraBookingDialog.patient.phone"  :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Email:</label>
								<input type="text" class="form-control" v-model="newTraBookingDialog.patient.email"  :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Dirección:</label>
								<input type="text" class="form-control" v-model="newTraBookingDialog.patient.address"  :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Previsión:</label>
								<select class="form-control" v-model="newTraBookingDialog.healthcompany_id" :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
									<option :value="null">-seleccione-</option>
									<option v-for="company in healthcompanies" :value="company.id">@{{ company.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Tipo de Atención:</label>
								<select class="form-control" v-model="newTraBookingDialog.attentiontype_id" :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
									<option :value="null">-seleccione-</option>
									<option v-for="atype in attentiontypes" :value="atype.id">@{{ atype.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Observaciones:</label>
								<input type="text" class="form-control" v-model="newTraBookingDialog.observations"  :disabled="newTraBookingDialog.patient.status == 'Pendiente' || newTraBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
				</div>

				<!-- Step 4 -->
				<div v-show="newTraBookingDialog.step == 4">
					<div class="text-center" v-show="newTraBookingDialog.booking">
						<span style="font-size: 120px; color: green;"><i class="fa fa-check-circle"></i></span>

						<h1>Hora Traumatológica Reservada</h1>
						
						<p class="lead">
							La hora fue agendada correctamente, el detalle es el siguiente:
						</p>

						<div class="row" v-if="newTraBookingDialog.booking">
							<div class="col-sm-6 col-sm-offset-3 text-left">
								<dl class="dl-horizontal">
									<dt style="width: 50%; margin-right: 10px;">Código de Reserva</dt><dd><strong>@{{ newTraBookingDialog.booking.code.toUpperCase() }}</strong></dd>
									<dt style="width: 50%; margin-right: 10px;">Sucursal</dt><dd>@{{ newTraBookingDialog.booking.branch.name }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Doctor</dt><dd>@{{ newTraBookingDialog.booking.doctor.name }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Fecha</dt><dd>@{{ moment(newTraBookingDialog.booking.bookingdate, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY") }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Hora</dt><dd>@{{ moment(newTraBookingDialog.booking.bookingdate, "YYYY-MM-DD HH:mm:ss").format("HH:mm") }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Tipo de Atención</dt><dd>@{{ newTraBookingDialog.booking.attentiontype.name }}</dd>
								</dl>
							</div>
						</div>
					</div>

					<div v-show="!newTraBookingDialog.booking" style="padding: 80px 0;" class="text-center">
						<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
					</div>
				</div>
			</div> 
			<div class="modal-footer">
				<div class="pull-left">
					<button v-show="newTraBookingDialog.step < 4" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button v-show="newTraBookingDialog.step < 4" :disabled="(newTraBookingDialog.step == 1)" @click.prevent="bookingTraGoBack()" type="button" class="btn btn-default">Volver</button>
				</div>

				<button v-show="newTraBookingDialog.step == 2" @click="bookingTraGoNextStep()" :disabled="!bookingTraCheckBookingDateStep" type="button" class="btn btn-primary">
					Continuar
				</button>

				<button v-show="newTraBookingDialog.step == 3" @click="bookingTraRegister()" :disabled="!bookingTraCheckDateForRegister" type="button" class="btn btn-primary">
					Confirmar
				</button>

				<button v-show="newTraBookingDialog.step == 4" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>