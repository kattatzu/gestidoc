<div class="modal fade" tabindex="-1" role="dialog" id="openBoxDialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Aperturar Caja</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						{{ Form::label('branch_id', 'Sucursal:', ['class' => 'control-label']) }}
						<select class="form-control" name="branch_id" id="branch_id" v-model="currentBranch" @change="loadBoxes()" :disabled="openBoxDialog.branch_id != null && boxes.length == 0">
							<option :value="null">-seleccione-</option>
							<option v-for="branch in branches" :value="branch">@{{ branch.name }}</option>
						</select>
					</div>
					<div class="col-sm-6">
						{{ Form::label('box_id', 'Caja:', ['class' => 'control-label']) }}
						<select class="form-control" name="box_id" id="box_id" v-model="openBoxDialog.paymentbox_id" :disabled="openBoxDialog.branch_id != null && boxes.length == 0">
							<option :value="null" v-if="boxes.length > 0">-seleccione-</option>
							<option :value="null" v-if="openBoxDialog.branch_id != null && boxes.length == 0">cargando cajas...</option>
							<option v-for="box in boxes" :value="box.id">@{{ box.name }}</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						{{ Form::label('password', 'Clave Segura:', ['class' => 'control-label']) }}
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							{{ Form::password('password', ['class' => 'form-control', 'v-model' => 'openBoxDialog.password', 'placeholder' => 'Ingrese su clave segura para confirmar']) }}
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
				<button @click.prevent="confirmOpenBox()" type="button" class="btn btn-primary" :disabled="openBoxDialog.paymentbox_id === null || openBoxDialog.password === null || openBoxDialog.password == ''">Confirmar</button>
			</div>
		</div>
	</div>
</div>