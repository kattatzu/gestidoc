<div v-if="newRadBookingDialog" class="modal fade" tabindex="-1" role="dialog" id="newRadiologyBookingDialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					Nueva Reserva de Hora Radiología 
					[
						Sucursal <b>@{{ (newRadBookingDialog.currentBranch ? newRadBookingDialog.currentBranch.name : '') }}</b> -
						<b>@{{ (newRadBookingDialog.currentDoctor ? newRadBookingDialog.currentDoctor.name : '') }}</b> -
						Fecha/Hora: <b>@{{ bookingRadDate ? bookingRadDate.format("ddd DD - HH:mm") : '' }}</b>
					]
				</h4>
			</div>
			<div class="modal-body">
				<transition name="fade">
					<div v-show="alert.message != ''" :class="alert.alertClass">
						<i :class="alert.icon"></i> @{{ alert.message }}
					</div>
				</transition>

				<!-- Step 1 -->
				<div v-show="newRadBookingDialog.step == 1">
					<div class="row" style="margin: 30px 0;">
						<div class="col-sm-3 col-sm-offset-2">
							<button @click.prevent="bookingRadSelectMode('doctor')" type="button" class="btn btn-primary btn-lg btn-block">
								<br>
								<i class="fa fa-user-md"></i> Hora por Doctor
								<br>&nbsp;
							</button>
						</div>
						<div class="col-sm-2 text-center">
							<br><br>
							ú
						</div>
						<div class="col-sm-3">
							<button @click.prevent="bookingRadSelectMode('hour')" type="button" class="btn btn-primary btn-lg btn-block">
								<br>
								<i class="fa fa-clock-o"></i> Hora más cercana
								<br>&nbsp;
							</button>
						</div>
					</div>
				</div>

				<!-- Step 2 -->
				<div v-show="newRadBookingDialog.step == 2">
					<div class="row">
						<div v-if="newRadBookingDialog.mode == 'doctor'">
							<div class="col-sm-3">
								<div class="control-group">
									<label class="control-label">Sucursal</label>
									<p>@{{ newRadBookingDialog.currentBranch ? newRadBookingDialog.currentBranch.name : '' }}</p>
								</div>
								<div class="control-group">
									<label class="control-label">Doctor</label>
									<p>@{{ newRadBookingDialog.currentDoctor ? newRadBookingDialog.currentDoctor.name : '' }}</p>
								</div>
								<div class="control-group">
									<label class="control-label">Fecha</label>
									<div class="datepicker" id="bookingRadCalendar"></div>
								</div>
							</div>
							<div class="col-sm-9" style="max-height: 400px; overflow: scroll;">
								<div class="table-responsive">
									<table v-show="!newRadBookingDialog.loading" class="table table-bordered">
										<thead>
											<tr>
												<th v-for="day in freehours" class="text-center" style="width: 16.5%;">
													@{{ moment(day.date).format("ddd DD") }}
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td v-for="day in freehours" style="vertical-align: top; width: 16.5%;">
													<button @click="bookingRadSelectTime(time.hour)" v-for="time in day.hours" :class="{'btn': true, 'btn-success': time.free, 'btn-primary': !time.free, 'btn-block': true, 'active': (newRadBookingDialog.type == 'Normal' && time.hour == newRadBookingDialog.currentTime)}" :disabled="!time.free">
														@{{ moment(time.hour).format('HH:mm') }}
													</button>
													<hr>
													
													<small class="text-center">HORAS EXTRAS</small>
													<div style="margin-bottom: 10px;">
														<button @click="bookingRadSelectExtraTime(day.date)" :class="{'btn': true, 'btn-warning': true, 'btn-block': true, 'active': (newRadBookingDialog.extraDate == day.date && newRadBookingDialog.type == 'Extra')}">
															<i class="fa fa-plus-circle"></i>
														</button>
														<input :id="'extraHour_' + moment(day.date).format('YYYYMMDD')" v-show="newRadBookingDialog.type == 'Extra' && newRadBookingDialog.extraDate == day.date" type="text" v-model="newRadBookingDialog.extraHour" placeholder="HH:MM" class="form-control timeInput text-center" maxlength="5">
													</div>
													
													<button v-for="extra in day.extra" class="btn btn-info btn-block" disabled>
														@{{ moment(extra).format('HH:mm') }}
													</button>
												</td>
											</tr>
										</tbody>
									</table>

									<div v-show="newRadBookingDialog.loading" style="padding-top: 120px;" class="text-center">
										<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
									</div>

									<div v-show="!newRadBookingDialog.currentBranch || !newRadBookingDialog.currentDoctor || !newRadBookingDialog.date" style="padding: 80px 0;" class="text-center">
										<i class="fa fa-info-circle" style="font-size: 120px; color: #ddd;"></i><br>
										<p class="lead">
											<i class="fa fa-long-arrow-left"></i> Seleccione los filtros<br> para encontrar horas disponibles.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div v-if="newRadBookingDialog.mode == 'hour'">
							<div class="col-sm-3">
								<div class="control-group">
									<label class="control-label">Sucursal</label>
									<select class="form-control input-lg" v-model="newRadBookingDialog.currentBranch" @change="bookingRadSelectBranch()" :disabled="branches.length == 0">
										<option :value="null" v-if="branches.length > 0">-seleccione-</option>
										<option :value="null" v-if="branches.length == 0">cargando...</option>
										<option v-for="branch in branches" :value="branch">@{{ branch.name }}</option>
									</select>
								</div>
								<div class="control-group">
									<label class="control-label">Fecha</label>
									<div class="datepicker" id="bookingRadCalendar2"></div>
								</div>
							</div>
							<div class="col-sm-9" style="max-height: 400px; overflow: scroll;">
								<table v-show="!newRadBookingDialog.loading" class="table table-bordered">
									<thead>
										<tr>
											<th width="100px">Hora</th>
											<th>Doctor</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="time in freehours">
											<td width="100px">
												<button @click="bookingRadSelectTime(time.hour, time.doctor)" :class="{'btn': true, 'btn-success': true, 'btn-block': true, 'active': (newRadBookingDialog.type == 'Normal' && time.hour == newRadBookingDialog.currentTime)}">
													<i class="fa fa-clock-o"></i> @{{ moment(time.hour).format('HH:mm') }}
												</button>
											</td>
											<td><i class="fa fa-user-md"></i> @{{ time.doctor.name }}</td>
										</tr>
									</tbody>
								</table>

								<div v-show="newRadBookingDialog.loading" style="padding-top: 120px;" class="text-center">
									<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
								</div>

								<div v-show="!newRadBookingDialog.currentBranch || !newRadBookingDialog.date" style="padding: 80px 0;" class="text-center">
									<i class="fa fa-info-circle" style="font-size: 120px; color: #ddd;"></i><br>
									<p class="lead">
										<i class="fa fa-long-arrow-left"></i> Seleccione los filtros<br> para encontrar horas disponibles.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Step 3 -->
				<div v-show="newRadBookingDialog.step == 3">
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">
									Rut Paciente:
								</label>
								<div class="input-group">
									<input type="text" maxlength="10" placeholder="ej. 16023817-8" class="form-control" v-model="newRadBookingDialog.patient.rut" @keyup.enter="bookingRadFindPatient()" :disabled="newRadBookingDialog.patient.id || newRadBookingDialog.patient.rut == 'PENDIENTE'">
									<span class="input-group-btn">
								        <button v-show="newRadBookingDialog.patient.id == null && newRadBookingDialog.patient.rut != 'PENDIENTE'" :disabled="newRadBookingDialog.patient.rut == '' || newRadBookingDialog.patient.rut == 'PENDIENTE' || newRadBookingDialog.patient.status == 'Buscando'" @click="bookingRadFindPatient()" class="btn btn-default" type="button">
								        	<i :class="{fa: true, 'fa-search': (newRadBookingDialog.patient.status != 'Buscando'), 'fa-refresh fa-spin fa-fw': (newRadBookingDialog.patient.status == 'Buscando')}"></i>
								        </button>
								        <button v-show="newRadBookingDialog.patient.id == null && newRadBookingDialog.patient.rut != 'PENDIENTE' && newRadBookingDialog.patient.status != 'Buscando'" :disabled="newRadBookingDialog.patient.id" @click="bookingRadSetNewPatient(true)" class="btn btn-default" type="button">
								        	<i class="fa fa-asterisk"></i>
								        </button> 
								        <button v-show="newRadBookingDialog.patient.id !== null || newRadBookingDialog.patient.rut == 'PENDIENTE'" @click="bookingRadResetPatient(true)" class="btn btn-default" type="button">
								        	<i class="fa fa-times"></i>
								        </button>
								    </span>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Nombres:</label>
								<input type="text" class="form-control" v-model="newRadBookingDialog.patient.firstname"  :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Apellidos:</label>
								<input type="text" class="form-control" v-model="newRadBookingDialog.patient.lastname"  :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Fecha de Nacimiento:</label>
								<input type="text" class="form-control" id="bookingRadBirthdate" placeholder="dd/mm/aaaa" v-model="newRadBookingDialog.patient.birthdate" :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Teléfono:</label>
								<input type="text" class="form-control" v-model="newRadBookingDialog.patient.phone"  :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Email:</label>
								<input type="text" class="form-control" v-model="newRadBookingDialog.patient.email"  :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Dirección:</label>
								<input type="text" class="form-control" v-model="newRadBookingDialog.patient.address"  :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Previsión:</label>
								<select class="form-control" v-model="newRadBookingDialog.healthcompany_id" :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
									<option :value="null">-seleccione-</option>
									<option v-for="company in healthcompanies" :value="company.id">@{{ company.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group">
								<label class="control-label">Tipo de Atención:</label>
								<select class="form-control" v-model="newRadBookingDialog.attentiontype_id" :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
									<option :value="null">-seleccione-</option>
									<option v-for="atype in attentiontypes" :value="atype.id">@{{ atype.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Observaciones:</label>
								<input type="text" class="form-control" v-model="newRadBookingDialog.observations"  :disabled="newRadBookingDialog.patient.status == 'Pendiente' || newRadBookingDialog.patient.status == 'Buscando'">
							</div>
						</div>
					</div>
				</div>

				<!-- Step 4 -->
				<div v-show="newRadBookingDialog.step == 4">
					<div class="text-center" v-show="newRadBookingDialog.booking">
						<span style="font-size: 120px; color: green;"><i class="fa fa-check-circle"></i></span>

						<h1>Hora Traumatológica Reservada</h1>
						
						<p class="lead">
							La hora fue agendada correctamente, el detalle es el siguiente:
						</p>

						<div class="row" v-if="newRadBookingDialog.booking">
							<div class="col-sm-6 col-sm-offset-3 text-left">
								<dl class="dl-horizontal">
									<dt style="width: 50%; margin-right: 10px;">Código de Reserva</dt><dd><strong>@{{ newRadBookingDialog.booking.code.toUpperCase() }}</strong></dd>
									<dt style="width: 50%; margin-right: 10px;">Sucursal</dt><dd>@{{ newRadBookingDialog.booking.branch.name }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Doctor</dt><dd>@{{ newRadBookingDialog.booking.doctor.name }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Fecha</dt><dd>@{{ moment(newRadBookingDialog.booking.bookingdate, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY") }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Hora</dt><dd>@{{ moment(newRadBookingDialog.booking.bookingdate, "YYYY-MM-DD HH:mm:ss").format("HH:mm") }}</dd>
									<dt style="width: 50%; margin-right: 10px;">Tipo de Atención</dt><dd>@{{ newRadBookingDialog.booking.attentiontype.name }}</dd>
								</dl>
							</div>
						</div>
					</div>

					<div v-show="!newRadBookingDialog.booking" style="padding: 80px 0;" class="text-center">
						<i class="fa fa-refresh fa-spin fa-fw" style="font-size: 120px; color: #ddd;"></i>
					</div>
				</div>
			</div> 
			<div class="modal-footer">
				<div class="pull-left">
					<button v-show="newRadBookingDialog.step < 4" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button v-show="newRadBookingDialog.step < 4" :disabled="(newRadBookingDialog.step == 1)" @click.prevent="bookingRadGoBack()" type="button" class="btn btn-default">Volver</button>
				</div>

				<button v-show="newRadBookingDialog.step == 2" @click="bookingRadGoNextStep()" :disabled="!bookingRadCheckBookingDateStep" type="button" class="btn btn-primary">
					Continuar
				</button>

				<button v-show="newRadBookingDialog.step == 3" @click="bookingRadRegister()" :disabled="!bookingRadCheckDateForRegister" type="button" class="btn btn-primary">
					Confirmar
				</button>

				<button v-show="newRadBookingDialog.step == 4" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>