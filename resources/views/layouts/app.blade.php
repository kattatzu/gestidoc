<!DOCTYPE html>
<html lang="es">
<head>
    {{ Html::meta('charset', 'utf-8') }}
    {{ Html::meta('X-UA-Compatible', 'IE=edge') }}
    {{ Html::meta('viewport', 'width=device-width, initial-scale=1') }}
    {{ Html::meta('csrf-token', csrf_token()) }}

    <title>{{ config('app.name') }}</title>

    {{ Html::style('css/app.css') }}
    @stack('styles')

    <script>
        window.Laravel = {
            user: {!! Auth::guest() ? 'null' : json_encode(Auth::user()->publicProfile()) !!},
            csrfToken: '{{ csrf_token() }}'
        };
    </script>

    <!--[if lt IE 9]>
        {{ Html::script('js/html5shiv.js') }}
        {{ Html::script('js/respond.min.js') }}
    <![endif]-->
</head>
<body id="@stack('body-id')" class="theme-red fixed-header fixed-leftmenu">
    <div id="app">

        @yield('content')
    
    </div>

    {{ Html::script('js/app.js') }}
    {{ Html::script('js/all.js') }}
    @stack('scripts')
</body>
</html>