<!DOCTYPE html>
<html lang="es">
<head>
    {{ Html::meta('charset', 'utf-8') }}
    {{ Html::meta('X-UA-Compatible', 'IE=edge') }}
    {{ Html::meta('viewport', 'width=device-width, initial-scale=1') }}
    {{ Html::meta('csrf-token', csrf_token()) }}

    <title>{{ config('app.name') }}</title>

    {{ Html::style('css/app.css') }}
    @stack('styles')
</head>
<body style="background: none;">
    <div id="app">

        @yield('content')
    
    </div>
</body>
</html>