<header style="page-break-before: always;">
	<div class="row">
		<div class="col-xs-12 text-center">
			{{ Html::image('img/logo-print.png', '', ['class' => 'logox']) }}
		</div>
		<div class="col-xs-12 text-center" style="margin-top: 30px;">
			<h1 class="title">Ficha de Atención Médica Nº {{ $record->id }}</h1>
		</div>
	</div>
</header>
<section>
	<h2 class="subtitle">Datos del Paciente</h2>
	<div class="row">
		<div class="col-xs-2 dvLabel">Rut:</div>
		<div class="col-xs-4">{{ $record->patient->rut }}</div>
		<div class="col-xs-2 dvLabel">Nombre:</div>
		<div class="col-xs-4">{{ $record->patient->fullname }}</div>
	</div>
	<div class="row">
		<div class="col-xs-2 dvLabel">F. Nacimiento:</div>
		<div class="col-xs-4">{{ $record->patient->birthdate->format('d/m/Y') }}</div>
		<div class="col-xs-2 dvLabel">Edad:</div>
		<div class="col-xs-4">{{ $record->patient->age }} años</div>
	</div>
	<div class="row">
		<div class="col-xs-2 dvLabel">Email:</div>
		<div class="col-xs-4">{{ $record->patient->email }}</div>
		<div class="col-xs-2 dvLabel">Teléfono:</div>
		<div class="col-xs-4">{{ $record->patient->phone }}</div>
	</div>
</section>
<section>
	<h2 class="subtitle">Datos de la Atención</h2>
	<div class="row">
		<div class="col-xs-2 dvLabel">F. Atención:</div>
		<div class="col-xs-4">{{ $record->attentiondate->format('d/m/Y H:i') }}</div>
		<div class="col-xs-2 dvLabel">Sucursal:</div>
		<div class="col-xs-4">{{ $record->branch->name }}</div>
	</div>
	<div class="row">
		<div class="col-xs-2 dvLabel">Doctor:</div>
		<div class="col-xs-4">{{ ucwords(strtolower($record->doctor->doctor_fullname)) }}</div>
		<div class="col-xs-2 dvLabel">Especialidad:</div>
		<div class="col-xs-4">{{ ucwords(strtolower($record->doctor->doctor_speciality)) }}</div>
	</div>
</section>
<section>
	<h2 class="subtitle">Anamnesis</h2>
	<p class="contents">{!! nl2br($record->anamnesis) !!}</p>
</section>
<section>
	<h2 class="subtitle">Examenes Físicos</h2>
	<p class="contents">{!! nl2br($record->physical_exam) !!}</p>
</section>
<section>
	<h2 class="subtitle">Diagnóstico</h2>
	<p class="contents">{!! nl2br($record->diagnosis) !!}</p>
</section>
<section>
	<h2 class="subtitle">Historial Familiar</h2>
	<p class="contents">{!! nl2br($record->patient->family_history) !!}</p>
</section>
