@extends('layouts.print')
@section('content')

<div class="orderLayout">
	<header>
		<div class="row">
			<div class="col-xs-6">
				{{ Html::image('img/logo-print.png', '', ['class' => 'logo']) }}
			</div>
			<div class="col-xs-6 text-center doctorInfo">
				<strong>{{ $doctorInfo['doctor_fullname'] }}</strong><br>
                {{ $doctorInfo['doctor_speciality'] }}<br>
                RUT.: {{ $doctorInfo['doctor_rut'] }}<br>
                R.C.M.: {{ $doctorInfo['doctor_rcm'] }}
			</div>
		</div>
	</header>

	<p><strong>RP.</strong></p>
	<br>
	<p class="orderDetails">

		{!! nl2br($orderDetails) !!}

	</p>
	<p class="signature">
		____________________________________<br>
		{{ $doctorInfo['doctor_fullname'] }}<br>
		{{ $doctorInfo['doctor_rut'] }}<br>
		{{ date('d/m/Y') }}
	</p>
</div>


<style>
	.buttons{
		text-align: center;
		background: #ddd;
		padding: 5px;
	}
	.logo{
		max-width: 100%;
	}
	.signature{
		text-align: center;
		padding-top: 70px;
		position: relative;
		bottom: -300px;
		width: 100%;
	}
	.orderLayout{
		width: 15cm;
		height: 21cm;
		border: solid 1px gray;
		padding: 30px 20px;
		font-size: 12px;
	}
	.orderLayout header{
		height: 3cm;
	}
	.doctorInfo{
		text-transform: uppercase; 
		font-family: monospace;
		font-size: 13px;
	}
	.orderDetails{
		text-transform: uppercase; 
		font-family: monospace;
		font-size: 12px;
		padding-left: 10px;
	}
</style>
<script>
	print();
</script>
@stop