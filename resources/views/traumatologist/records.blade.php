@extends('traumatologist.layout')
@section('s-content')

<div class="main-box">
	<header class="main-box-header clearfix">
		<a href="{{ action('Traumatologist\HomeController@index') }}" class="btn btn-default btn-xs">
			<i class="fa fa-chevron-circle-left"></i> Volver
		</a>
		<div class="pull-right">

		</div>
	</header>
</div>

<div class="main-box">
	<header class="main-box-header clearfix">
		<h2>
			Buscar Fichas Médicas
		</h2>
	</header>
	<div class="main-box-body clearfix">
		{{ Form::open(['method' => 'GET', 'class' => 'form-inline']) }}

		<div class="form-group">
			{{ Form::label('date', 'Fecha de Atención:') }}
			{{ Form::date('date', $selectedDate, ['class' => 'form-control']) }}
		</div>
		<div class="form-group">
			{{ Form::label('patient_rut', 'Rut Paciente:') }}
			{{ Form::text('patient_rut', $selectedPatientRut, ['class' => 'form-control', 'placeholder' => 'ej. 16023817-8']) }}
		</div>
		<div class="form-group">
			{{ Form::label('patient_name', 'Nombre Paciente:') }}
			{{ Form::text('patient_name', $selectedPatientName, ['class' => 'form-control', 'placeholder' => 'ej. Jorge Rojas']) }}
		</div>
		<div class="form-group">
			{{ Form::label('record', 'Nº de Ficha:') }}
			{{ Form::text('record', $selectedRecord, ['class' => 'form-control', 'placeholder' => 'ej. 290']) }}
		</div>

		<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>

		{{ Form::close() }}
	</div>
</div>

@if($records->total() > 0)
<div class="main-box">
	<header class="main-box-header clearfix">
		<h2 class="pull-left">
			Fichas Médicas
			<span class="badge">{{ $records->total() }}</span>
		</h2>

		<div class="text-right">
			<button type="button" id="btnMassivePrint" class="btn btn-default"><i class="fa fa-print"></i> Imprimir</button>
		</div>
	</header>
	<div class="main-box-body clearfix">
		{{ Form::open(['id' => 'frmMassivePrint', 'target' => '_blank', 'action' => 'Traumatologist\RecordController@printMassive']) }}
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="text-center">
						<button class="btn btn-default btn-xs" id="btnCheckAll">
							<i class="fa fa-check-square"></i>
						</button>
					</th>
					<th width="120px" class="text-center">Nº de Ficha</th>
					<th width="150px" class="text-center">Fecha Atención</th>
					<th>Paciente</th>
					<th width="120px" class="text-center">Rut</th>
					<th>Sucursal</th>
					<th>Doctor</th>
					<th width="60px">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				@foreach($records as $record)
				<tr>
					<td class="text-center">
						<label>
							{{ Form::checkbox('print_ids[]', $record->id, false, ['class' => 'print_ids']) }}
						</label>
					</td>
					<td class="text-center">{{ $record->id }}</td>
					<td class="text-center">{{ $record->attentiondate->format('d/m/Y H:i') }}</td>
					<td>{{ $record->patient->fullname }}</td>
					<td class="text-center">{{ $record->patient->rut }}</td>
					<td>{{ $record->branch->name }}</td>
					<td>{{ $record->doctor->name }}</td>
					<td class="text-center">
						<a href="{{ action('Traumatologist\RecordController@edit', $record->id) }}" class="btn btn-default btn-xs">
                            <i class="fa fa-file-text-o"></i>
                        </a>
					</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td colspan="8" class="text-right">
						{{ $records->appends(request()->except('page'))->links() }}
					</td>
				</tr>
			</tfoot>
		</table>
		{{ Form::close() }}
	</div>
</div>
@else

@endif
@endsection
@push('scripts')
<script>
$(function(){
	$("#btnCheckAll").click(function(e){
		e.preventDefault();
		if($(".print_ids:checked").length == 0){
			$(".print_ids").prop('checked', true);
		}else{
			$(".print_ids").prop('checked', false);
		}
	});
	$("#btnMassivePrint").click(function(e){
		e.preventDefault();
		if($(".print_ids:checked").length == 0){
			alert("Primero debe selecionar una o mas fichas a imprimir");
			return;
		}else{
			$("#frmMassivePrint").submit();
		}
	});
});
</script>
@endpush
