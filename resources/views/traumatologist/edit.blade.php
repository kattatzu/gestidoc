@extends('traumatologist.layout')
@section('s-content')

@if($allowEditing)
{{ Form::open(['action' => ['Traumatologist\RecordController@update', $record->id], 'method' => 'PUT']) }}
@endif

<h1>Ficha Médica Nº {{ $record->id }}</h1>

<div class="main-box">
	<header class="main-box-header clearfix">
		<a href="{{ action('Traumatologist\HomeController@index') }}" class="btn btn-default btn-xs">
			<i class="fa fa-chevron-circle-left"></i> Volver
		</a>
		<a href="{{ action('Traumatologist\RecordController@index', ['patient_rut' => $record->patient->rut]) }}" class="btn btn-default">
			<i class="fa fa-history"></i> Historial Médico del Paciente
		</a>
		<a href="{{ action('Traumatologist\RecordController@print', $record->id) }}" target="_blank" class="btn btn-default">
			<i class="fa fa-print"></i> Imprimir
		</a>
		<div class="pull-right">
			@if($allowEditing)
			<button type="button" @click="openMedicalOrderDialog()" class="btn btn-default"><i class="fa fa-plus-circle"></i> Nueva orden médica</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Guardar cambios</button>
			@endif
		</div>
	</header>
</div>

@if($isOwner and !$allowEditing)
<div class="alert alert-warning">
	<i class="fa fa-archive"></i> Esta ficha ha sido archivada ya que han pasado más de 48 horas desde
	la atención médica.
</div>
@endif

<div class="main-box">
	<header class="main-box-header clearfix">
		<h2>Datos del Paciente</h2>
	</header>
	<div class="main-box-body clearfix">
		<div class="row">
			<div class="col-xs-2">Rut:</div>
			<div class="col-xs-4">{{ $record->patient->rut }}</div>
			<div class="col-xs-2">Nombre:</div>
			<div class="col-xs-4">{{ $record->patient->fullname }}</div>
		</div>
		<div class="row">
			<div class="col-xs-2">F. Nacimiento:</div>
			<div class="col-xs-4">{{ $record->patient->birthdate->format('d/m/Y') }}</div>
			<div class="col-xs-2">Edad:</div>
			<div class="col-xs-4">{{ $record->patient->age }} años</div>
		</div>
		<div class="row">
			<div class="col-xs-2">Email:</div>
			<div class="col-xs-4">{{ $record->patient->email }}</div>
			<div class="col-xs-2">Teléfono:</div>
			<div class="col-xs-4">{{ $record->patient->phone }}</div>
		</div>
	</div>
</div>
<div class="main-box">
	<header class="main-box-header clearfix">
		<h2>Datos de la Atención</h2>
	</header>
	<div class="main-box-body clearfix">
		<div class="row">
			<div class="col-xs-2 dvLabel">F. Atención:</div>
			<div class="col-xs-4">{{ $record->attentiondate->format('d/m/Y H:i') }}</div>
			<div class="col-xs-2 dvLabel">Sucursal:</div>
			<div class="col-xs-4">{{ $record->branch->name }}</div>
		</div>
		<div class="row">
			<div class="col-xs-2 dvLabel">Doctor:</div>
			<div class="col-xs-4">{{ ucwords(strtolower($record->doctor->doctor_fullname)) }}</div>
			<div class="col-xs-2 dvLabel">Especialidad:</div>
			<div class="col-xs-4">{{ ucwords(strtolower($record->doctor->doctor_speciality)) }}</div>
		</div>
	</div>
</div>

<div class="main-box">
	<header class="main-box-header clearfix">
		<h2>Ficha Médica</h2>
	</header>
	<div class="main-box-body clearfix">
		<div class="row">
			<div class="col-sm-6">
				{{ Form::label('anamnesis', 'Anamnesis', ['class'=> 'control-label']) }}
				@if($allowEditing)
				{{ Form::textarea('anamnesis', $record->anamnesis, ['class' => 'form-control']) }}
				@else
				<p style="padding: 5px; min-height: 200px; background: #f7f7f7;">
					{!! nl2br($record->anamnesis) !!}
				</p>
				@endif
			</div>
			<div class="col-sm-6">
				{{ Form::label('physical_exam', 'Examenes Físicos', ['class'=> 'control-label']) }}
				@if($allowEditing)
				{{ Form::textarea('physical_exam', $record->physical_exam, ['class' => 'form-control', 'id' => 'physical_exam']) }}
				@else
				<p style="padding: 5px; min-height: 200px; background: #f7f7f7;">
					{!! nl2br($record->physical_exam) !!}
				</p>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				{{ Form::label('diagnosis', 'Diagnóstico', ['class'=> 'control-label']) }}
				@if($allowEditing)
				{{ Form::textarea('diagnosis', $record->diagnosis, ['class' => 'form-control']) }}
				@else
				<p style="padding: 5px; min-height: 200px; background: #f7f7f7;">
					{!! nl2br($record->diagnosis) !!}
				</p>
				@endif
			</div>
			<div class="col-sm-6">
				{{ Form::label('family_history', 'Historial Familiar', ['class'=> 'control-label']) }}
				@if($allowEditing)
				{{ Form::textarea('family_history', $record->patient->family_history, ['class' => 'form-control']) }}
				@else
				<p style="padding: 5px; min-height: 200px; background: #f7f7f7;">
					{!! nl2br($record->family_history) !!}
				</p>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="main-box">
	<header class="main-box-header clearfix">
		<h2>Documentos Adjuntos</h2>
	</header>
	<div class="main-box-body clearfix">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>Documento</th>
					<th class="text-center">Tamaño</th>
					<th class="text-center">Fecha de Carga</th>
					<th class="text-center">Ver/Descargar</th>
				</tr>
			</thead>
			<tbody>
				@foreach($record->files as $file)
				<tr>
					<td>	
						<a href="{{ route('files.show', [$file->id, $file->filename]) }}" target="_blank">
							<i class="fa fa-file-o"></i>
							{{ $file->realname }}
						</a>
					</td>
					<td class="text-center">{{ number_format($file->size / 1024, 2) }} Mb</td>
					<td class="text-center">{{ $file->created_at->format('d/m/Y H:i') }}</td>
					<td class="text-center">
						<a href="{{ route('files.show', [$file->id, $file->filename]) }}" target="_blank">
							<i class="fa fa-download"></i> Descargar
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@if($allowEditing)
{{ Form::close() }}
@endif

@if($allowEditing)
<medicalorder v-if="showMedicalOrderDialog"></medicalorder>
@endif

@endsection
@push('scripts')
	<script>
		window.currentDoctor = {
			fullname: '{{ strtoupper($currentDoctor->doctor_fullname) }}',
			speciality: '{{ strtoupper($currentDoctor->doctor_speciality) }}',
			rut: '{{ $currentDoctor->doctor_rut }}',
			rcm: '{{ $currentDoctor->doctor_icm }}'
		};
		window.currentRecord = {
			id: {{ $record->id }}
		};
	</script>
@endpush