@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="{{ action('Admin\UserController@index') }}"><i class="fa fa-users"></i> Usuarios</a></li>
      <li class="active">Editar Usuario "{{ $user->name }}"</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        {{ Form::open(['action' => ['Admin\UserController@update', $user->id], 'method' => 'PUT']) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                Editar Usuario "{{ $user->name }}"
            </div>
            <div class="panel-body">
                @include('admin.users._form')
            </div>
            <div class="panel-footer text-right">
                <a href="{{ action('Admin\UserController@index') }}" class="btn btn-default pull-left">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar cambios</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
