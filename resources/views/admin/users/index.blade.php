@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Usuarios</li>
    </ol>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="panel-title">Usuarios</h3>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ action('Admin\UserController@create') }}" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Registrar</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Nombre</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Perfil</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr {!! !$user->active ? 'class="danger"' : '' !!}>
                    <td class="text-center">{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->profile }}</td>
                    <td class="text-center">{{ $user->active ? 'Activo' : 'Inactivo' }}</td>
                    <td class="text-center">
                        <a href="{{ action('Admin\UserController@edit', [$user->id]) }}"><i class="fa fa-edit"></i> Editar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <td class="text-right" colspan="7">
                    {!! $users->links() !!}
                </td>
            </tfoot>
        </table>
    </div>
</div>
@endsection
