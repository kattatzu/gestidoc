<div class="control-group">
    {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
    {{ Form::text('name', $user->name, ['class' => 'form-control', 'required']) }}
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('username', 'Nombre de Usuario', ['class' => 'control-label']) }}
            {{ Form::text('username', $user->username, ['class' => 'form-control', 'required']) }}
        </div>
        </div>
            <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('email', 'E-mail', ['class' => 'control-label']) }}
            {{ Form::email('email', $user->email, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('profile', 'Perfil', ['class' => 'control-label']) }}
            {{ Form::select('profile', $profiles, $user->profile, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('active', 'Estado', ['class' => 'control-label']) }}
            {{ Form::select('active', $status, $user->active, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('password', ($user->exists ? 'Nueva Contraseña' : 'Contraseña'), ['class' => 'control-label']) }}
            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => ($user->exists ? 'Dejar vacia si no desea cambiarla' : ''), 'required' => ($user->exists ? null : true)]) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('password_confirmation', ($user->exists ? 'Confirmar nueva Contraseña' : 'Confirmar Contraseña'), ['class' => 'control-label']) }}
            {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => ($user->exists ? 'Dejar vacia si no desea cambiarla' : ''), 'required' => ($user->exists ? null : true)]) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('secure_password', ($user->exists ? 'Nueva Clave de Seguridad' : 'Clave de Seguridad'), ['class' => 'control-label']) }}
            {{ Form::password('secure_password', ['class' => 'form-control', 'placeholder' => ($user->exists ? 'Dejar vacia si no desea cambiarla' : ''), 'required' => ($user->exists ? null : true)]) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('secure_password_confirmation', ($user->exists ? 'Confirmar nueva Clave de Seguridad' : 'Confirmar Clave de Seguridad'), ['class' => 'control-label']) }}
            {{ Form::password('secure_password_confirmation', ['class' => 'form-control', 'placeholder' => ($user->exists ? 'Dejar vacia si no desea cambiarla' : ''), 'required' => ($user->exists ? null : true)]) }}
        </div>
    </div>
</div>

<h3>Solo Traumatólogos</h3>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('doctor_fullname', 'Nombre para Orden Médica', ['class' => 'control-label']) }}
            {{ Form::text('doctor_fullname', $user->doctor_fullname, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('doctor_speciality', 'Especialidad para Orden Médica', ['class' => 'control-label']) }}
            {{ Form::text('doctor_speciality', $user->doctor_speciality, ['class' => 'form-control']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('doctor_rut', 'Rut para Orden Médica', ['class' => 'control-label']) }}
            {{ Form::text('doctor_rut', $user->doctor_rut, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('doctor_icm', 'I.C.M. para Orden Médica', ['class' => 'control-label']) }}
            {{ Form::text('doctor_icm', $user->doctor_icm, ['class' => 'form-control']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('doctor_age_begin', 'Edad Mínima de Pacientes', ['class' => 'control-label']) }}
            {{ Form::number('doctor_age_begin', $user->doctor_age_begin, ['class' => 'form-control', 'min' => 0, 'max' => 99]) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="control-group">
            {{ Form::label('doctor_age_end', 'Edad Máxima de Pacientes', ['class' => 'control-label']) }}
            {{ Form::number('doctor_age_end', $user->doctor_age_end, ['class' => 'form-control', 'min' => 0, 'max' => 999]) }}
        </div>
    </div>
</div>
