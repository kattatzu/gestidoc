@extends('layouts.app')
@section('content')
<div id="theme-wrapper">
    <header class="navbar" id="header-navbar">
        <div class="container">
            <a href="{{ url('/') }}" id="logo" class="navbar-brand">
                {{ Html::image('img/logo.png', config('app.name'), ['class' => 'normal-logo logo-white']) }}
                {{ Html::image('img/logo-black.png', config('app.name'), ['class' => 'normal-logo logo-black']) }}
                {{ Html::image('img/logo-small.png', config('app.name'), ['class' => 'small-logo hidden-xs hidden-sm hidden']) }}
            </a>
            <div class="clearfix">
                <div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
                    <ul class="nav navbar-nav pull-left">
                        <li class="topTools">
                            <a href="http://www.schot.cl" target="_blank">
                                {!! Html::image('img/tools_schot.jpg') !!}
                            </a>
                        </li>
                        <li class="topTools">
                            <a href="http://cl.prvademecum.com/index.php" target="_blank">
                                {!! Html::image('img/tools_vade.jpg') !!}
                            </a>
                        </li>
                        <li class="topTools">
                            <a href="http://www.fonasa.cl" target="_blank">
                                {!! Html::image('img/tools_fonasa.jpg') !!}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="nav-no-collapse pull-right" id="header-nav">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown profile-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs">{{ app('request')->user()->name }}</span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i>Salir del Sistema</a></li>
                            </ul>
                        </li>
                        <li class="hidden-xxs">
                            <a href="{{ url('logout') }}" class="btn">
                                <i class="fa fa-power-off"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div id="page-wrapper" class="container">
        <div id="content-wrapper">
            @if(Session::has('message'))
                <div class="alert alert-{{ Session::get('type') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    @if(Session::get('type', 'info') == 'danger')<i class="fa fa-times-circle"></i>@endif
                    @if(Session::get('type', 'info') == 'warning')<i class="fa fa-warning"></i>@endif
                    @if(Session::get('type', 'info') == 'info')<i class="fa fa-info-circle"></i>@endif
                    @if(Session::get('type', 'info') == 'success')<i class="fa fa-check-circle"></i>@endif
                    {{ Session::get('message') }}
                </div>
            @endif

            @yield('s-content')
        </div>
    </div>
</div>

@stop
@push('scripts')

@endpush
