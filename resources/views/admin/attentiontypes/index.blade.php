@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Tipos de Atención</li>
    </ol>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="panel-title">Tipos de Atención</h3>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ action('Admin\AttentionTypeController@create') }}" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Registrar</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Tipo de Atención</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($atypes as $atype)
                <tr>
                    <td class="text-center">{{ $atype->id }}</td>
                    <td>{{ $atype->name }}</td>
                    <td class="text-center">
                        <a href="{{ action('Admin\AttentionTypeController@edit', [$atype->id]) }}"><i class="fa fa-edit"></i> Editar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <td class="text-right" colspan="3">
                    {!! $atypes->links() !!}
                </td>
            </tfoot>
        </table>
    </div>
</div>
@endsection
