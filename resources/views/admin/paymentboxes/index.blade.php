@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Cajas</li>
    </ol>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="panel-title">Cajas</h3>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ action('Admin\PaymentBoxController@create') }}" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Registrar</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Sucursal</th>
                    <th>Caja</th>
                    <th class="text-center">Código</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pboxes as $pbox)
                <tr>
                    <td class="text-center">{{ $pbox->id }}</td>
                    <td>{{ $pbox->branch->name }}</td>
                    <td>{{ $pbox->name }}</td>
                    <td class="text-center">{{ $pbox->code }}</td>
                    <td class="text-center">
                        <a href="{{ action('Admin\PaymentBoxController@edit', [$pbox->id]) }}"><i class="fa fa-edit"></i> Editar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <td class="text-right" colspan="5">
                    {!! $pboxes->links() !!}
                </td>
            </tfoot>
        </table>
    </div>
</div>
@endsection
