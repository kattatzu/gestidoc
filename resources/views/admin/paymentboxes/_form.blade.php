<div class="control-group">
    {{ Form::label('branch_id', 'Sucursal', ['class' => 'control-label']) }}
    {{ Form::select('branch_id', $branches, $pbox->branch_id, ['class' => 'form-control', 'required']) }}
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="control-group">
            {{ Form::label('code', 'Código', ['class' => 'control-label']) }}
            {{ Form::text('code', $pbox->code, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
    <div class="col-sm-9">
        <div class="control-group">
            {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
            {{ Form::text('name', $pbox->name, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>
