<div class="row" style="margin-bottom: 30px;">
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\UserController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-users fa-3x"></i><br>
            Usuarios
        </a>
    </div>
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\BranchController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-building fa-3x"></i><br>
            Sucursales
        </a>
    </div>
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\HourlyController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-clock-o fa-3x"></i><br>
            Horarios
        </a>
    </div>
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\AttentionTypeController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-server fa-3x"></i><br>
            Tipos de Atención
        </a>
    </div>
</div>
<div class="row" style="margin-bottom: 30px;">
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\PaymentBoxController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-money fa-3x"></i><br>
            Cajas
        </a>
    </div>
    <!--<div class="col-sm-3 text-center">
        <a href="" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-building fa-3x"></i><br>
            Clínicas
        </a>
    </div>-->
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\CommercialCompanyController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-credit-card fa-3x"></i><br>
            C. Comerciales
        </a>
    </div>
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\HealthCompanyController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-building fa-3x"></i><br>
            Previsiones
        </a>
    </div>
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\InsuranceCompanyController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-building fa-3x"></i><br>
            Cias. Seguros
        </a>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\ProductController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-shopping-cart fa-3x"></i><br>
            Productos/Serv.
        </a>
    </div>
    <div class="col-sm-3 text-center">
        <a href="{{ action('Admin\PatientController@index') }}" class="btn btn-default btn-lg btn-block" style="padding: 20px;">
            <i class="fa fa-users fa-3x"></i><br>
            Pacientes
        </a>
    </div>
</div>
