@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="{{ action('Admin\CommercialCompanyController@index') }}"><i class="fa fa-building"></i> Companías Comerciales</a></li>
      <li class="active">Registrar Nueva Compañia Comercial</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        {{ Form::open(['action' => ['Admin\CommercialCompanyController@store'], 'method' => 'POST']) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                Registrar Nueva Compañia Comercial
            </div>
            <div class="panel-body">
                @include('admin.commercialcompanies._form')
            </div>
            <div class="panel-footer text-right">
                <a href="{{ action('Admin\CommercialCompanyController@index') }}" class="btn btn-default pull-left">Cancelar</a>
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
