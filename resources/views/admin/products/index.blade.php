@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Productos/Servicios</li>
    </ol>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="panel-title">Productos/Servicios</h3>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ action('Admin\ProductController@create') }}" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Registrar</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center">Código</th>
                    <th>Familia</th>
                    <th>Producto/Serv.</th>
                    <th>Categoría</th>
                    <th>Sub Categoría</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td class="text-center">{{ $product->code }}</td>
                    <td>{{ $product->type }}</td>
                    <td>{{ ($product->type == 'Consulta' ? 'Consulta ' . $product->doctor->name : $product->name) }}</td>
                    <td>{{ $product->category }}</td>
                    <td>{{ $product->sub_category }}</td>
                    <td class="text-center">
                        <a href="{{ action('Admin\ProductController@edit', [$product->id]) }}"><i class="fa fa-edit"></i> Editar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <td class="text-right" colspan="6">
                    {!! $products->links() !!}
                </td>
            </tfoot>
        </table>
    </div>
</div>
@endsection
