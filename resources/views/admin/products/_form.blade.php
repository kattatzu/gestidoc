<div class="control-group">
    {{ Form::label('type', 'Familia', ['class' => 'control-label']) }}
    {{ Form::select('type', $types, $product->type, ['class' => 'form-control', 'required']) }}
</div>
<div class="control-group">
    {{ Form::label('category', 'Categoría (opcional)', ['class' => 'control-label']) }}
    {{ Form::text('category', $product->category, ['class' => 'form-control']) }}
</div>
<div class="control-group">
    {{ Form::label('sub_category', 'Sub Categoría (opcional)', ['class' => 'control-label']) }}
    {{ Form::text('sub_category', $product->sub_category, ['class' => 'form-control']) }}
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="control-group">
            {{ Form::label('code', 'Código', ['class' => 'control-label']) }}
            {{ Form::text('code', $product->code, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
    <div class="col-sm-9">
        <div class="control-group">
            {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
            {{ Form::text('name', $product->name, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>
<div class="control-group">
    {{ Form::label('doctor_id', 'Doctor (solo si es Consulta)', ['class' => 'control-label']) }}
    {{ Form::select('doctor_id', $doctors, $product->doctor_id, ['class' => 'form-control']) }}
</div>
