<div class="row">
    <div class="col-sm-3">
        <div class="control-group">
            {{ Form::label('code', 'Código', ['class' => 'control-label']) }}
            {{ Form::text('code', $icompany->code, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
    <div class="col-sm-9">
        <div class="control-group">
            {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
            {{ Form::text('name', $icompany->name, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>
