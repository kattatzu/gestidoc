@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Pacientes</li>
    </ol>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="panel-title">Pacientes</h3>
            </div>
            <div class="col-sm-6 text-right">
                <a href="{{ action('Admin\PatientController@create') }}" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Registrar</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th class="text-center">Rut</th>
                    <th class="text-center">Teléfono</th>
                    <th>Email</th>
                    <th class="text-center">Edad</th>
                    <th class="text-center">Editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($patients as $patient)
                <tr>
                    <td>{{ $patient->fullname_inverse }}</td>
                    <td class="text-center">{{ $patient->rut }}</td>
                    <td class="text-center">{{ $patient->phone }}</td>
                    <td>{{ $patient->email }}</td>
                    <td class="text-center">{{ $patient->age }} años</td>
                    <td class="text-center">
                        <a href="{{ action('Admin\PatientController@edit', [$patient->id]) }}"><i class="fa fa-edit"></i> Editar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <td class="text-right" colspan="6">
                    {!! $patients->links() !!}
                </td>
            </tfoot>
        </table>
    </div>
</div>
@endsection
