@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="{{ action('Admin\PatientController@index') }}"><i class="fa fa-users"></i> Pacientes</a></li>
      <li class="active">Editar Paciente "{{ $patient->fullname }}"</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        {{ Form::open(['action' => ['Admin\PatientController@update', $patient->id], 'method' => 'PUT']) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                Editar Paciente "{{ $patient->fullname }}"
            </div>
            <div class="panel-body">
                @include('admin.patients._form')
            </div>
            <div class="panel-footer text-right">
                <a href="{{ action('Admin\PatientController@index') }}" class="btn btn-default pull-left">Cancelar</a>
                <a href="{{ action('Admin\PatientController@index') }}" class="btn btn-default pull-left" id="btnDelete"><i class="fa fa-trash"></i></a>
                <button type="submit" class="btn btn-primary">Guardar cambios</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

{{ Form::open(['action' => ['Admin\PatientController@destroy', $patient->id], 'method' => 'DELETE', 'id' => 'frmDelete']) }}
{{ Form::close() }}
@endsection
@push('scripts')
<script>
    $(function(){
        $("#btnDelete").click(function(e){
            e.preventDefault();
            if(confirm('¿Está seguro de eliminar este registro?')){
                $("#frmDelete").submit();
            }
        });
    });
</script>
@endpush
