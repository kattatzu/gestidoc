<div class="row">
    <div class="col-sm-4">
        <div class="control-group">
            {{ Form::label('rut', 'Rut', ['class' => 'control-label']) }}
            {{ Form::text('rut', $patient->rut, ['class' => 'form-control', 'required', 'placeholder' => 'ej. 11111111-1']) }}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="control-group">
            {{ Form::label('name', 'Nombre', ['class' => 'control-label']) }}
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('firstname', $patient->firstname, ['class' => 'form-control', 'required', 'placeholder' => 'ej. Jorge Andrés']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('lastname', $patient->lastname, ['class' => 'form-control', 'required', 'placeholder' => 'ej. Soto Perez']) }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="control-group">
            {{ Form::label('phone', 'Teléfono', ['class' => 'control-label']) }}
            {{ Form::text('phone', $patient->phone, ['class' => 'form-control', 'required', 'placeholder' => 'ej. (+569) 9876 3783']) }}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="control-group">
            {{ Form::label('email', 'E-mail', ['class' => 'control-label']) }}
            {{ Form::email('email', $patient->email, ['class' => 'form-control', 'placeholder' => 'ej. usuario@gmail.com']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="control-group">
            {{ Form::label('birthdate', 'F. Nacimiento', ['class' => 'control-label']) }}
            {{ Form::text('birthdate', ($patient->exists ? $patient->birthdate->format('d/m/Y') : null), ['class' => 'form-control', 'required', 'placeholder' => 'ej. 24/03/1985', 'maxlength' => 10]) }}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="control-group">
            {{ Form::label('address', 'Dirección', ['class' => 'control-label']) }}
            {{ Form::text('address', $patient->address, ['class' => 'form-control', 'placeholder' => 'ej. San Antonio 123, Valparaíso']) }}
        </div>
    </div>
</div>
<div class="control-group">
    {{ Form::label('healthcompany_id', 'Previsión', ['class' => 'control-label']) }}
    {{ Form::select('healthcompany_id', $healthcompanies, $patient->healthcompany_id, ['class' => 'form-control', 'required']) }}
</div>
