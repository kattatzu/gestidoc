@extends('admin.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Horarios</li>
    </ol>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Horarios</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="well">
                    {{ Form::open(['method' => 'GET']) }}
                    <div class="control-group">
                        <label class="control-label">Doctor</label>
                        {{ Form::select('doctor_id', $doctors, request()->input('doctor_id'), ['class' => 'form-control', 'required']) }}
                    </div>
                    <div class="control-group">
                        <label class="control-label">Sucursal</label>
                        {{ Form::select('branch_id', $branches, request()->input('branch_id'), ['class' => 'form-control', 'required']) }}
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Consultar</button>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-sm-9">
                {{ Form::open(['action' => 'Admin\HourlyController@store', 'method' => 'POST']) }}
                {{ Form::hidden('doctor_id', request()->input('doctor_id')) }}
                {{ Form::hidden('branch_id', request()->input('branch_id')) }}
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="150px">&nbsp;</th>
                            <th class="text-center">Lunes</th>
                            <th class="text-center">Martes</th>
                            <th class="text-center">Miercoles</th>
                            <th class="text-center">Jueves</th>
                            <th class="text-center">Viernes</th>
                            <th class="text-center">Sábado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>T. Atención <small>(min.)</small></th>
                            <td>{{ Form::select('lun_slots', $slots, $hourly['1']['slot'], ['class' => 'form-control text-center', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::select('mar_slots', $slots, $hourly['2']['slot'], ['class' => 'form-control text-center', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::select('mie_slots', $slots, $hourly['3']['slot'], ['class' => 'form-control text-center', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::select('jue_slots', $slots, $hourly['4']['slot'], ['class' => 'form-control text-center', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::select('vie_slots', $slots, $hourly['5']['slot'], ['class' => 'form-control text-center', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::select('sab_slots', $slots, $hourly['6']['slot'], ['class' => 'form-control text-center', 'disabled' => $disabled]) }}</td>
                        </tr>
                        <tr>
                            <th>Inicio Turno</th>
                            <td>{{ Form::text('lun_begin', $hourly['1']['begin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mar_begin', $hourly['2']['begin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mie_begin', $hourly['3']['begin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('jue_begin', $hourly['4']['begin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('vie_begin', $hourly['5']['begin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('sab_begin', $hourly['6']['begin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                        </tr>
                        <tr>
                            <th>Inicio Break</th>
                            <td>{{ Form::text('lun_bbegin', $hourly['1']['bbegin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mar_bbegin', $hourly['2']['bbegin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mie_bbegin', $hourly['3']['bbegin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('jue_bbegin', $hourly['4']['bbegin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('vie_bbegin', $hourly['5']['bbegin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('sab_bbegin', $hourly['6']['bbegin'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                        </tr>
                        <tr>
                            <th>Término Break</th>
                            <td>{{ Form::text('lun_bend', $hourly['1']['bend'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mar_bend', $hourly['2']['bend'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mie_bend', $hourly['3']['bend'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('jue_bend', $hourly['4']['bend'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('vie_bend', $hourly['5']['bend'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('sab_bend', $hourly['6']['bend'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                        </tr>
                        <tr>
                            <th>Término Turno</th>
                            <td>{{ Form::text('lun_end', $hourly['1']['end'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mar_end', $hourly['2']['end'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('mie_end', $hourly['3']['end'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('jue_end', $hourly['4']['end'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('vie_end', $hourly['5']['end'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                            <td>{{ Form::text('sab_end', $hourly['6']['end'], ['class' => 'form-control text-center', 'placeholder' => 'HH:MM', 'disabled' => $disabled]) }}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="text-right" colspan="7">
                                <button type="submit" class="btn btn-primary" {{ $disabled ? 'disabled' : '' }}>Guardar cambios</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
