@extends('administrative.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Detalle de Ventas</li>
    </ol>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Detalle de Ventas</h3>
    </div>
    <div class="panel-body">
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="control-group">
                    {{ Form::label('branch_id', 'Sucursal', ['class' => 'control-label']) }}
                    {{ Form::select('branch_id', $branches, $branch_id, ['class' => 'form-control', 'required']) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    {{ Form::label('datebegin', 'Fecha de Inicio', ['class' => 'control-label']) }}
                    {{ Form::text('datebegin', $datebegin, ['class' => 'form-control datepicker', 'required']) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    {{ Form::label('dateend', 'Fecha de Término', ['class' => 'control-label']) }}
                    {{ Form::text('dateend', $dateend, ['class' => 'form-control datepicker', 'required']) }}
                </div>
            </div>
            <div class="col-sm-4">
                <br>
                <div class="btn-group" role="group" aria-label="...">
                    <button type="submit" name="type" value="query" class="btn btn-primary">
                        <i class="fa fa-search"></i> Consultar
                    </button>
                    <button type="submit" name="type" value="export" class="btn btn-default">
                        <i class="fa fa-file-excel-o"></i> Exportar
                    </button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="2">Vendedor</th>
                <th colspan="2" class="text-center">Boleta</th>
                <th colspan="3" class="text-center">Previsión</th>
                <th colspan="2" class="text-center">Excedentes</th>
                <th colspan="3" class="text-center">Compañia de Seguros</th>
                <th class="text-center" rowspan="2">Total</th>
            </tr>
            <tr>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
                <th>Previsión</th>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
                <th>Compañia</th>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
            </tr>
        </thead>
        <tbody>
            @foreach($paymentsProducts as $paymentsProduct)
            <tr>
                <td>{{ $paymentsProduct->seller->name }}</td>
                <td class="text-center">{{ $paymentsProduct->copago_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->copago_amount, 0, ',', '.') }}</td>
                <td>{{ ($paymentsProduct->healthcompany) ? $paymentsProduct->healthcompany->name : '' }}</td>
                <td class="text-center">{{ $paymentsProduct->health_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->health_amount, 0, ',', '.') }}</td>
                <td class="text-center">{{ $paymentsProduct->excessive_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->excessive_amount, 0, ',', '.') }}</td>
                <td>{{ ($paymentsProduct->insurancecompany) ? $paymentsProduct->insurancecompany->name : '' }}</td>
                <td class="text-center">{{ $paymentsProduct->insurance_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->insurance_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->total, 0, ',', '.') }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('copago_amount'), 0, ',', '.') }}</td>
                <td colspan="2">&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('health_amount'), 0, ',', '.') }}</td>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('excessive_amount'), 0, ',', '.') }}</td>
                <td colspan="2">&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('insurance_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('total'), 0, ',', '.') }}</td>
            </tr>
        </tfoot>
    </table>
</div>
@endsection
@push('scripts')
<script>
    $(function(){
        $(".datepicker").datepicker({
            autoclose: true,
            endDate: '0d',
            language: 'es',
            format: 'dd/mm/yyyy'
        });
    });
</script>
@endpush
