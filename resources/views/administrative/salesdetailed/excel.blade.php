<!DOCTYPE html>
<html lang="es">
<head>
    {{ Html::meta('charset', 'utf-8') }}
</head>
<body>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="2">Sucursal</th>
                <th rowspan="2">Fecha de Venta</th>
                <th rowspan="2">Vendedor</th>
                <th colspan="2" class="text-center">Boleta</th>
                <th colspan="3" class="text-center">Previsión</th>
                <th colspan="2" class="text-center">Excedentes</th>
                <th colspan="3" class="text-center">Compañia de Seguros</th>
                <th class="text-center" rowspan="2">Total</th>
            </tr>
            <tr>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
                <th>Previsión</th>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
                <th>Compañia</th>
                <th class="text-center">Nº Docto.</th>
                <th class="text-right">Monto</th>
            </tr>
        </thead>
        <tbody>
            @foreach($paymentsProducts as $paymentsProduct)
            <tr>
                <td>{{ $paymentsProduct->branch->name }}</td>
                <td class="text-center">{{ $paymentsProduct->paymentdate->format('d/m/Y') }}</td>
                <td>{{ $paymentsProduct->seller->name }}</td>
                <td class="text-center">{{ $paymentsProduct->copago_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->copago_amount, 0, ',', '.') }}</td>
                <td>{{ ($paymentsProduct->healthcompany) ? $paymentsProduct->healthcompany->name : '' }}</td>
                <td class="text-center">{{ $paymentsProduct->health_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->health_amount, 0, ',', '.') }}</td>
                <td class="text-center">{{ $paymentsProduct->excessive_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->excessive_amount, 0, ',', '.') }}</td>
                <td>{{ ($paymentsProduct->insurancecompany) ? $paymentsProduct->insurancecompany->name : '' }}</td>
                <td class="text-center">{{ $paymentsProduct->insurance_document }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->insurance_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($paymentsProduct->total, 0, ',', '.') }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('copago_amount'), 0, ',', '.') }}</td>
                <td colspan="2">&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('health_amount'), 0, ',', '.') }}</td>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('excessive_amount'), 0, ',', '.') }}</td>
                <td colspan="2">&nbsp;</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('insurance_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($paymentsProducts->sum('total'), 0, ',', '.') }}</td>
            </tr>
        </tfoot>
    </table>
</body>
</html>
