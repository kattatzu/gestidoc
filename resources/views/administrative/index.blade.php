@extends('administrative.layout')
@section('s-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenid@ {{ auth()->user()->name }}!</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom: 30px;">
                        <div class="col-sm-3 text-center">
                            <a href="{{ action('Administrative\SalesResumeController@index') }}" class="btn btn-info btn-lg btn-block" style="padding: 20px;">
                                <i class="fa fa-table fa-3x"></i><br>
                                Resumen<br>de Ventas
                            </a>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a href="{{ action('Administrative\SalesDetailedController@index') }}" class="btn btn-info btn-lg btn-block" style="padding: 20px;">
                                <i class="fa fa-table fa-3x"></i><br>
                                Detalle<br>de Ventas
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('admin.menu')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
