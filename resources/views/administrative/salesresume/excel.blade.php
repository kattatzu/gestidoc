<!DOCTYPE html>
<html lang="es">
<head>
    {{ Html::meta('charset', 'utf-8') }}
</head>
<body>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="2">Vendedor</th>
                <th rowspan="2" class="text-right">Subtotal</th>
                <th colspan="2" class="text-center">Descuento</th>
                <th rowspan="2" class="text-right">Total</th>
                <th colspan="2" class="text-center">Previsión</th>
                <th rowspan="2" class="text-right">Cía. Seguros</th>
                <th rowspan="2" class="text-right">Excedentes</th>
                <th rowspan="2" class="text-right">Total Copago</th>
                <th rowspan="2" class="text-right">Débito</th>
                <th rowspan="2" class="text-right">Crédito</th>
                <th colspan="2" class="text-center">T. Comercial</th>
                <th rowspan="2" class="text-right">Efectivo</th>
            </tr>
            <tr>
                <th>Motivo</th>
                <th class="text-right">Monto</th>
                <th>Isapre</th>
                <th class="text-right">Monto</th>
                <th>Tarjeta</th>
                <th class="text-right">Monto</th>
            </tr>
        </thead>
        <tbody>
            @foreach($payments as $payment)
            <tr>
                <td>{{ $payment->seller->name }}</td>
                <td class="text-right">${{ number_format($payment->subtotal, 0, ',', '.') }}</td>
                <td>{{ $payment->discount_reason }}</td>
                <td class="text-right">${{ number_format($payment->discount_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->total, 0, ',', '.') }}</td>
                <td>{{ $payment->healthcompany ? $payment->healthcompany->name : '' }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('health_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('insurance_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('excessive_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('copago_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->debit_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->credit_amount, 0, ',', '.') }}</td>
                <td>{{ $payment->commercialcompany ? $payment->commercialcompany->name : '' }}</td>
                <td class="text-right">${{ number_format($payment->commercial_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->cash_amount, 0, ',', '.') }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($payments->sum('subtotal'), 0, ',', '.') }}</td>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($payments->sum('discount_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('total'), 0, ',', '.') }}</td>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($healthTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($insuranceTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($excessiveTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($copagoTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('debit_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('credit_amount'), 0, ',', '.') }}</td>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($payments->sum('commercial_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('cash_amount'), 0, ',', '.') }}</td>
            </tr>
        </tfoot>
    </table>
</body>
</html>
