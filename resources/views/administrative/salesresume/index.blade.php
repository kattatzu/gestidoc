@extends('administrative.layout')
@section('s-content')
<div style="margin-bottom: 20px;">
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Resumen de Ventas</li>
    </ol>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Resumen de Ventas</h3>
    </div>
    <div class="panel-body">
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="control-group">
                    {{ Form::label('branch_id', 'Sucursal', ['class' => 'control-label']) }}
                    {{ Form::select('branch_id', $branches, $branch_id, ['class' => 'form-control', 'required']) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    {{ Form::label('datebegin', 'Fecha de Inicio', ['class' => 'control-label']) }}
                    {{ Form::text('datebegin', $datebegin, ['class' => 'form-control datepicker', 'required']) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    {{ Form::label('dateend', 'Fecha de Término', ['class' => 'control-label']) }}
                    {{ Form::text('dateend', $dateend, ['class' => 'form-control datepicker', 'required']) }}
                </div>
            </div>
            <div class="col-sm-4">
                <br>
                <div class="btn-group" role="group" aria-label="...">
                    <button type="submit" name="type" value="query" class="btn btn-primary">
                        <i class="fa fa-search"></i> Consultar
                    </button>
                    <button type="submit" name="type" value="export" class="btn btn-default">
                        <i class="fa fa-file-excel-o"></i> Exportar
                    </button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Vendedor</th>
                <th class="text-center">Subtotal</th>
                <th class="text-center">Descuento</th>
                <th class="text-center">Total</th>
                <th class="text-center">Previsión</th>
                <th class="text-center">Cía. Seguros</th>
                <th class="text-center">Excedentes</th>
                <th class="text-center">Total Copago</th>
                <th class="text-center">Débito</th>
                <th class="text-center">Crédito</th>
                <th class="text-center">T. Comercial</th>
                <th class="text-center">Efectivo</th>
            </tr>
        </thead>
        <tbody>
            @foreach($payments as $payment)
            <tr>
                <td>{{ $payment->seller->name }}</td>
                <td class="text-right">${{ number_format($payment->subtotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->discount_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->total, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('health_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('insurance_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('excessive_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->productsPivot->sum('copago_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->debit_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->credit_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->commercial_amount, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payment->cash_amount, 0, ',', '.') }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td>&nbsp;</td>
                <td class="text-right">${{ number_format($payments->sum('subtotal'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('discount_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('total'), 0, ',', '.') }}</td>

                <td class="text-right">${{ number_format($healthTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($insuranceTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($excessiveTotal, 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($copagoTotal, 0, ',', '.') }}</td>

                <td class="text-right">${{ number_format($payments->sum('debit_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('credit_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('commercial_amount'), 0, ',', '.') }}</td>
                <td class="text-right">${{ number_format($payments->sum('cash_amount'), 0, ',', '.') }}</td>
            </tr>
        </tfoot>
    </table>
</div>
@endsection
@push('scripts')
<script>
    $(function(){
        $(".datepicker").datepicker({
            autoclose: true,
            endDate: '0d',
            language: 'es',
            format: 'dd/mm/yyyy'
        });
    });
</script>
@endpush
