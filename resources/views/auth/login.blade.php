@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div id="login-box">
                <div id="login-box-holder">
                    <div class="row">
                        <div class="col-xs-12">
                            <header id="login-header">
                                <div id="login-logo">
                                    {{ Html::image('img/logo.png', config('app.name')) }}
                                </div>
                            </header>
                            <div id="login-box-inner">
                                {{ Form::open() }}

                                    @if($errors->count() > 0)
                                    <div class="alert alert-warning">
                                        <i class="fa fa-warning"></i> Las credenciales no son válidas!
                                    </div>
                                    @endif

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        {{ Form::text('username', '', ['required', 'class' => 'form-control', 'placeholder' => 'Nombre de Usuario']) }}
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        {{ Form::password('password', ['required', 'class' => 'form-control', 'placeholder' => 'Contraseña']) }}
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-success col-xs-12">Login</button>
                                        </div>
                                    </div>

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('body-id', 'login-page')