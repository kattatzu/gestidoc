@extends('kinesiologist.layout')
@section('s-content')

{{ Form::open(['action' => ['Kinesiologist\RecordController@update', $booking->id], 'method' => 'PUT', 'files' => true]) }}
    <h1>Ficha Kinesiológica Nº {{ $kinedetail->id }}</h1>

    <div class="main-box">
        <header class="main-box-header clearfix">
            <a href="{{ action('Kinesiologist\HomeController@index') }}" class="btn btn-default btn-xs">
                <i class="fa fa-chevron-circle-left"></i> Volver
            </a>
            <a href="{{ action('Kinesiologist\RecordController@print', $booking->id) }}" target="_blank" class="btn btn-default">
                <i class="fa fa-print"></i> Imprimir
            </a>
            <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Guardar cambios</button>
            </div>
        </header>
    </div>

    <div class="main-box">
        <header class="main-box-header clearfix">
            <h2>Ficha del Paciente</h2>
        </header>
        <div class="main-box-body clearfix">
            <div class="row">
                <div class="col-xs-2">Rut:</div>
                <div class="col-xs-4">{{ $booking->patient->rut }}</div>
                <div class="col-xs-2">Nombre:</div>
                <div class="col-xs-4">{{ $booking->patient->fullname }}</div>
            </div>
            <div class="row">
                <div class="col-xs-2">F. Nacimiento:</div>
                <div class="col-xs-4">{{ $booking->patient->birthdate->format('d/m/Y') }}</div>
                <div class="col-xs-2">Edad:</div>
                <div class="col-xs-4">{{ $booking->patient->age }} años</div>
            </div>
            <div class="row">
                <div class="col-xs-2">Email:</div>
                <div class="col-xs-4">{{ $booking->patient->email }}</div>
                <div class="col-xs-2">Teléfono:</div>
                <div class="col-xs-4">{{ $booking->patient->phone }}</div>
            </div>
            <div class="row">
                <div class="col-xs-2">Dirección:</div>
                <div class="col-xs-4">{{ $booking->patient->address }}</div>
                <div class="col-xs-2">Previsión:</div>
                <div class="col-xs-4">{{ ($booking->healthcompany) ? $booking->healthcompany->name : '' }}</div>
            </div>
            <div class="row">
                <div class="col-xs-2">Médico Tratante:</div>
                <div class="col-xs-4">{{ $booking->doctor->name }}</div>
                <div class="col-xs-2">Nº Sesiones:</div>
                <div class="col-xs-4">{{ $kinedetail->sessions }}</div>
            </div>
            <div class="row">
                <div class="col-xs-2">Diagnóstico:</div>
                <div class="col-xs-4">
                    {{ Form::text('diagnosis', $kinedetail->diagnosis, ['class' => 'form-control']) }}
                </div>
                <div class="col-xs-2">Valor Programa:</div>
                <div class="col-xs-4">$ {{ number_format($kinedetail->price, 0, ',', '.') }}</div>
            </div>
            <div class="row">
                <div class="col-xs-2">Fecha Indicación:</div>
                <div class="col-xs-4">
                    {{ Form::date('indication_date', $kinedetail->indication_date, ['class' => 'form-control', 'required']) }}
                </div>
                <div class="col-xs-2">Fecha Tratamiento:</div>
                <div class="col-xs-4">
                    {{ Form::date('treatment_date', $kinedetail->treatment_date, ['class' => 'form-control', 'required']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="main-box">
        <header class="main-box-header clearfix">
            <h2>Sesiones</h2>
        </header>
        <div class="main-box-body clearfix">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Nº</th>
                        <th class="text-center">Fecha</th>
                        <th>Prestaciones</th>
                        <th class="text-center">Realizado</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1;?>
                    @foreach($kinedetail->bookings()->orderBy('bookingdate')->get() as $aBooking)
                    <tr @if($aBooking->kine_success == 'Si') class="success" @endif>
                        <td class="text-center">{{ $i }}</td>
                        <td class="text-center">
                            {{ $aBooking->bookingdate->format('d/m/Y H:i') }}
                        </td>
                        <td>
                            {!! str_replace(",", "<br>", $kinedetail->details) !!}
                        </td>
                        <td class="text-center">
                            {{ Form::select('kine_success['.$aBooking->id.']', ['No' => 'No', 'Si' => 'Si'], $aBooking->kine_success, ['class' => 'form-control']) }}
                        </td>
                    </tr>
                    <?php $i++;?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="main-box">
        <header class="main-box-header clearfix">
            <h2>Observaciones y/o Comentarios</h2>
        </header>
        <div class="main-box-body clearfix">
            {{ Form::textarea('observations', $kinedetail->observations, ['class' => 'form-control', 'rows' => '10']) }}
        </div>
    </div>

    <div class="main-box">
        <header class="main-box-header clearfix">
            <h2>Informes y Documentos Adjuntos</h2>
        </header>
        <div class="main-box-body clearfix">
            {{ Form::file('file') }}
            <hr>

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Documento</th>
                        <th class="text-center">Tamaño</th>
                        <th class="text-center">Fecha de Carga</th>
                        <th class="text-center">Ver/Descargar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($kinedetail->files as $file)
                    <tr>
                        <td>
                            <a href="{{ route('files.show', [$file->id, $file->filename]) }}" target="_blank">
                                <i class="fa fa-file-o"></i>
                                {{ $file->realname }}
                            </a>
                        </td>
                        <td class="text-center">{{ number_format($file->size / 1024, 2) }} Mb</td>
                        <td class="text-center">{{ $file->created_at->format('d/m/Y H:i') }}</td>
                        <td class="text-center">
                            <a href="{{ route('files.show', [$file->id, $file->filename]) }}" target="_blank">
                                <i class="fa fa-download"></i> Descargar
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <hr>
    <div class="text-right">
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Guardar cambios</button>
    </div>
{{ Form::close() }}

@endsection
@push('scripts')
    <script>
        window.currentDoctor = {
            fullname: '{{ strtoupper($currentDoctor->doctor_fullname) }}',
            speciality: '{{ strtoupper($currentDoctor->doctor_speciality) }}',
            rut: '{{ $currentDoctor->doctor_rut }}',
            rcm: '{{ $currentDoctor->doctor_icm }}'
        };
        window.currentkinedetail = {
            id: {{ $kinedetail->id }}
        };
    </script>
@endpush
