<header>
    <div class="row">
        <div class="col-xs-12 text-center">
            {{ Html::image('img/logo-print.png', '', ['class' => 'logox']) }}
        </div>
        <div class="col-xs-12 text-center" style="margin-top: 30px;">
            <h1 class="title">Ficha Kinesiológica Nº {{ $kinedetail->id }}</h1>
        </div>
    </div>
</header>
<section>
    <h2 class="subtitle">Ficha del Paciente</h2>
    <div class="row">
        <div class="col-xs-2 dvLabel">Rut:</div>
        <div class="col-xs-4">{{ $booking->patient->rut }}</div>
        <div class="col-xs-2 dvLabel">Nombre:</div>
        <div class="col-xs-4">{{ $booking->patient->fullname }}</div>
    </div>
    <div class="row">
        <div class="col-xs-2 dvLabel">F. Nacimiento:</div>
        <div class="col-xs-4">{{ $booking->patient->birthdate->format('d/m/Y') }}</div>
        <div class="col-xs-2 dvLabel">Edad:</div>
        <div class="col-xs-4">{{ $booking->patient->age }} años</div>
    </div>
    <div class="row">
        <div class="col-xs-2 dvLabel">Email:</div>
        <div class="col-xs-4">{{ $booking->patient->email }}</div>
        <div class="col-xs-2 dvLabel">Teléfono:</div>
        <div class="col-xs-4">{{ $booking->patient->phone }}</div>
    </div>
    <div class="row">
        <div class="col-xs-2 dvLabel">Dirección:</div>
        <div class="col-xs-4">{{ $booking->patient->address }}</div>
        <div class="col-xs-2 dvLabel">Previsión:</div>
        <div class="col-xs-4">{{ ($booking->healthcompany) ? $booking->healthcompany->name : '' }}</div>
    </div>
    <div class="row">
        <div class="col-xs-2 dvLabel">Médico Tratante:</div>
        <div class="col-xs-4">{{ $booking->doctor->name }}</div>
        <div class="col-xs-2 dvLabel">Nº de Sesiones:</div>
        <div class="col-xs-4">{{ $kinedetail->sessions }}</div>
    </div>
    <div class="row">
        <div class="col-xs-2 dvLabel">Diagnóstico:</div>
        <div class="col-xs-4">{{ $kinedetail->diagnosis }}</div>
        <div class="col-xs-2 dvLabel">Valor Programa:</div>
        <div class="col-xs-4">$ {{ number_format($kinedetail->price, 0, ',', '.') }}</div>
    </div>
    <div class="row">
        <div class="col-xs-2 dvLabel">Fecha Indicación:</div>
        <div class="col-xs-4">{{ ($kinedetail->indication_date) ? $kinedetail->indication_date->format('d/m/Y') : '' }}</div>
        <div class="col-xs-2 dvLabel">Fecha Tratamiento:</div>
        <div class="col-xs-4">{{ ($kinedetail->treatment_date) ? $kinedetail->treatment_date->format('d/m/Y') : '' }}</div>
    </div>
</section>

<section>
    <h2 class="subtitle">Sesiones</h2>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th class="text-center">Nº</th>
                <th class="text-center">Fecha</th>
                <th>Prestaciones</th>
                <th class="text-center">Realizado</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($kinedetail->bookings()->orderBy('bookingdate')->get() as $aBooking)
            <tr>
                <td class="text-center">{{ $i }}</td>
                <td class="text-center">
                    {{ $aBooking->bookingdate->format('d/m/Y H:i') }}
                </td>
                <td>
                    {!! $kinedetail->details !!}
                </td>
                <td class="text-center">
                    {{ $aBooking->kine_success }}
                </td>
            </tr>
            <?php $i++;?>
            @endforeach
        </tbody>
    </table>
</section>

<section>
    <h2 class="subtitle">Observaciones y/o Comentarios</h2>
    <p class="contents">{!! nl2br($kinedetail->observations) !!}</p>
</section>
