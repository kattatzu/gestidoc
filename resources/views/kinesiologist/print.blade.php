@extends('layouts.print')
@section('content')
<div class="container">
    @include('kinesiologist.printcontent')

    <p class="text-center">Fecha de impresión: {{ date('d/m/Y H:i') }}</p>
</div>
<style type="text/css">
    *{
        color: #000 !important;
        font-family: "Lucida Console", "Courier New", monospace;
        font-size: 10px;
        text-transform: uppercase;
    }
    .container{
        margin: auto;
        max-width: 800px;
    }
    .logo{
        display: block;
        margin: 5px;
    }
    .title{
        font-size: 16px;
        font-weight: bold;
        text-transform: uppercase;
    }
    section{
        margin-bottom: 20px;
        padding: 10px;
        border: solid 1px #dee;
    }
    .subtitle{
        font-size: 13px;
        padding: 0;
        margin: 5px 0 10px 0;
        text-transform: uppercase;
        font-weight: bold;
    }
    .dvLabel{
        text-transform: uppercase;
    }
    .contents{
        min-height: 20px;
    }
</style>
<script>
    print();
</script>
@stop
