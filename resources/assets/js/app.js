require('./bootstrap');
require('./bus');
require('./store');
require('./utils');
require('./models/bookingModel');
require('./models/patientModel');
require('./models/recordModel');

Vue.component('alert', require('./components/Alert.vue'));
Vue.component('securepassword', require('./components/SecurePassword.vue'));
Vue.component('secretarydashboard', require('./components/SecretaryDashboard.vue'));

Vue.component('trabookingdialog', require('./components/TraBookingDialog.vue'));
Vue.component('tradiarydialog', require('./components/TraDiaryDialog.vue'));

Vue.component('kinebookingdialog', require('./components/KineBookingDialog.vue'));
Vue.component('kinediarydialog', require('./components/KineDiaryDialog.vue'));

Vue.component('radbookingdialog', require('./components/RadBookingDialog.vue'));
Vue.component('raddiarydialog', require('./components/RadDiaryDialog.vue'));

Vue.component('patientdata', require('./components/PatientData.vue'));
Vue.component('receivepatientdialog', require('./components/ReceivePatientDialog.vue'));
Vue.component('openpaymentboxdialog', require('./components/OpenPaymentBoxDialog.vue'));
Vue.component('recordsaledialog', require('./components/RecordSaleDialog.vue'));
Vue.component('paymentview', require('./components/PaymentView.vue'));
Vue.component('paymentproductsview', require('./components/PaymentProductsView.vue'));
Vue.component('pricelist', require('./components/PriceList.vue'));
Vue.component('medicalorder', require('./components/MedicalOrder.vue'));

Vue.component('tradashboard', require('./components/TraDashboard.vue'));
Vue.component('trarecorddialog', require('./components/TraRecordDialog.vue'));
Vue.component('kinedashboard', require('./components/KineDashboard.vue'));

let app = new Vue({
    el: '#app',
    data: {
    	shared: store.data,
        utils: utils,

        showMedicalOrderDialog: false
    },
    created(){
        if(Laravel.user){
    	   store.loadData();
        }
    },
    methods: {

        openMedicalOrderDialog(){
            this.showMedicalOrderDialog = true;
        }

    }
});
