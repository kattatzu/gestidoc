window.store = {
	debug: true,
	data: {
		branches: [],
		doctors: [],
		healthcompanies: [],
		attentiontypes: [],
		insurancecompanies: [],
		comercialcompanies: [],
        products: [],
        currentUser: null,
	},
	/**
	 * [log Muestra un mensaje por consola]
	 * @param  string message Mensaje a mostrar
	 * @return void
	 */
	log(message) {
		this.debug && console.log(message);
	},
	/**
	 * [loadData Carga los datos desde la Api]
	 * @return void
	 */
	loadData(){
		let that = this;
		that.log("loadData()");

        that.loadCurrentUserProfile();
		that.loadBranches();
		that.loadDoctors();
		that.loadHealthCompanies();
		that.loadInsuranceCompanies();
		that.loadComercialCompanies();
		that.loadAttentionTypes();
        that.loadProducts();
	},


    loadCurrentUserProfile(){
        let that = this;
        that.log("loadCurrentUserProfile()");

        Vue.http.get('/api/session')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.currentUser = rs.data;

                    bus.$emit('store-loaded-current-user', rs.data)
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
    },

	/**
	 * [loadBranches Carga las sucursales]
	 * @return void
	 */
	loadBranches(){
		let that = this;
		that.log("loadBranches()");

		Vue.http.get('/api/branches')
			.then((response) => {
				var rs = response.body;

				if(rs.success){
					that.data.branches = rs.data;
				}else{
					alert(rs.message);
				}
			}, (response) => {
				alert("Algo salio mal, por favor intente nuevamente!");
			}
		);
	},
    /**
     * [loadProducts Carga los productos]
     * @return void
     */
    loadProducts(){
        let that = this;
        that.log("loadProducts()");

        Vue.http.get('/api/products')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.products = rs.data;
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
    },
	/**
	 * [loadDoctors Carga los doctores]
	 * @return void
	 */
	loadDoctors(){
		let that = this;
		that.log("loadDoctors()");

        Vue.http.get('/api/doctors')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.doctors = rs.data;
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
	},
	/**
	 * [loadHealthCompanies Carga las previsiones]
	 * @return void
	 */
	loadHealthCompanies(){
        let that = this;
        that.log("loadHealthCompanies()");
        
        Vue.http.get('/api/healthcompanies')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.healthcompanies = rs.data;
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
    },
    /**
	 * [loadInsuranceCompanies Carga las compañias de salud]
	 * @return void
	 */
	loadInsuranceCompanies(){
        let that = this;
        that.log("loadInsuranceCompanies()");
        
        Vue.http.get('/api/insurancecompanies')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.insurancecompanies = rs.data;
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
    },
    /**
	 * [loadInsuranceCompanies Carga las compañias de salud]
	 * @return void
	 */
	loadComercialCompanies(){
        let that = this;
        that.log("loadComercialCompanies()");
        
        Vue.http.get('/api/comercialcompanies')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.comercialcompanies = rs.data;
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
    },
    /**
     * [loadAttentionTypes Carga los tipos de atención]
     * @return void
     */
    loadAttentionTypes(){
        let that = this;
        that.log("loadAttentionTypes()");
        
        Vue.http.get('/api/attentiontypes')
            .then((response) => {
                var rs = response.body;

                if(rs.success){
                    that.data.attentiontypes = rs.data;
                }else{
                    alert(rs.message);
                }
            }, (response) => {
                alert("Algo salio mal, por favor intente nuevamente!");
            }
        );
    }
}