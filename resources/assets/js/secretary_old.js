Vue.component(
    'alert', 
    require('./components/Alert.vue')
);

Vue.component(
    'trabookingdialog', 
    require('./components/TraBookingDialog.vue')
);

const secretary = new Vue({
    el: '#app',
    data: {
    	openBoxDialog: {
    		branch_id: null,
    		paymentbox_id: null,
    		password: null
    	},
    	closeBoxDialog: {
    		password: null
    	},
        newTraBookingDialog: null,
        newKineBookingDialog: null,
        newRadBookingDialog: null,
        receivePatientDialog: {
            search: {
                rut: null,
                name: null,
                bookingdate: null,
                branch_id: null,
                doctor_id: null
            },
            hours: [],
            selectedHour: null
        },
        freehours: [],
    	branches: [],
        currentBranch: null,
    	boxes: [],
        doctors: [],
        allDoctors: [],
        healthcompanies: [],
        attentiontypes: [],
        alert: {
            message: '',
            icon: '',
            alertClass: ''
        },
    },
    created(){
        this.loadAllDoctors();
    	this.loadBranches();
        this.loadHealthCompanies();
        this.loadAttentionTypes();

        $("#gridContents").removeClass("hide");
    },
    methods: {
        // Kinesiología
            bookingKineOpenDialog(){
                this.bookingKineReset();

                setTimeout(function(){
                    $("#newKinesiologyBookingDialog").modal('show');
                }, 500);
            },
            bookingKineReset(){
                this.freehours = [];
                this.newKineBookingDialog = {
                    step: 1,
                    mode: null,
                    profile: 'Kinesiólogo',
                    branch_id: null,
                    doctor_id: null,
                    currentDoctor: null,
                    currentBranch: null,
                    dates: [],
                    patient: {
                        status: 'Pendiente',

                        id: null,
                        rut: '',
                        firstname: null,
                        lastname: null,
                        phone: null,
                        email: null,
                        birthdate: null,
                        address: null,
                    },
                    healthcompany_id: null,
                    attentiontype_id: null,
                    observations: null,
                    loading: false,
                    booking: null,
                    type: 'Normal',
                };
            },
            bookingKineSelectMode(mode){
                let that = this;

                that.newKineBookingDialog.mode = mode;
                that.bookingKineGoNextStep();
            },
            bookingKineGoBack(){
                if(this.newKineBookingDialog.step > 1)
                    this.newKineBookingDialog.step--;
            },
            bookingKineGoNextStep(){
                let that = this;
                that.newKineBookingDialog.step++;

                setTimeout(function(){
                    if(that.newKineBookingDialog.step == 2){
                        if(that.newKineBookingDialog.mode == 'doctor'){
                            if($("#bookingKineCalendar").attr("hasLoaded") != 'Si'){

                                $("#bookingKineCalendar").datepicker({
                                    startDate: '0d',
                                    language: 'es',
                                    format: 'yyyy-mm-dd'
                                }).on('changeDate', function(e){
                                    e.preventDefault();

                                    that.newKineBookingDialog.date = e.format();

                                    that.bookingKineLoadResumeHours();
                                }).datepicker('setDate', moment().format('YYYY-MM-DD'));

                                $("#bookingKineCalendar").attr("hasLoaded", "Si");
                            }
                        }else{
                            if($("#bookingKineCalendar2").attr("hasLoaded") != 'Si'){

                                $("#bookingKineCalendar2").datepicker({
                                    startDate: '0d',
                                    language: 'es',
                                    format: 'yyyy-mm-dd'
                                }).on('changeDate', function(e){
                                    e.preventDefault();

                                    that.newKineBookingDialog.date = e.format();

                                    that.bookingKineLoadLastHours();
                                }).datepicker('setDate', moment().format('YYYY-MM-DD'));

                                $("#bookingKineCalendar2").attr("hasLoaded", "Si");
                            }
                        }
                    }else if(that.newKineBookingDialog.step == 3){
                        $('#bookingKineBirthdate').mask('00/00/0000');
                    }
                }, 300);
            },
            bookingKineLoadResumeHours(){
                let that = this;
                let date = that.newKineBookingDialog.date;
                let doctor_id = that.newKineBookingDialog.doctor_id;
                let branch_id = that.newKineBookingDialog.branch_id;
                that.freehours = [];

                if(date && date != ''){
                    let beginDate = moment(date).startOf('isoweek').format("YYYY-MM-DD");
                    let endDate = moment(date).endOf('isoweek').format("YYYY-MM-DD");

                    if(doctor_id && branch_id){

                        let query = 'doctor_id=' + doctor_id;
                        query += '&branch_id=' + branch_id;
                        query += '&begin_date=' + beginDate;
                        query += '&end_date=' + endDate;

                        that.newKineBookingDialog.loading = true;
                        Vue.http.get('/api/doctors/resumehoursbetween?' + query)
                            .then((response) => {
                                var rs = response.body;
                                that.newKineBookingDialog.loading = false;

                                if(rs.success){
                                    that.freehours = rs.data;

                                    setTimeout(function(){
                                        $('.timeInput').mask('00:00');
                                    }, 500);
                                }else{
                                    alert(rs.message);
                                }
                            }, (response) => {
                                alert("Algo salio mal, por favor intente nuevamente!");
                            }
                        );
                    }
                }
            },
            bookingKineLoadLastHours(){
                let that = this;
                let date = that.newKineBookingDialog.date;
                let branch_id = that.newKineBookingDialog.branch_id;
                that.freehours = [];

                if(date && date != '' && branch_id){
                    let query = 'date=' + date + '&profile=Kinesiólogo';

                    that.newKineBookingDialog.loading = true;
                    Vue.http.get('/api/branches/'+branch_id+'/last-hours?' + query)
                        .then((response) => {
                            var rs = response.body;
                            that.newKineBookingDialog.loading = false;

                            if(rs.success){
                                that.freehours = rs.data;

                                setTimeout(function(){
                                    $('.timeInput').mask('00:00');
                                }, 500);
                            }else{
                                alert(rs.message);
                            }
                        }, (response) => {
                            alert("Algo salio mal, por favor intente nuevamente!");
                        }
                    );
                }
            },
            bookingKineSelectBranch(){
                if(this.newKineBookingDialog.currentBranch){
                    this.newKineBookingDialog.branch_id = this.newKineBookingDialog.currentBranch.id;
                }else{
                    this.newKineBookingDialog.branch_id = null;
                }

                this.doctors = this.newKineBookingDialog.currentBranch.kinesiologists;

                if(this.newKineBookingDialog.mode == 'hour'){
                    this.bookingKineLoadLastHours();
                }
            },
            bookingKineSelectDoctor(){
                if(this.newKineBookingDialog.currentDoctor){
                    this.newKineBookingDialog.doctor_id = this.newKineBookingDialog.currentDoctor.id;
                }else{
                    this.newKineBookingDialog.doctor_id = null;
                }

                if(this.newKineBookingDialog.mode == 'doctor'){
                    this.bookingKineLoadResumeHours();
                }
            },
            bookingKineSelectTime(time, doctor){
                let that = this;

                if(that.newKineBookingDialog.mode == 'doctor'){
                    if(!that.inArray(time, that.newKineBookingDialog.dates)){
                        that.newKineBookingDialog.dates.push(time);
                    }
                }else{
                    that.newKineBookingDialog.dates = [];
                    that.newKineBookingDialog.dates.push(time);
                }
                
                if(doctor){
                    that.newKineBookingDialog.currentDoctor = doctor;
                    that.newKineBookingDialog.doctor_id = doctor.id;
                }
            },
            bookingKineRemoveTime(time){
                let dates = this.newKineBookingDialog.dates;

                this.newKineBookingDialog.dates = dates.filter(function(item) { 
                    return item !== time;
                });
            },
            bookingKineResetPatient(clearRut){
                let rut = null;

                if(!clearRut)
                    rut = this.newKineBookingDialog.patient.rut;

                this.newKineBookingDialog.patient = {
                    status: 'Pendiente',

                    id: null,
                    rut: rut,
                    firstname: null,
                    lastname: null,
                    phone: null,
                    email: null,
                    birthdate: null,
                    address: null,
                };
                this.newKineBookingDialog.healthcompany_id = null;
                this.newKineBookingDialog.attentiontype_id = null;
                this.newKineBookingDialog.observations = null;
            },
            bookingKineFindPatient(){
                let that = this;
                let rut = that.newKineBookingDialog.patient.rut;

                if(!rut || rut == '' || rut == 'PENDIENTE')
                    return;

                if(!that.validateRut(rut)){
                    that.bookingShowAlert('El rut ingresado no es válido, reviselo e intente de nuevo.', 'danger');
                    return;
                }

                that.bookingKineResetPatient();

                that.newKineBookingDialog.patient.status = 'Buscando';

                Vue.http.get('/api/patients?rut=' + rut)
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            let patient = rs.data;
                            
                            that.newKineBookingDialog.patient = {
                                status: 'Existente',

                                id:                 patient.id,
                                rut:                patient.rut,
                                firstname:          patient.firstname,
                                lastname:           patient.lastname,
                                phone:              patient.phone,
                                email:              patient.email,
                                birthdate:          (patient.birthdate == '' ? '' : moment(patient.birthdate).format("DD/MM/YYYY")),
                                address:            patient.address,
                            };

                            that.newKineBookingDialog.healthcompany_id = patient.healthcompany_id;
                            that.newKineBookingDialog.attentiontype_id = 1;
                        }else{
                            that.newKineBookingDialog.patient.status = 'No existente'; 
                            that.bookingShowAlert('No hemos encontrado ningún paciente con el rut ingresado. Complete la información para registrarlo.', 'warning');
                        }
                    }, (response) => {
                        that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                    }
                );
            },
            bookingKineSetNewPatient(withoutRut){
                this.bookingKineResetPatient();
                this.newKineBookingDialog.patient.status = 'Nuevo';

                if(withoutRut){
                    this.newKineBookingDialog.patient.rut = 'PENDIENTE';
                }
            },
            bookingKineRegister(){
                let that = this;
                let patient_id = that.newKineBookingDialog.patient.id;
                that.bookingKineGoNextStep();

                let patientData = {
                    healthcompany_id: that.newKineBookingDialog.healthcompany_id,
                    rut:            that.newKineBookingDialog.patient.rut,
                    firstname:      that.newKineBookingDialog.patient.firstname,
                    lastname:       that.newKineBookingDialog.patient.lastname,
                    email:          that.newKineBookingDialog.patient.email,
                    phone:          that.newKineBookingDialog.patient.phone,
                    address:        that.newKineBookingDialog.patient.address,
                    birthdate:      moment(that.newKineBookingDialog.patient.birthdate, "DD/MM/YYYY").format("YYYY-MM-DD"),
                    age:            moment().diff(moment("09/03/1985", "DD/MM/YYYY"), 'years')
                };

                let bookingDates = that.newKineBookingDialog.dates;

                let bookingData = {
                    healthcompany_id:   that.newKineBookingDialog.healthcompany_id,
                    branch_id:          that.newKineBookingDialog.branch_id,
                    patient_id:         patient_id,
                    doctor_id:          that.newKineBookingDialog.doctor_id,
                    bookingdates:       bookingDates,
                    attentiontype_id:   that.newKineBookingDialog.attentiontype_id,
                    observations:       that.newKineBookingDialog.observations,
                    type:               'Normal'
                };

                let success = function(rs){
                    that.newKineBookingDialog.booking = rs;

                    if(!rs.success){
                        that.bookingShowAlert(rs.message, 'warning');
                    }
                };
                let failback = function(){
                    that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                        that.bookingKineGoBack();
                };

                if(!patient_id){
                    that.storePatient(patientData, function(){
                        bookingData.patient_id = that.newKineBookingDialog.patient.id;

                        that.storeBooking(bookingData, success, failback);
                    });
                }else{
                    that.storeBooking(bookingData, success, failback);
                }
            },

        // Traumatología
            bookingTraOpenDialog(){
                this.bookingTraReset();

                setTimeout(function(){
                    $("#newTraumatologyBookingDialog").modal('show');
                }, 500);
            },
            bookingTraReset(){
                this.freehours = [];
                this.newTraBookingDialog = {
                    step: 1,
                    mode: null,
                    profile: 'Traumatólogo',
                    branch_id: null,
                    doctor_id: null,
                    date: null,
                    time:  null,
                    currentDoctor: null,
                    currentBranch: null,
                    currentTime: null,
                    patient: {
                        status: 'Pendiente',

                        id: null,
                        rut: '',
                        firstname: null,
                        lastname: null,
                        phone: null,
                        email: null,
                        birthdate: null,
                        address: null,
                    },
                    healthcompany_id: null,
                    attentiontype_id: null,
                    observations: null,
                    loading: false,
                    booking: null,
                    type: 'Normal',
                    extraHour: null,
                    extraDate: null
                };
            },
            bookingTraSelectMode(mode){
                let that = this;

                that.newTraBookingDialog.mode = mode;
                that.bookingTraGoNextStep();
            },
            bookingTraLoadResumeHours(){
                let that = this;
                let date = that.newTraBookingDialog.date;
                let doctor_id = that.newTraBookingDialog.doctor_id;
                let branch_id = that.newTraBookingDialog.branch_id;
                that.freehours = [];

                if(date && date != ''){
                    let beginDate = moment(date).startOf('isoweek').format("YYYY-MM-DD");
                    let endDate = moment(date).endOf('isoweek').format("YYYY-MM-DD");

                    if(doctor_id && branch_id){

                        let query = 'doctor_id=' + doctor_id;
                        query += '&branch_id=' + branch_id;
                        query += '&begin_date=' + beginDate;
                        query += '&end_date=' + endDate;

                        that.newTraBookingDialog.loading = true;
                        Vue.http.get('/api/doctors/resumehoursbetween?' + query)
                            .then((response) => {
                                var rs = response.body;
                                that.newTraBookingDialog.loading = false;

                                if(rs.success){
                                    that.freehours = rs.data;

                                    setTimeout(function(){
                                        $('.timeInput').mask('00:00');
                                    }, 500);
                                }else{
                                    alert(rs.message);
                                }
                            }, (response) => {
                                alert("Algo salio mal, por favor intente nuevamente!");
                            }
                        );
                    }
                }
            },
            bookingTraLoadLastHours(){
                let that = this;
                let date = that.newTraBookingDialog.date;
                let branch_id = that.newTraBookingDialog.branch_id;
                that.freehours = [];

                if(date && date != '' && branch_id){
                    let query = 'date=' + date + '&profile=Traumatólogo';

                    that.newTraBookingDialog.loading = true;
                    Vue.http.get('/api/branches/'+branch_id+'/last-hours?' + query)
                        .then((response) => {
                            var rs = response.body;
                            that.newTraBookingDialog.loading = false;

                            if(rs.success){
                                that.freehours = rs.data;

                                setTimeout(function(){
                                    $('.timeInput').mask('00:00');
                                }, 500);
                            }else{
                                alert(rs.message);
                            }
                        }, (response) => {
                            alert("Algo salio mal, por favor intente nuevamente!");
                        }
                    );
                }
            },
            bookingTraLoadFreeHours(){
                let that = this;
                that.freehours = [];

                let query = 'doctor_id=' + that.newTraBookingDialog.doctor_id;
                query += '&branch_id=' + that.newTraBookingDialog.branch_id;
                query += '&date=' + that.newTraBookingDialog.date;

                Vue.http.get('/api/doctors/freehours?' + query)
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            that.freehours = rs.data;
                        }else{
                            alert(rs.message);
                        }
                    }, (response) => {
                        alert("Algo salio mal, por favor intente nuevamente!");
                    }
                );
            },
            bookingTraGoBack(){
                if(this.newTraBookingDialog.step > 1)
                    this.newTraBookingDialog.step--;
            },
            bookingTraGoNextStep(){
                let that = this;
                that.newTraBookingDialog.step++;

                setTimeout(function(){
                    if(that.newTraBookingDialog.step == 2){
                        if(that.newTraBookingDialog.mode == 'doctor'){
                            if($("#bookingTraCalendar").attr("hasLoaded") != 'Si'){

                                $("#bookingTraCalendar").datepicker({
                                    startDate: '0d',
                                    language: 'es',
                                    format: 'yyyy-mm-dd'
                                }).on('changeDate', function(e){
                                    e.preventDefault();

                                    that.newTraBookingDialog.date = e.format();

                                    that.bookingTraLoadResumeHours();
                                }).datepicker('setDate', moment().format('YYYY-MM-DD'));

                                $("#bookingTraCalendar").attr("hasLoaded", "Si");
                            }
                        }else{
                            if($("#bookingTraCalendar2").attr("hasLoaded") != 'Si'){

                                $("#bookingTraCalendar2").datepicker({
                                    startDate: '0d',
                                    language: 'es',
                                    format: 'yyyy-mm-dd'
                                }).on('changeDate', function(e){
                                    e.preventDefault();

                                    that.newTraBookingDialog.date = e.format();

                                    that.bookingTraLoadLastHours();
                                }).datepicker('setDate', moment().format('YYYY-MM-DD'));

                                $("#bookingTraCalendar2").attr("hasLoaded", "Si");
                            }
                        }
                    }else if(that.newTraBookingDialog.step == 3){
                        $('#bookingTraBirthdate').mask('00/00/0000');
                    }
                }, 300);
            },
            bookingTraLoadDoctors(){
                let that = this;
                that.doctors = [];
            
                if(that.newTraBookingDialog.branch_id){
                    Vue.http.get('/api/branches/' + that.newTraBookingDialog.branch_id + '/doctors?profile=' + that.newTraBookingDialog.profile)
                        .then((response) => {
                            var rs = response.body;

                            if(rs.success){
                                that.doctors = rs.data;
                            }else{
                                alert(rs.message);
                            }
                        }, (response) => {
                            alert("Algo salio mal, por favor intente nuevamente!");
                        }
                    );
                }
            },
            bookingTraSelectTime(time, doctor){
                let that = this;

                that.newTraBookingDialog.currentTime = time;
                that.newTraBookingDialog.time = moment(time).format('HH:mm');
                that.newTraBookingDialog.type = 'Normal';

                if(doctor){
                    that.newTraBookingDialog.currentDoctor = doctor;
                    that.newTraBookingDialog.doctor_id = doctor.id;
                }
            },
            bookingTraSelectExtraTime(date){
                this.newTraBookingDialog.type = 'Extra';
                this.newTraBookingDialog.extraDate = date;

                setTimeout(function(){
                    $("#extraHour_" + moment(date).format('YYYYMMDD')).focus();  
                }, 200);
            },
            bookingTraSelectBranch(){
                if(this.newTraBookingDialog.currentBranch){
                    this.newTraBookingDialog.branch_id = this.newTraBookingDialog.currentBranch.id;
                }else{
                    this.newTraBookingDialog.branch_id = null;
                }

                this.doctors = this.newTraBookingDialog.currentBranch.traumatologists;

                if(this.newTraBookingDialog.mode == 'hour'){
                    this.bookingTraLoadLastHours();
                }
            },
            bookingTraSelectDoctor(){
                if(this.newTraBookingDialog.currentDoctor){
                    this.newTraBookingDialog.doctor_id = this.newTraBookingDialog.currentDoctor.id;
                }else{
                    this.newTraBookingDialog.doctor_id = null;
                }

                if(this.newTraBookingDialog.mode == 'doctor'){
                    this.bookingTraLoadResumeHours();
                }
            },
            bookingTraResetPatient(clearRut){
                let rut = null;

                if(!clearRut)
                    rut = this.newTraBookingDialog.patient.rut;

                this.newTraBookingDialog.patient = {
                    status: 'Pendiente',

                    id: null,
                    rut: rut,
                    firstname: null,
                    lastname: null,
                    phone: null,
                    email: null,
                    birthdate: null,
                    address: null,
                };
                this.newTraBookingDialog.healthcompany_id = null;
                this.newTraBookingDialog.attentiontype_id = null;
                this.newTraBookingDialog.observations = null;
            },
            bookingTraFindPatient(){
                let that = this;
                let rut = that.newTraBookingDialog.patient.rut;

                if(!rut || rut == '' || rut == 'PENDIENTE')
                    return;

                if(!that.validateRut(rut)){
                    that.bookingShowAlert('El rut ingresado no es válido, reviselo e intente de nuevo.', 'danger');
                    return;
                }

                that.bookingTraResetPatient();

                that.newTraBookingDialog.patient.status = 'Buscando';

                Vue.http.get('/api/patients?rut=' + rut)
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            let patient = rs.data;
                            
                            that.newTraBookingDialog.patient = {
                                status: 'Existente',

                                id:                 patient.id,
                                rut:                patient.rut,
                                firstname:          patient.firstname,
                                lastname:           patient.lastname,
                                phone:              patient.phone,
                                email:              patient.email,
                                birthdate:          (patient.birthdate == '' ? '' : moment(patient.birthdate).format("DD/MM/YYYY")),
                                address:            patient.address,
                            };

                            that.newTraBookingDialog.healthcompany_id = patient.healthcompany_id;
                            that.newTraBookingDialog.attentiontype_id = 1;
                        }else{
                            that.newTraBookingDialog.patient.status = 'No existente'; 
                            that.bookingShowAlert('No hemos encontrado ningún paciente con el rut ingresado. Complete la información para registrarlo.', 'warning');
                        }
                    }, (response) => {
                        that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                    }
                );
            },
            bookingTraSetNewPatient(withoutRut){
                this.bookingTraResetPatient();
                this.newTraBookingDialog.patient.status = 'Nuevo';

                if(withoutRut){
                    this.newTraBookingDialog.patient.rut = 'PENDIENTE';
                }
            },
            bookingTraRegister(){
                let that = this;
                let patient_id = that.newTraBookingDialog.patient.id;
                that.bookingTraGoNextStep();

                let patientData = {
                    healthcompany_id: that.newTraBookingDialog.healthcompany_id,
                    rut:            that.newTraBookingDialog.patient.rut,
                    firstname:      that.newTraBookingDialog.patient.firstname,
                    lastname:       that.newTraBookingDialog.patient.lastname,
                    email:          that.newTraBookingDialog.patient.email,
                    phone:          that.newTraBookingDialog.patient.phone,
                    address:        that.newTraBookingDialog.patient.address,
                    birthdate:      moment(that.newTraBookingDialog.patient.birthdate, "DD/MM/YYYY").format("YYYY-MM-DD"),
                    age:            moment().diff(moment("09/03/1985", "DD/MM/YYYY"), 'years')
                };

                let bookingDate = (that.newTraBookingDialog.type == 'Normal') ? that.newTraBookingDialog.currentTime : that.newTraBookingDialog.extraDate + ' ' + that.newTraBookingDialog.extraHour + ':00';

                let bookingData = {
                    healthcompany_id:   that.newTraBookingDialog.healthcompany_id,
                    branch_id:          that.newTraBookingDialog.branch_id,
                    patient_id:         patient_id,
                    doctor_id:          that.newTraBookingDialog.doctor_id,
                    bookingdate:        bookingDate,
                    attentiontype_id:   that.newTraBookingDialog.attentiontype_id,
                    observations:       that.newTraBookingDialog.observations,
                    type:               that.newTraBookingDialog.type

                };

                let success = function(rs){
                    if(rs.success){
                        that.newTraBookingDialog.booking = rs.data;
                    }else{
                        that.bookingShowAlert(rs.message, 'warning');
                        that.bookingTraGoBack();
                    }
                };
                let failback = function(){
                    that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                        that.bookingTraGoBack();
                };

                if(!patient_id){
                    that.storePatient(patientData, function(){
                        bookingData.patient_id = that.newTraBookingDialog.patient.id;

                        that.storeBooking(bookingData, success, failback);
                    });
                }else{
                    that.storeBooking(bookingData, success, failback);
                }
            },

        // Radiología
            bookingRadOpenDialog(){
                this.bookingRadReset();

                let branch = this.branches.filter(function(item){
                    return item.id == 1;
                });

                this.newRadBookingDialog.currentBranch = branch[0];
                this.newRadBookingDialog.branch_id = this.newRadBookingDialog.currentBranch.id;

                this.doctors = this.newRadBookingDialog.currentBranch.radiologists;
                this.newRadBookingDialog.currentDoctor = this.doctors[0];
                this.bookingRadSelectDoctor();

                setTimeout(function(){
                    $("#newRadiologyBookingDialog").modal('show');
                }, 500);
            },
            bookingRadReset(){
                this.freehours = [];
                this.newRadBookingDialog = {
                    step: 1,
                    mode: null,
                    profile: 'Radiólogo',
                    branch_id: null,
                    doctor_id: null,
                    date: null,
                    time:  null,
                    currentDoctor: null,
                    currentBranch: null,
                    currentTime: null,
                    patient: {
                        status: 'Pendiente',

                        id: null,
                        rut: '',
                        firstname: null,
                        lastname: null,
                        phone: null,
                        email: null,
                        birthdate: null,
                        address: null,
                    },
                    healthcompany_id: null,
                    attentiontype_id: null,
                    observations: null,
                    loading: false,
                    booking: null,
                    type: 'Normal',
                    extraHour: null,
                    extraDate: null
                };
            },
            bookingRadSelectMode(mode){
                let that = this;

                that.newRadBookingDialog.mode = mode;
                that.bookingRadGoNextStep();
            },
            bookingRadLoadResumeHours(){
                let that = this;
                let date = that.newRadBookingDialog.date;
                let doctor_id = that.newRadBookingDialog.doctor_id;
                let branch_id = that.newRadBookingDialog.branch_id;
                that.freehours = [];

                if(date && date != ''){
                    let beginDate = moment(date).startOf('isoweek').format("YYYY-MM-DD");
                    let endDate = moment(date).endOf('isoweek').format("YYYY-MM-DD");

                    if(doctor_id && branch_id){

                        let query = 'doctor_id=' + doctor_id;
                        query += '&branch_id=' + branch_id;
                        query += '&begin_date=' + beginDate;
                        query += '&end_date=' + endDate;

                        that.newRadBookingDialog.loading = true;
                        Vue.http.get('/api/doctors/resumehoursbetween?' + query)
                            .then((response) => {
                                var rs = response.body;
                                that.newRadBookingDialog.loading = false;

                                if(rs.success){
                                    that.freehours = rs.data;

                                    setTimeout(function(){
                                        $('.timeInput').mask('00:00');
                                    }, 500);
                                }else{
                                    alert(rs.message);
                                }
                            }, (response) => {
                                alert("Algo salio mal, por favor intente nuevamente!");
                            }
                        );
                    }
                }
            },
            bookingRadLoadLastHours(){
                let that = this;
                let date = that.newRadBookingDialog.date;
                let branch_id = that.newRadBookingDialog.branch_id;
                that.freehours = [];

                if(date && date != '' && branch_id){
                    let query = 'date=' + date + '&profile=Traumatólogo';

                    that.newRadBookingDialog.loading = true;
                    Vue.http.get('/api/branches/'+branch_id+'/last-hours?' + query)
                        .then((response) => {
                            var rs = response.body;
                            that.newRadBookingDialog.loading = false;

                            if(rs.success){
                                that.freehours = rs.data;

                                setTimeout(function(){
                                    $('.timeInput').mask('00:00');
                                }, 500);
                            }else{
                                alert(rs.message);
                            }
                        }, (response) => {
                            alert("Algo salio mal, por favor intente nuevamente!");
                        }
                    );
                }
            },
            bookingRadLoadFreeHours(){
                let that = this;
                that.freehours = [];

                let query = 'doctor_id=' + that.newRadBookingDialog.doctor_id;
                query += '&branch_id=' + that.newRadBookingDialog.branch_id;
                query += '&date=' + that.newRadBookingDialog.date;

                Vue.http.get('/api/doctors/freehours?' + query)
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            that.freehours = rs.data;
                        }else{
                            alert(rs.message);
                        }
                    }, (response) => {
                        alert("Algo salio mal, por favor intente nuevamente!");
                    }
                );
            },
            bookingRadGoBack(){
                if(this.newRadBookingDialog.step > 1)
                    this.newRadBookingDialog.step--;
            },
            bookingRadGoNextStep(){
                let that = this;
                that.newRadBookingDialog.step++;

                setTimeout(function(){
                    if(that.newRadBookingDialog.step == 2){
                        if(that.newRadBookingDialog.mode == 'doctor'){
                            if($("#bookingRadCalendar").attr("hasLoaded") != 'Si'){

                                $("#bookingRadCalendar").datepicker({
                                    startDate: '0d',
                                    language: 'es',
                                    format: 'yyyy-mm-dd'
                                }).on('changeDate', function(e){
                                    e.preventDefault();

                                    that.newRadBookingDialog.date = e.format();

                                    that.bookingRadLoadResumeHours();
                                }).datepicker('setDate', moment().format('YYYY-MM-DD'));

                                $("#bookingRadCalendar").attr("hasLoaded", "Si");
                            }
                        }else{
                            if($("#bookingRadCalendar2").attr("hasLoaded") != 'Si'){

                                $("#bookingRadCalendar2").datepicker({
                                    startDate: '0d',
                                    language: 'es',
                                    format: 'yyyy-mm-dd'
                                }).on('changeDate', function(e){
                                    e.preventDefault();

                                    that.newRadBookingDialog.date = e.format();

                                    that.bookingRadLoadLastHours();
                                }).datepicker('setDate', moment().format('YYYY-MM-DD'));

                                $("#bookingRadCalendar2").attr("hasLoaded", "Si");
                            }
                        }
                    }else if(that.newRadBookingDialog.step == 3){
                        $('#bookingRadBirthdate').mask('00/00/0000');
                    }
                }, 300);
            },
            bookingRadLoadDoctors(){
                let that = this;
                that.doctors = [];
            
                if(that.newRadBookingDialog.branch_id){
                    Vue.http.get('/api/branches/' + that.newRadBookingDialog.branch_id + '/doctors?profile=' + that.newRadBookingDialog.profile)
                        .then((response) => {
                            var rs = response.body;

                            if(rs.success){
                                that.doctors = rs.data;
                            }else{
                                alert(rs.message);
                            }
                        }, (response) => {
                            alert("Algo salio mal, por favor intente nuevamente!");
                        }
                    );
                }
            },
            bookingRadSelectTime(time, doctor){
                let that = this;

                that.newRadBookingDialog.currentTime = time;
                that.newRadBookingDialog.time = moment(time).format('HH:mm');
                that.newRadBookingDialog.type = 'Normal';

                if(doctor){
                    that.newRadBookingDialog.currentDoctor = doctor;
                    that.newRadBookingDialog.doctor_id = doctor.id;
                }
            },
            bookingRadSelectExtraTime(date){
                this.newRadBookingDialog.type = 'Extra';
                this.newRadBookingDialog.extraDate = date;

                setTimeout(function(){
                    $("#extraHour_" + moment(date).format('YYYYMMDD')).focus();  
                }, 200);
            },
            bookingRadSelectBranch(){
                if(this.newRadBookingDialog.currentBranch){
                    this.newRadBookingDialog.branch_id = this.newRadBookingDialog.currentBranch.id;
                }else{
                    this.newRadBookingDialog.branch_id = null;
                }

                this.doctors = this.newRadBookingDialog.currentBranch.radiologists;

                if(this.newRadBookingDialog.mode == 'hour'){
                    this.bookingRadLoadLastHours();
                }
            },
            bookingRadSelectDoctor(){
                if(this.newRadBookingDialog.currentDoctor){
                    this.newRadBookingDialog.doctor_id = this.newRadBookingDialog.currentDoctor.id;
                }else{
                    this.newRadBookingDialog.doctor_id = null;
                }

                if(this.newRadBookingDialog.mode == 'doctor'){
                    this.bookingRadLoadResumeHours();
                }
            },
            bookingRadResetPatient(clearRut){
                let rut = null;

                if(!clearRut)
                    rut = this.newRadBookingDialog.patient.rut;

                this.newRadBookingDialog.patient = {
                    status: 'Pendiente',

                    id: null,
                    rut: rut,
                    firstname: null,
                    lastname: null,
                    phone: null,
                    email: null,
                    birthdate: null,
                    address: null,
                };
                this.newRadBookingDialog.healthcompany_id = null;
                this.newRadBookingDialog.attentiontype_id = null;
                this.newRadBookingDialog.observations = null;
            },
            bookingRadFindPatient(){
                let that = this;
                let rut = that.newRadBookingDialog.patient.rut;

                if(!rut || rut == '' || rut == 'PENDIENTE')
                    return;

                if(!that.validateRut(rut)){
                    that.bookingShowAlert('El rut ingresado no es válido, reviselo e intente de nuevo.', 'danger');
                    return;
                }

                that.bookingRadResetPatient();

                that.newRadBookingDialog.patient.status = 'Buscando';

                Vue.http.get('/api/patients?rut=' + rut)
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            let patient = rs.data;
                            
                            that.newRadBookingDialog.patient = {
                                status: 'Existente',

                                id:                 patient.id,
                                rut:                patient.rut,
                                firstname:          patient.firstname,
                                lastname:           patient.lastname,
                                phone:              patient.phone,
                                email:              patient.email,
                                birthdate:          (patient.birthdate == '' ? '' : moment(patient.birthdate).format("DD/MM/YYYY")),
                                address:            patient.address,
                            };

                            that.newRadBookingDialog.healthcompany_id = patient.healthcompany_id;
                            that.newRadBookingDialog.attentiontype_id = 1;
                        }else{
                            that.newRadBookingDialog.patient.status = 'No existente'; 
                            that.bookingShowAlert('No hemos encontrado ningún paciente con el rut ingresado. Complete la información para registrarlo.', 'warning');
                        }
                    }, (response) => {
                        that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                    }
                );
            },
            bookingRadSetNewPatient(withoutRut){
                this.bookingRadResetPatient();
                this.newRadBookingDialog.patient.status = 'Nuevo';

                if(withoutRut){
                    this.newRadBookingDialog.patient.rut = 'PENDIENTE';
                }
            },
            bookingRadRegister(){
                let that = this;
                let patient_id = that.newRadBookingDialog.patient.id;
                that.bookingRadGoNextStep();

                let patientData = {
                    healthcompany_id: that.newRadBookingDialog.healthcompany_id,
                    rut:            that.newRadBookingDialog.patient.rut,
                    firstname:      that.newRadBookingDialog.patient.firstname,
                    lastname:       that.newRadBookingDialog.patient.lastname,
                    email:          that.newRadBookingDialog.patient.email,
                    phone:          that.newRadBookingDialog.patient.phone,
                    address:        that.newRadBookingDialog.patient.address,
                    birthdate:      moment(that.newRadBookingDialog.patient.birthdate, "DD/MM/YYYY").format("YYYY-MM-DD"),
                    age:            moment().diff(moment("09/03/1985", "DD/MM/YYYY"), 'years')
                };

                let bookingDate = (that.newRadBookingDialog.type == 'Normal') ? that.newRadBookingDialog.currentTime : that.newRadBookingDialog.extraDate + ' ' + that.newRadBookingDialog.extraHour + ':00';

                let bookingData = {
                    healthcompany_id:   that.newRadBookingDialog.healthcompany_id,
                    branch_id:          that.newRadBookingDialog.branch_id,
                    patient_id:         patient_id,
                    doctor_id:          that.newRadBookingDialog.doctor_id,
                    bookingdate:        bookingDate,
                    attentiontype_id:   that.newRadBookingDialog.attentiontype_id,
                    observations:       that.newRadBookingDialog.observations,
                    type:               that.newRadBookingDialog.type

                };

                let success = function(rs){
                    if(rs.success){
                        that.newRadBookingDialog.booking = rs.data;
                    }else{
                        that.bookingShowAlert(rs.message, 'warning');
                        that.bookingRadGoBack();
                    }
                };
                let failback = function(){
                    that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                        that.bookingRadGoBack();
                };

                if(!patient_id){
                    that.storePatient(patientData, function(){
                        bookingData.patient_id = that.newRadBookingDialog.patient.id;

                        that.storeBooking(bookingData, success, failback);
                    });
                }else{
                    that.storeBooking(bookingData, success, failback);
                }
            },

        // Recibir Paciente       
            openReceivePatientDialog(){
                let that = this;

                $("#receivePatientDialog").modal('show');

                $("#bookingReceivePatientDatepicker").datepicker({
                    startDate: '0d',
                    language: 'es',
                    format: 'yyyy-mm-dd'
                }).on('changeDate', function(e){
                    e.preventDefault();

                    that.receivePatientDialog.search.bookingdate = e.format();
                }).datepicker('setDate', moment().format('YYYY-MM-DD'));
            },
            openReceivePatientSetHour(hour){
                this.receivePatientDialog.selectedHour = hour;
            },

        // Globales
            bookingShowAlert(message, type){
                let that = this;

                that.alert.message = message;

                switch(type){
                    case 'info':
                        that.alert.icon = 'fa fa-info-circle';
                        that.alert.alertClass = 'alert alert-info';
                        break;
                    case 'warning':
                        that.alert.icon = 'fa fa-warning';
                        that.alert.alertClass = 'alert alert-warning';
                        break;
                    case 'success':
                        that.alert.icon = 'fa fa-check-circle';
                        that.alert.alertClass = 'alert alert-success';
                        break;
                    case 'danger':
                        that.alert.icon = 'fa fa-times-circle';
                        that.alert.alertClass = 'alert alert-danger';
                        break;
                    default:
                        that.alert.icon = 'fa fa-info-circle';
                        that.alert.alertClass = 'alert alert-info';
                        break;
                }

                setTimeout(function(){
                    that.alert.message = '';
                }, 5000);
            },
            storePatient(data, callback, profile){
                let that = this;

                Vue.http.post('/api/secretary/patients', data)
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            that.newTraBookingDialog.patient = rs.data;

                            if(typeof(callback) == 'function')
                                callback();
                        }else{
                            that.bookingShowAlert(rs.message, 'warning');
                            that.bookingTraGoBack();
                        }
                    }, (response) => {
                        that.bookingShowAlert("Algo salio mal, por favor intente nuevamente!", 'danger');
                        that.bookingTraGoBack();
                    }
                );
            },
            storeBooking(data, success, failback){
                let that = this;

                Vue.http.post('/api/secretary/bookings', data)
                    .then((response) => {
                        var rs = response.body;

                        if(typeof(success) == 'function')
                            success(rs);
                    }, (response) => {
                        if(typeof(failback) == 'function')
                            failback();
                    }
                );
            },
            loadAllDoctors(){
                let that = this;

                Vue.http.get('/api/doctors')
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            that.allDoctors = rs.data;
                        }else{
                            alert(rs.message);
                        }
                    }, (response) => {
                        alert("Algo salio mal, por favor intente nuevamente!");
                    }
                );
            },
        	loadBranches(){
        		let that = this;
        		
        		Vue.http.get('/api/branches')
        			.then((response) => {
        				var rs = response.body;

        				if(rs.success){
        					that.branches = rs.data;
        				}else{
        					alert(rs.message);
        				}
    				}, (response) => {
    					alert("Algo salio mal, por favor intente nuevamente!");
    				}
    			);
        	},
        	loadBoxes(){
        		let that = this;
        		
        		that.openBoxDialog.paymentbox_id = null;
                that.openBoxDialog.branch_id = that.currentBranch.id;
                that.boxes = that.currentBranch.paymentboxes;
        	},
            loadHealthCompanies(){
                let that = this;
                that.healthcompanies = [];

                Vue.http.get('/api/healthcompanies')
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            that.healthcompanies = rs.data;
                        }else{
                            alert(rs.message);
                        }
                    }, (response) => {
                        alert("Algo salio mal, por favor intente nuevamente!");
                    }
                );
            },
            loadAttentionTypes(){
                let that = this;
                that.attentiontypes = [];

                Vue.http.get('/api/attentiontypes')
                    .then((response) => {
                        var rs = response.body;

                        if(rs.success){
                            that.attentiontypes = rs.data;
                        }else{
                            alert(rs.message);
                        }
                    }, (response) => {
                        alert("Algo salio mal, por favor intente nuevamente!");
                    }
                );
            },
        	openBox(){

        		$("#openBoxDialog").modal('show');
        	},
        	confirmOpenBox(){
        		Vue.http.post('/api/secretary/session/open-box', this.openBoxDialog)
        			.then((response) => {
        				var rs = response.body;

        				if(rs.success){
        					
        					window.location.reload();

        				}else{
        					alert(rs.message);
        				}
    				}, (response) => {
    					alert("Algo salio mal, por favor intente nuevamente!");
    				}
    			);
        	},
        	closeBox(){

        		$("#closeBoxDialog").modal('show');
        	},
        	confirmCloseBox(){
        		Vue.http.post('/api/secretary/session/close-box', this.closeBoxDialog)
        			.then((response) => {
        				var rs = response.body;

        				if(rs.success){
        					
        					window.location.reload();

        				}else{
        					alert(rs.message);
        				}
    				}, (response) => {
    					alert("Algo salio mal, por favor intente nuevamente!");
    				}
    			);
        	},
            moment(date){

                return moment(date);
            },
            validateRut: function (rutCompleto) {
                if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
                    return false;
                var tmp     = rutCompleto.split('-');
                var digv    = tmp[1]; 
                var rut     = tmp[0];
                if ( digv == 'K' ) digv = 'k' ;
                return (this.dv(rut) == digv );
            },
            dv: function(T){
                var M=0,S=1;
                for(;T;T=Math.floor(T/10))
                    S=(S+T%10*(9-M++%6))%11;
                return S?S-1:'k';
            },
            inArray: function(needle, haystack) {
                var length = haystack.length;
                for(var i = 0; i < length; i++) {
                    if(haystack[i] == needle) return true;
                }
                return false;
            }
    },
    computed: {
        // Kinesiología
            bookingKineDate(){
                let info = this.newKineBookingDialog;
                
                return (info && info.currentTime ? moment(info.currentTime) : null);
            },
            bookingKineCheckBookingDateStep(){
                let info = this.newKineBookingDialog;
                if(info){
                    let bookingInfo = (info.currentBranch && info.currentDoctor);
                    let timeInfo = (info.currentTime !== null && info.currentTime != '');

                    return (info && bookingInfo && timeInfo);
                }

                return false;
            },
            bookingKineCheckDateForRegister(){
                let info = this.newKineBookingDialog;
                if(info){
                    let otherInfo = (info.healthcompany_id && info.attentiontype_id);

                    return (this.bookingKineCheckBookingDateStep && otherInfo);
                }

                return false;
            }, 

        // Traumatología
            bookingTraCheckBookingDateStep(){
                let info = this.newTraBookingDialog;
                if(info){
                    let bookingInfo = (info.currentBranch && info.currentDoctor);
                    let timeInfo = (info.type == 'Normal' ? (info.currentTime !== null && info.currentTime != '') : (info.extraHour !== null && info.extraHour.length == 5));

                    return (info && bookingInfo && timeInfo);
                }

                return false;
            },
            bookingTraCheckDateForRegister(){
                let info = this.newTraBookingDialog;
                if(info){
                    let otherInfo = (info.healthcompany_id && info.attentiontype_id);

                    return (this.bookingTraCheckBookingDateStep && otherInfo);
                }

                return false;
            }, 
            bookingTraDate(){
                let info = this.newTraBookingDialog;
                if(info){
                    return (info.type == 'Normal') ? 
                        (info.currentTime ? moment(info.currentTime) : null) : 
                        (info.extraHour && info.extraHour.length == 5 ? moment(info.extraDate + ' ' + info.extraHour + ':00') : null);
                }

                return null;
            },

        // Radiología
            bookingRadCheckBookingDateStep(){
                let info = this.newRadBookingDialog;
                if(info){
                    let bookingInfo = (info.currentBranch && info.currentDoctor);
                    let timeInfo = (info.type == 'Normal' ? (info.currentTime !== null && info.currentTime != '') : (info.extraHour !== null && info.extraHour.length == 5));

                    return (info && bookingInfo && timeInfo);
                }

                return false;
            },
            bookingRadCheckDateForRegister(){
                let info = this.newRadBookingDialog;
                if(info){
                    let otherInfo = (info.healthcompany_id && info.attentiontype_id);

                    return (this.bookingRadCheckBookingDateStep && otherInfo);
                }

                return false;
            }, 
            bookingRadDate(){
                let info = this.newRadBookingDialog;
                if(info){
                    return (info.type == 'Normal') ? 
                        (info.currentTime ? moment(info.currentTime) : null) : 
                        (info.extraHour && info.extraHour.length == 5 ? moment(info.extraDate + ' ' + info.extraHour + ':00') : null);
                }

                return null;
            },
    }
});
