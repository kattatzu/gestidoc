window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
require('bootstrap');
require('bootstrap-datepicker');
require('jquery-mask-plugin');
window.moment = moment = require('moment');
moment.locale('es');

window.Vue = require('vue');
require('vue-resource');

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});