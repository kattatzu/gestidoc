window.patientModel = {
    store(data, onSuccess, onFail){
        store.log("patient::store()");

        let that = this;

        Vue.http.post('/api/patients', data)
            .then((response) => {
                var rs = response.body;

                if(typeof(onSuccess) == 'function')
                    onSuccess(rs);
            }, (response) => {
                if(typeof(onFail) == 'function')
                    onFail(response);
            }
        );
    },
    update(id, data, onSuccess, onFail){
        store.log("patient::update()");

        let that = this;

        Vue.http.put('/api/patients/' + id, data)
            .then((response) => {
                var rs = response.body;

                if(typeof(onSuccess) == 'function')
                    onSuccess(rs);
            }, (response) => {
                if(typeof(onFail) == 'function')
                    onFail(response);
            }
        );
    },
};