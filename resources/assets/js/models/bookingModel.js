window.bookingModel = {
    store(data, onSuccess, onFail){
        store.log("booking::store()");

        let that = this;

        Vue.http.post('/api/bookings', data)
            .then((response) => {
                var rs = response.body;

                if(typeof(onSuccess) == 'function')
                    onSuccess(rs);
            }, (response) => {
                if(typeof(onFail) == 'function')
                    onFail(response);
            }
        );
    },
};