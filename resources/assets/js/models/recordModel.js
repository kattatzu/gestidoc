window.recordModel = {
    store(data, onSuccess, onFail){
        store.log("record::store()");

        let that = this;

        Vue.http.post('/api/records', data)
            .then((response) => {
                var rs = response.body;

                if(typeof(onSuccess) == 'function')
                    onSuccess(rs);
            }, (response) => {
                if(typeof(onFail) == 'function')
                    onFail(response);
            }
        );
    },
};