const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir((mix) => {
    mix.sass('app.scss')
    mix.webpack('app.js')
   	mix.webpack('secretary.js')
	/*mix.scripts([
		'../../../node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js'
	])*/
    mix.browserSync({
    	proxy: 'cominnew.app'
	});
});
